<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tasks declaration.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

$tasks = array(

    // Actualiza las entidades de gestión de derechos.
    array(
        'classname' => 'local_lpi\task\update_rights_entity',
        'blocking' => 0,
        'minute' => '*/5',
        'hour' => '*',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    ),

    // Oculta las actividades y recursos que no han rellenado los metadatos en
    // el plazo indicado.
    array(
        'classname' => 'local_lpi\task\hide_expired_modules',
        'blocking' => 0,
        'minute' => '*/5',
        'hour' => '*',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    ),

    // Sends doubt messages to the configured email address.
    array(
        'classname' => 'local_lpi\task\send_doubts_email',
        'blocking' => 0,
        'minute' => '*/1',
        'hour' => '*',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    ),

    // Actions on expired licenses.
    array(
        'classname' => 'local_lpi\task\review_expired_licenses',
        'blocking' => 0,
        'minute' => '0',
        'hour' => '5',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    ),

    // Orphaned files.
    array(
        'classname' => '\local_lpi\task\insert_orphaned_files',
        'blocking' => 0,
        'minute' => '0',
        'hour' => '6',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    )
);
