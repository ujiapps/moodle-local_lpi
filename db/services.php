<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File description
 *
 * @package    xxx_xxxxxx
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

$services = array(

    // This services allows an application to be integrated with the system.
    'LPI Integration Service' => array(
        'shortname' => 'lpi_integration_service',
        'functions' => array(
            'local_lpi_get_pending_files',
            'local_lpi_get_file_metadata',
            'local_lpi_set_file_authorization',
            'local_lpi_get_externally_reviewed_logs'
        ),
        'requiredcapability' => 'local/lpi:addfilemetadata',
        'restrictedusers' => true,
        'downloadfiles' => true,
        'enabled' => false
    ),

    // This service allows a user to audit what have been done.
    'LPI Audit Service' => array(
        'shortname' => 'lpi_audit_service',
        'functions' => array(
            'local_lpi_get_auditable_file_metadata'
        ),
        'requiredcapability' => 'local/lpi:audit',
        'restrictedusers' => true,
        'downloadfiles' => true,
        'enabled' => false
    )
);

$functions = array(

    'local_lpi_get_pending_files' => array(
        'classname' => '\local_lpi\external\integration_service',
        'methodname' => 'get_pending_files',
        'description' => 'Gets the list of files in a course that must be LPI checked',
        'type' => 'read',
        'ajax' => false
    ),

    'local_lpi_get_file_metadata' => array(
        'classname' => '\local_lpi\external\integration_service',
        'methodname' => 'get_file_metadata',
        'description' => 'Gets the metadata associated with a file',
        'type' => 'read',
        'ajax' => false
    ),

    'local_lpi_set_file_authorization' => array(
        'classname' => '\local_lpi\external\integration_service',
        'methodname' => 'set_file_authorization',
        'description' => 'Sets file authorization state',
        'type' => 'write',
        'ajax' => true
    ),

    'local_lpi_get_externally_reviewed_logs' => [
        'classname' => '\local_lpi\external\integration_service',
        'methodname' => 'get_externally_reviewed_logs',
        'description' => 'Gets the logs of the externally reviewed files',
        'type' => 'write',
        'ajax' => false
    ],

    'local_lpi_get_auditable_file_metadata' => array(
        'classname' => '\local_lpi\external\audit_service',
        'methodname' => 'get_auditable_file_metadata',
        'description' => 'Gets the audir for an associated file',
        'type' => 'read',
        'ajax' => false
    ),

    // AJAX calls.

    'local_lpi_get_pending_fileids' => array(
        'classname' => 'local_lpi_external',
        'methodname' => 'get_pending_fileids',
        'classpath' => 'local/lpi/externallib.php',
        'description' => 'Gets the ids of the pending files to review for a course',
        'loginrequired' => true,
        'type' => 'read',
        'ajax' => true
    ),

    'local_lpi_add_doubt_message' => array(
        'classname' => 'local_lpi_external',
        'methodname' => 'add_doubt_message',
        'classpath' => 'local/lpi/externallib.php',
        'description' => 'Inserts a new doubt message into the doubt queue',
        'loginrequired' => true,
        'type' => 'read',
        'ajax' => true
    ),

    'local_lpi_should_notify' => array(
        'classname' => 'local_lpi_external',
        'methodname' => 'should_notify',
        'classpath' => 'local/lpi/externallib.php',
        'description' => 'Returns if should notify the user about pending files to review',
        'loginrequired' => true,
        'type' => 'read',
        'ajax' => true
    )
);
