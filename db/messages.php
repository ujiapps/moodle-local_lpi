<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define the messages.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

$messageproviders = array(

    // Notifies a user that the state of a file has been externally modified.
    \local_lpi\message\fileexternallyreviewed::NAME => array(
        'capability' => 'local/lpi:fileexternallyreviewed'
    ),

    // Notifies a user that a doubt has been sent to the expert.
    \local_lpi\message\doubt_sent_confirmation::NAME => array(
        'capability' => 'local/lpi:notifydoubtsentconfirmation'
    ),

    // Notifies a user about hidden modules.
    \local_lpi\message\hidden_module_confirmation::NAME => array(
        'capability' => 'local/lpi:notifymodulehidden'
    ),

    // Notifies a user when a license has just expired.
    \local_lpi\message\license_expired_confirmation::NAME => array(
        'capability' => 'local/lpi:notifyexpiredlicenses'
    )
);
