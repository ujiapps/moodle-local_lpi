<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade code.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Upgrade code.
 *
 * @global \stdClass $CFG
 * @global \moodle_database $DB
 * @param string $oldversion
 */
function xmldb_local_lpi_upgrade($oldversion) {
    global $CFG, $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2018032100) {

        // Changing type of field comments on table local_lpi_file_metadata to text.
        $table = new xmldb_table('local_lpi_file_metadata');
        $field = new xmldb_field('comments', XMLDB_TYPE_TEXT, null, null, null, null, null, 'timeendlicense');

        // Launch change of type for field comments.
        $dbman->change_field_type($table, $field);

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2018032100, 'local', 'lpi');
    }

    if ($oldversion < 2018042000.01) {
        // Define field timemodified to be added to local_lpi_file_metadata.
        $table = new xmldb_table('local_lpi_file_metadata');
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'timecreated');

        // Conditionally launch add field timemodified.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2018042000.01, 'local', 'lpi');
    }

    if ($oldversion < 2018042000.02) {
        // Set timemodified to be timecreated as default.
        $DB->execute('UPDATE {local_lpi_file_metadata} SET timemodified = timecreated');

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2018042000.02, 'local', 'lpi');
    }

    if ($oldversion < 2018042000.03) {
        // Changing nullability of field timemodified on table local_lpi_file_metadata to not null.
        $table = new xmldb_table('local_lpi_file_metadata');
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'timecreated');

        // Launch change of nullability for field timemodified.
        $dbman->change_field_notnull($table, $field);

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2018042000.03, 'local', 'lpi');
    }

    if ($oldversion < 2018052300) {

        // Define table local_lpi_pseudo_course to be created.
        $table = new xmldb_table('local_lpi_pseudo_course');

        // Adding fields to table local_lpi_pseudo_course.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('pseudocourse', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table local_lpi_pseudo_course.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('courseid_fk', XMLDB_KEY_UNIQUE, array('courseid'));
        $table->add_key('pseudocourseid_key', XMLDB_KEY_UNIQUE, array('pseudocourse'));

        // Conditionally launch create table for local_lpi_pseudo_course.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2018052300, 'local', 'lpi');
    }

    if ($oldversion < 2018070100) {

        // Define table local_lpi_file_metadata_log to be created.
        $table = new xmldb_table('local_lpi_file_metadata_log');

        // Adding fields to table local_lpi_file_metadata_log.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('timelogged', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('metadataid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('property', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('contenthash', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('author', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('license', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('identification', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('state', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('publisher', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('pages', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('totalpages', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('rightsentity', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('timeendlicense', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('comments', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('denyreason', XMLDB_TYPE_CHAR, '400', null, null, null, null);
        $table->add_field('nenrolments', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('lastdoubttime', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table local_lpi_file_metadata_log.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid_fk', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Adding indexes to table local_lpi_file_metadata_log.
        $table->add_index('state_idx', XMLDB_INDEX_NOTUNIQUE, array('state'));

        // Conditionally launch create table for local_lpi_file_metadata_log.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2018070100, 'local', 'lpi');
    }

    if ($oldversion < 2018070101) {
        // Fill the log table with the current state of file_metadata table.
        $now = time();
        $sql = 'INSERT INTO {local_lpi_file_metadata_log} '
                . '(timelogged,metadataid,userid,property,type'
                . ',contenthash,author,license,title,identification'
                . ',courseid,state,timecreated,timemodified,publisher,pages'
                . ',totalpages,rightsentity,timeendlicense,comments,denyreason'
                . ',nenrolments,lastdoubttime) '
                . 'SELECT ' . $now . ', fm.id, fm.userid,fm.property,fm.type'
                . ',contenthash,author,license,title,identification'
                . ',courseid,state,timecreated,timemodified,publisher,pages'
                . ',totalpages,rightsentity,timeendlicense,comments,denyreason'
                . ',nenrolments,lastdoubttime '
                . 'FROM {local_lpi_file_metadata} fm';

        $DB->execute($sql);
    }

    if ($oldversion < 2019120300) {

        // Changing type of field identification on table local_lpi_file_metadata_log to char.
        $table = new xmldb_table('local_lpi_file_metadata_log');
        $field = new xmldb_field('identification', XMLDB_TYPE_CHAR, '1333', null, null, null, null, 'title');

        // Launch change of type for field identification.
        $dbman->change_field_type($table, $field);

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2019120300, 'local', 'lpi');
    }

    if ($oldversion < 2019120301) {

        // Changing precision of field identification on table local_lpi_file_metadata to (1333).
        $table = new xmldb_table('local_lpi_file_metadata');
        $field = new xmldb_field('identification', XMLDB_TYPE_CHAR, '1333', null, null, null, null, 'title');

        // Launch change of precision for field identification.
        $dbman->change_field_precision($table, $field);

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2019120301, 'local', 'lpi');
    }

    if ($oldversion < 2020070602) {
        // Define table local_lpi_file_visibility to be created.
        $table = new xmldb_table('local_lpi_file_visibility');

        // Adding fields to table local_lpi_file_visibility.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('contenthash', XMLDB_TYPE_CHAR, '40', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('visible', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, '8');
        $table->add_field('origincontextid', XMLDB_TYPE_INTEGER, '8', null, null, null, null);

        // Adding keys to table local_lpi_file_visibility.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for local_lpi_file_visibility.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Lpi savepoint reached.
        upgrade_plugin_savepoint(true, 2020070602, 'local', 'lpi');
    }


    return true;
}
