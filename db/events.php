<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

$observers = array(
    // Shows a notification at course page.
    array(
        'eventname' => '\core\event\course_viewed',
        'callback' => '\local_lpi\observers\course_viewed::observe',
        'internal' => false
    ),

    array(
        'eventname' => '\local_lpi\event\file_reviewed',
        'callback' => '\local_lpi\observers\file_reviewed::observe'
    ),

    array(
        'eventname' => '\local_lpi\event\file_externally_reviewed',
        'callback' => '\local_lpi\observers\file_externally_reviewed::observe'
    ),

    array(
        'eventname' => '\local_lpi\event\doubt_sent',
        'callback' => '\local_lpi\observers\doubt_sent::observe'
    ),

    // Sends an email to the teacher.
    array(
        'eventname' => '\local_lpi\event\module_hidden',
        'callback' => '\local_lpi\observers\module_hidden::observe'
    ),

    // Listens for a license expired event and send a message to the user.
    array(
        'eventname' => '\local_lpi\event\license_expired',
        'callback' => '\local_lpi\observers\license_expired::observe'
    ),

    // Update files and visibility tables.
    array(
        'eventname' => '\core\event\course_module_updated',
        'callback' => '\local_lpi\observers\course_module_updated::observe_updated'
    ),
    array(
        'eventname' => '\core\event\course_module_created',
        'callback' => '\local_lpi\observers\course_module_updated::observe_created'
    ),
    array(
        'eventname' => '\core\event\course_module_deleted',
        'callback' => '\local_lpi\observers\course_module_updated::observe_deleted'
    ),

    array(
        'eventname' => '\core\event\course_section_updated',
        'callback' => '\local_lpi\observers\course_section_updated::observe_updated'
    ),
    array(
        'eventname' => '\core\event\course_module_updated',
        'callback' => '\local_lpi\observers\course_module_updated::observe_updated'
    ),
    array(
        'eventname' => '\core\event\course_module_deleted',
        'callback' => '\local_lpi\observers\course_module_updated::observe_deleted'
    )
);
