<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * We can mark a file as ignored in order to avoid it being a candidate for
 * reviewing. Imagine this situation:
 *
 *    * A student submits a file to an assignment.
 *    * The student gets unenrolled from the course.
 *
 * Because the student is not a participant of the course the file still exists
 * and the file will be ellegible por reviewing (ie. a teacher should review
 * that file in order to prevent the assignment from being hidden).
 *
 * The only way I think this can be avoided is to mark the file as ignored and
 * don't use for this.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\orm;

defined('MOODLE_INTERNAL') || die();

class ignored_file {

    /**
     * The file is ignored because it has been uploaded by a student.
     */
    const REASON_STUDENT_FILE = 1;

    private $id;
    private $fileid;
    private $courseid;
    private $timecreated;
    private $reason;

    public function __construct($dbrec = null) {
        if ($dbrec) {
            $this->id = isset($dbrec->id) ? $dbrec->id : null;
            $this->fileid = $dbrec->fileid;
            $this->courseid = $dbrec->courseid;
            $this->timecreated = $dbrec->timecreated;
            $this->reason = $dbrec->reason;
        }
    }

    public function todb() {
        $ret = new \stdClass;
        $ret->id = $this->id;
        $ret->fileid = $this->fileid;
        $ret->courseid = $this->courseid;
        $ret->timecreated = $this->timecreated;
        $ret->reason = $this->reason;
        return $ret;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_fileid() {
        return $this->fileid;
    }

    public function get_courseid() {
        return $this->courseid;
    }

    public function get_reason() {
        return $this->reason;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_fileid($fileid) {
        $this->fileid = $fileid;
    }

    public function set_courseid($courseid) {
        $this->courseid = $courseid;
    }

    public function set_reason($reason) {
        $this->reason = $reason;
    }

    public function get_timecreated() {
        return $this->timecreated;
    }

    public function set_timecreated($timecreated) {
        $this->timecreated = $timecreated;
    }
}
