<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class for mapping local_lpi_file_metadata table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\orm;

defined('MOODLE_INTERNAL') || die();

use local_lpi\objects\document_content;

class file_metadata {

    const PROPERTY_PUBLICDOMAIN = 'publicdomain';
    const PROPERTY_TEACHERS = 'teachers';
    const PROPERTY_UNIVERSITY = 'university';

    /**
     * Unknown state. When no state has been choosen.
     */
    const STATE_UNKNOWN = 'unknown';
    /**
     * I can publish the file because of its type or property.
     */
    const STATE_OK = 'ok';
    /**
     * Less ot equal 10% of the original.
     */
    const STATE_LOE10 = 'loe10';
    /**
     * Greater than 10% of the original.
     */
    const STATE_GT10 = 'gt10';
    /**
     * Greater than 10% of the original but I have permission to publish it.
     */
    const STATE_GT10_AND_PERM = 'gt10perm';
    /**
     * A user has sent a doubt and the previous state was unknown. The user
     * can't set the state to doubt if another state was set before.
     */
    const STATE_DOUBT = 'doubt';

    /**
     * An external entity has forbidden the publication of the document.
     */
    const STATE_FORBIDDEN = 'forbidden';

    /**
     * To diferenciate UNKWNOWN state. A file can be in DONT_APPLY state when
     * TYPE is GRAPHIC or property is set to mine or public domain.
     */
    const STATE_DONT_APPLY = 'dontapply';

    const TYPE_PRINTED_OR_PRINTABLE = 'text';
    const TYPE_PLASTIC = 'plastic';
    const TYPE_NOT_PRINTED_OR_PRINTABLE = 'notprintable';

    // Administrative document. This includes documents with grades, personal
    // data and the like.
    const TYPE_ADMINISTRATIVE = 'administrative';

    private static $allowedproperties = array(
        self::PROPERTY_PUBLICDOMAIN,
        self::PROPERTY_TEACHERS,
        self::PROPERTY_UNIVERSITY
    );

    private static $allowedstates = array(
        self::STATE_UNKNOWN,
        self::STATE_OK,
        self::STATE_LOE10,
        self::STATE_GT10,
        self::STATE_DOUBT,
        self::STATE_FORBIDDEN,
        self::STATE_GT10_AND_PERM,
        self::STATE_DONT_APPLY
    );

    private static $allowedtypes = array(
        self::TYPE_PRINTED_OR_PRINTABLE,
        self::TYPE_PLASTIC
    );

    protected $id;
    private $property;
    private $type;
    private $contenthash;
    private $author;
    private $license;
    private $userid;
    private $courseid;
    private $title;
    private $identification;
    private $state;
    private $timecreated;
    private $timemodified;
    private $publisher;
    private $pages;
    private $totalpages;
    private $rightsentity;
    private $denyreason;

    // License end date.
    private $timeendlicense;

    // Comments added.
    private $comments;

    // Number of students enrolled.
    private $nenrolments;

    // Last time a doubt was sent. We use this in order to limit the time a file
    // can be accesed.
    private $lastdoubttime;

    // All of this fields come from the {files} table.
    private $fileids;
    private $files;

    // This item comes from {course_modules} table. It shows the modules where
    // the file belong.
    private $cmids;
    private $modules;

    // Sections this file belongs to.
    private $sectionnums = [];
    private $sections = [];

    /**
     * Given a database record, construct a file_metadata object.
     *
     * @param \stdClass $r local_lpi_file_metadata table registry.
     */
    public function __construct($r=null) {

        if ($r) {
            $this->id = isset($r->id) ? $r->id : null;
            $this->userid = $r->userid;
            $this->property = $r->property;
            $this->type = $r->type;
            $this->author = $r->author;
            $this->license = $r->license;
            $this->contenthash = $r->contenthash;
            $this->title = $r->title;
            $this->identification = $r->identification;
            $this->courseid = $r->courseid;
            $this->state = $r->state;
            $this->timecreated = $r->timecreated;
            $this->timemodified = $r->timemodified;
            $this->publisher = $r->publisher;
            $this->pages = $r->pages;
            $this->totalpages = $r->totalpages;
            $this->rightsentity = $r->rightsentity;
            $this->timeendlicense = $r->timeendlicense;

            $this->nenrolments = $r->nenrolments;

            $this->comments = $r->comments;

            $this->denyreason = $r->denyreason;
            $this->lastdoubttime = $r->lastdoubttime;
        }

        $this->files = [];
        $this->fileids = [];
        $this->modules = [];
        $this->cmids = [];
    }

    /**
     * Returns an \stdClass() ready to be used as table record. Remember that
     * fields corresponding to the {files} table are not returned. They have
     * to be managed sepparately.
     *
     * @return \stdClass
     */
    public function to_db() {
        $ret = new \stdClass();
        $ret->id = $this->id;
        $ret->property = $this->property;
        $ret->type = $this->type;
        $ret->author = $this->author === null ? '' : $this->author;
        $ret->license = $this->license === null ? '' : $this->license;
        $ret->contenthash = $this->contenthash;
        $ret->title = $this->title;
        $ret->identification = $this->identification;
        $ret->userid = $this->userid;
        $ret->courseid = $this->courseid;
        $ret->state = is_null($this->state) ? self::STATE_UNKNOWN : $this->state;
        $ret->timecreated = $this->timecreated;
        $ret->timemodified = $this->timemodified;
        $ret->publisher = $this->publisher;
        $ret->pages = $this->pages;
        $ret->totalpages = $this->totalpages;
        $ret->rightsentity = $this->rightsentity;
        $ret->timeendlicense = $this->timeendlicense;
        $ret->comments = $this->comments;
        $ret->denyreason = $this->denyreason;
        $ret->nenrolments = $this->nenrolments;
        $ret->lastdoubttime = $this->lastdoubttime;

        return $ret;
    }

    /**
     * Just for debug puposes.
     *
     * @return string a string representation of file_metadata.
     */
    public function __toString() {
        $ret = '';

        $reflection = new \ReflectionClass('\local_uji\file_metadata');
        $properties = $reflection->getProperties();
        foreach ($properties as $p) {
            $ret .= $p->getName() . ': ' . $this->{$p->getName()} .'; ';
        }
        $ret = substr($ret, 0, -2);
        return $ret;
    }

    public function get_state() {
        return $this->state === null ? self::STATE_UNKNOWN : $this->state;
    }

    public function set_state($state) {
        if (!in_array($state, self::$allowedstates)) {
            throw new \coding_exception("Invalid file_metadata state ($state)");
        }
        $this->state = $state;
    }
    public static function get_allowedproperties() {
        return self::$allowedproperties;
    }
    public static function get_allowedstates() {
        return self::$allowedstates;
    }
    public static function get_allowed_types() {
        return self::$allowedtypes;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_contenthash() {
        return $this->contenthash;
    }

    public function get_title() {
        return $this->title;
    }

    public function get_identification() {
        return $this->identification;
    }

    public function get_userid() {
        return $this->userid;
    }

    public function get_courseid() {
        return $this->courseid;
    }

    public function set_id($id) {
        $this->id = $id;
    }


    public function set_contenthash($contenthash) {
        $this->contenthash = $contenthash;
    }

    public function set_title($title) {
        $this->title = $title;
    }

    public function set_identification($identification) {
        $this->identification = $identification;
    }

    public function set_userid($userid) {
        $this->userid = $userid;
    }

    public function set_courseid($courseid) {
        $this->courseid = $courseid;
    }

    public function set_filename($filename) {
        $this->filename = $filename;
    }

    public function get_timecreated() {
        return $this->timecreated;
    }

    public function set_timecreated($timecreated) {
        $this->timecreated = $timecreated;
    }

    public function get_author() {
        return $this->author;
    }

    public function get_license() {
        return $this->license;
    }

    public function set_author($author) {
        $this->author = $author;
    }

    public function set_license($license) {
        $this->license = $license;
    }
    public function get_type() {
        return $this->type;
    }

    public function get_publisher() {
        return $this->publisher;
    }

    public function get_pages() {
        return $this->pages;
    }

    public function set_pages($pages) {
        $this->pages = $pages;
    }

    public function get_totalpages() {
        return $this->totalpages;
    }

    public function set_type($type) {
        $this->type = $type;
    }

    public function set_publisher($publisher) {
        $this->publisher = $publisher;
    }

    public function set_totalpages($totalpages) {
        $this->totalpages = $totalpages;
    }

    public function get_rightsentity() {
        return $this->rightsentity;
    }

    public function set_rightsentity($rightsentity) {
        $this->rightsentity = $rightsentity;
    }

    public function get_property() {
        return $this->property;
    }

    public function set_property($property) {
        $this->property = $property;
    }

    public function get_timeendlicense() {
        return $this->timeendlicense;
    }

    public function set_timeendlicense($timeendlicense) {
        $this->timeendlicense = $timeendlicense;
    }

    public function get_comments() {
        return $this->comments;
    }

    public function set_comments($comments) {
        $this->comments = $comments;
    }

    public function get_denyreason() {
        return $this->denyreason;
    }

    public function set_denyreason($denyreason) {
        $this->denyreason = $denyreason;
    }

    /**
     * Return an array of stored_files corresponding to this metadata.
     *
     * @return \stored_file[]
     */
    public function get_files() {
        if (!$this->files) {
            $fs = get_file_storage();
            $this->files = array_map(function($fileid) use ($fs) {
                return $fs->get_file_by_id($fileid);
            }, $this->fileids);
        }
        return $this->files;
    }

    /**
     * Modules info.
     *
     * @return \cm_info[]
     */
    public function get_modules() {
        if (!$this->modules) {
            $this->modules = array();
            $modinfo = get_fast_modinfo($this->courseid);
            foreach ($this->cmids as $cmid) {
                try {
                    $this->modules[] = $modinfo->get_cm($cmid);
                } catch (\moodle_exception $e) {
                    continue;
                }
            }
        }
        return $this->modules;
    }

    public function set_files($files) {
        $this->files = $files;
    }

    public function set_modules($modules) {
        $this->modules = $modules;
    }
    public function get_fileids() {
        return $this->fileids;
    }

    public function get_cmids() {
        return $this->cmids;
    }

    public function set_fileids($fileids) {
        $this->fileids = $fileids;
    }

    public function set_cmids($cmids) {
        $this->cmids = $cmids;
    }

    public function get_sectionnums() {
        return $this->sectionnums;
    }

    public function set_sectionnums($sectionids) {
        $this->sectionnums = $sectionids;
    }

    /**
     * Returns info about the sections where this file belongs. If it belongs to
     * a course module, its sections is excluded.
     *
     * @return section_info[]
     */
    public function get_sections() {
        if (!$this->sections) {
            $this->sections = [];
            $modinfo = get_fast_modinfo($this->courseid);
            foreach ($this->sectionnums as $sectionnum) {
                try {
                    $this->sections[] = $modinfo->get_section_info($sectionnum);
                } catch (\moodle_exception $e) {
                    continue;
                }
            }

        }
        return $this->sections;
    }

    public function get_nenrolments() {
        return $this->nenrolments;
    }

    public function set_nenrolments($nenrolments) {
        $this->nenrolments = $nenrolments;
    }

    public function get_lastdoubttime() {
        return $this->lastdoubttime;
    }

    public function set_lastdoubttime($lastdoubttime) {
        $this->lastdoubttime = $lastdoubttime;
    }

    public function get_timemodified() {
        return $this->timemodified;
    }

    public function set_timemodified($timemodified) {
        $this->timemodified = $timemodified;
    }

    /**
     * Sets the file_metadata fields to match the document_content object.
     *
     * @param document_content $dc
     */
    public function set_document_content(document_content $dc) {
        $dc->update_file_metadata($this);
    }
    /**
     * Returns a document_content instance that this file_metadata contains.
     *
     * @return document_content
     */
    public function get_document_content() {
        return document_content::get_document_content_by_file_metadata($this);
    }
}
