<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Represents a doubt sent to an specialist in intellectual property.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\orm;

defined('MOODLE_INTERNAL') || die();

class doubt {

    private $id;
    private $timecreated;
    private $timesent;
    private $userid;
    private $courseid;
    private $message;
    private $messageformat;
    private $contenthash;

    /**
     * Files associated with the doubt.
     * @var \stored_file[]
     */
    private $files;

    private $course;


    public function __construct($dbrec = null) {
        if ($dbrec) {
            $this->id = isset($dbrec->id) ? $dbrec->id : null;
            $this->timecreated = $dbrec->timecreated;
            $this->timesent = ($dbrec->timesent) ? 1 : 0;
            $this->userid = $dbrec->userid;
            $this->courseid = $dbrec->courseid;
            $this->message = $dbrec->message;
            $this->messageformat = $dbrec->messageformat;
            $this->contenthash = $dbrec->contenthash;

            $this->files = null;
            $this->course = null;
        }
    }

    public function to_db() {
        $ret = new \stdClass();
        $ret->id = $this->id;
        $ret->timecreated = $this->timecreated;
        $ret->timesent = $this->timesent;
        $ret->userid = $this->userid;
        $ret->courseid = $this->courseid;
        $ret->message = $this->message;
        $ret->messageformat = $this->messageformat;
        $ret->contenthash = $this->contenthash;
        return $ret;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_timecreated() {
        return $this->timecreated;
    }

    public function get_userid() {
        return $this->userid;
    }

    public function get_message() {
        return $this->message;
    }

    public function get_messageformat() {
        return $this->messageformat;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_timecreated($timecreated) {
        $this->timecreated = $timecreated;
    }

    public function set_userid($userid) {
        $this->userid = $userid;
    }

    public function set_message($message) {
        $this->message = $message;
    }

    public function set_messageformat($messageformat) {
        $this->messageformat = $messageformat;
    }

    public function get_timesent() {
        return $this->timesent;
    }

    public function set_timesent($timesent) {
        $this->timesent = $timesent;
    }
    public function get_courseid() {
        return $this->courseid;
    }

    public function set_courseid($courseid) {
        $this->courseid = $courseid;
    }

    public function get_contenthash() {
        return $this->contenthash;
    }

    public function set_contenthash($contenthash) {
        $this->contenthash = $contenthash;
    }

    /**
     * Returns an array of stored_files
     * @return \stored_file[]
     */
    public function get_files() {
        if ($this->files === null) {
            $manager = \local_lpi\manager::get_instance();
            $filemetadata = $manager->get_file_metadata_by_contenthash($this->courseid, $this->contenthash);
            // Can be null if the file has been deleted :-(
            if ($filemetadata) {
                $this->files = $filemetadata->get_files();
            }
        }
        return $this->files;
    }

    /**
     *
     * @global \moodle_database $DB
     */
    public function get_course() {
        global $DB;
        if ($this->course === null) {
            $this->course = $DB->get_record('course', array('id' => $this->courseid), 'id, fullname');
        }
        return $this->course;
    }
}
