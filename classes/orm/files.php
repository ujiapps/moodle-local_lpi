<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A model for local_lpi_files table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\orm;

defined('MOODLE_INTERNAL') || die();

class files {
    private $id;
    private $metadataid;
    private $fileid;
    private $filename;
    private $userid;
    private $timecreated;

    public function __construct($db = null) {
        if ($db) {
            $this->id = isset($db->id) ? $db->id : null;
            $this->metadataid = $db->metadataid;
            $this->fileid = $db->fileid;
            $this->filename = $db->filename;
            $this->userid = $db->userid;
            $this->timecreated = $db->timecreated;
        }
    }

    public function to_db() {
        $ret = new \stdClass();
        $ret->id = $this->id;
        $ret->metadataid = $this->metadataid;
        $ret->fileid = $this->fileid;
        $ret->filename = $this->filename;
        $ret->userid = $this->userid;
        $this->timecreated = $this->timecreated;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_metadataid() {
        return $this->metadataid;
    }

    public function get_userid() {
        return $this->userid;
    }

    public function get_timecreated() {
        return $this->timecreated;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_metadataid($metadataid) {
        $this->metadataid = $metadataid;
    }

    public function set_userid($userid) {
        $this->userid = $userid;
    }

    public function set_timecreated($timecreated) {
        $this->timecreated = $timecreated;
    }

    public function get_fileid() {
        return $this->fileid;
    }

    public function set_fileid($fileid) {
        $this->fileid = $fileid;
    }

}
