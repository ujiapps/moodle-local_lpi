<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class for mapping local_lpi_file_metadata table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\orm;

defined('MOODLE_INTERNAL') || die();

class file_metadata_log extends file_metadata {

    /**
     * The timestamp of the logged change.
     * @var int
     */
    private $timelogged;
    /**
     * The id of the metadata.
     * @var int
     */
    private $metadataid;

    public function __construct(file_metadata $filemetadata) {
        parent::__construct($filemetadata->to_db());

        $this->id = null;
        $this->timelogged = time();
        $this->metadataid = $filemetadata->get_id();
    }

    public function to_db() {
        $r = parent::to_db();
        $r->timelogged = $this->timelogged;
        $r->metadataid = $this->metadataid;
        return $r;
    }

    public function get_timelogged() {
        return $this->timelogged;
    }

    public function get_metadataid() {
        return $this->metadataid;
    }

}
