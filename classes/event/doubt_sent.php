<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This event is triggered when the doubt written by a user is sent to the
 * external entity.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\event;

defined('MOODLE_INTERNAL') || die();

class doubt_sent extends \core\event\base {
    /**
     * Creates the event from a doubt.
     *
     * @param \local_lpi\orm\doubt $doubt
     * @return doubt_sent
     */
    public static function create_from_doubt (\local_lpi\orm\doubt $doubt) {
        return self::create(array(
            'objectid' => $doubt->get_id(),
            'context' => \context_course::instance($doubt->get_courseid()),
            'relateduserid' => $doubt->get_userid(),
            'other' => array(
                'contenthash' => $doubt->get_contenthash()
            )
        ));
    }

    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'local_lpi_doubt_queue';
    }
}
