<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An event triggerd when a module has been hidden automatically.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\event;

defined('MOODLE_INTERNAL') || die();

class module_hidden extends \core\event\base {

    /**
     * Given a coursemodule id and a userid returns an instance of module_hidden
     * event.
     *
     * @param int $cmid
     * @param int $fileid
     * @param int $userid
     * @return module_hidden
     */
    public static function create_from_cmid ($courseid, $cmid, $fileid, $userid) {
        return self::create(array(
            'objectid' => $cmid,
            'context' => \context_module::instance($cmid),
            'relateduserid' => $userid,
            'other' => array(
                'fileid' => $fileid,
                'courseid' => $courseid
            )
        ));
    }

    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'course_modules';
    }
}
