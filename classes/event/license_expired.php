<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A license for a file has expired.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\event;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class license_expired extends \core\event\base {

    public static function create_from_file_metadata (file_metadata $fm) {
        return self::create(array(
            'objectid' => $fm->get_id(),
            'context' => \context_course::instance($fm->get_courseid()),
            'relateduserid' => $fm->get_userid(),
            'other' => array(
                'metadataid' => $fm->get_id(),
                'fileid' => $fm->get_files[0]->get_id(),
                'filename' => $fm->get_filename(),
                'modulename' => $fm->get_cminfo()->name
            )
        ));
    }

    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'local_lpi_file_metadata';
    }
}
