<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A class to manage ignored files.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\ignored_file;
use local_lpi\config;

class ignored_files_manager {

    private static $instance = null;

    private $timecreatedfrom;

    /**
     * Returns a singleton instance.
     *
     * @return ignored_files_manager
     */
    public static function get_instante() {
        if (self::$instance === null) {
            self::$instance = new self(config::get_instance()->get_datefrom());
        }
        return self::$instance;
    }

    private function __construct($timecreatedfrom) {
        $this->timecreatedfrom = $timecreatedfrom;
    }

    /**
     * Returns a query to get ignored files for a given course.
     *
     * @param int $courseid the courseif to test or an external field (c.id)
     * @param string $courseidalias the alias to obtain the courseid.
     * @return string[]
     */
    public function get_ignored_files_sql($courseid, $courseidalias = "c") {
        $select = 'SELECT ifiles.fileid ';
        $from = 'FROM {local_lpi_ignored_files} ifiles ';

        if ($courseid !== null) {
            $where = 'WHERE ifiles.courseid = :ignoredcourseid ';
            $params = array(
                'ignoredcourseid' => $courseid
            );
        } else {
            $where = "WHERE ifiles.courseid = $courseidalias.id";
            $params = array();
        }

        return [$select . $from . $where, $params];
    }

    /**
     * Adds a new file to the list of ignored files.
     *
     * @global \moodle_database $DB
     * @param ignored_file $ignoredfile
     */
    public function add(ignored_file $ignoredfile) {
        global $DB;
        $dbrec = $ignoredfile->todb();
        $id = $DB->insert_record('local_lpi_ignored_files', $dbrec, true);
        $ignoredfile->set_id($id);
    }

    /**
     * Inserts ignored files for a given course.
     *
     * @param int $courseid
     */
    public function update_ignored_files ($courseid, $reason = ignored_file::REASON_STUDENT_FILE) {

        global $DB;

        if ($reason === ignored_file::REASON_STUDENT_FILE) {

            // Insert the missing files.
            $context = \context_course::instance($courseid);
            list($enrolsql, $enrolparams) = get_enrolled_sql($context, 'local/lpi:countasstudent');

            $sql = 'SELECT '
                    . 'f.id as fileid, '
                    . 'cm.course as courseid, '
                    . ':now as timecreated, '
                    . ':reasonstudent as reason '

                    . 'FROM {files} f '
                    . '     JOIN {context} ctx ON f.contextid = ctx.id and ctx.contextlevel = :contextmodule '
                    . '     JOIN {course_modules} cm ON ctx.instanceid = cm.id '
                    . '     JOIN {course} c ON cm.course = c.id '
                    . '     LEFT JOIN {local_lpi_ignored_files} ifiles ON f.id = ifiles.fileid AND ifiles.courseid = c.id '

                    . 'WHERE f.filename != :filenamedir '
                    . '      AND c.timecreated >= :timecreatedfrom '
                    . '      AND cm.deletioninprogress = :notdeletioninprogress '
                    . '      AND ifiles.id IS NULL '
                    . '      AND cm.course = :courseid '
                    . '      AND f.userid IN (' . $enrolsql . ')';

            $params = array(
                'timecreatedfrom' => $this->timecreatedfrom,
                'filenamedir' => '.',
                'notdeletioninprogress' => 0,
                'now' => time(),
                'reasonstudent' => ignored_file::REASON_STUDENT_FILE,
                'contextmodule' => CONTEXT_MODULE,
                'courseid' => $courseid
            );
            $params = array_merge($params, $enrolparams);

            $DB->execute(
                    "INSERT INTO {local_lpi_ignored_files} (fileid, courseid, timecreated, reason) " . $sql,
                    $params
            );

            // Remove the files if the userid is not a student and is enrolled
            // into the course. First test if there are such files and then
            // do the deletion, it seems to be quite more efficient.

            $todeletesql = 'SELECT f.id'
                    . '   FROM {files} f '
                    . '        JOIN {context} ctx ON f.contextid = ctx.id AND ctx.contextlevel = :contextmodule '
                    . '        JOIN {course_modules} cm ON ctx.instanceid = cm.id '
                    . '        JOIN {course} c ON cm.course = c.id '
                    . '        JOIN {enrol} e ON c.id = e.courseid '
                    . '        JOIN {user_enrolments} ue ON ue.enrolid = e.id AND f.userid = ue.userid '
                    . '        JOIN {local_lpi_ignored_files} ifiles ON ifiles.fileid = f.id AND ifiles.courseid = c.id '
                    . 'WHERE f.filename != :filenamedir '
                    . '      AND c.timecreated >= :timecreatedfrom '
                    . '      AND cm.deletioninprogress = :notdeletioninprogress '
                    . '      AND c.id = :courseid '
                    . '      AND f.userid NOT IN ( ' . $enrolsql . ' ) ';

            $params = array(
                'timecreatedfrom' => $this->timecreatedfrom,
                'filenamedir' => '.',
                'courseid' => $courseid,
                'contextmodule' => CONTEXT_MODULE,
                'notdeletioninprogress' => 0
            );
            $params = array_merge($params, $enrolparams);

            if ( $DB->record_exists_sql($todeletesql, $params) ) {

                $sql = 'DELETE FROM {local_lpi_ignored_files} '

                        . 'WHERE fileid IN ('
                        . 'SELECT f.id'
                        . '   FROM {files} f '
                        . '        JOIN {context} ctx ON f.contextid = ctx.id AND ctx.contextlevel = :contextmodule '
                        . '        JOIN {course_modules} cm ON ctx.instanceid = cm.id '
                        . '        JOIN {course} c ON cm.course = c.id '
                        . '        JOIN {enrol} e ON c.id = e.courseid '
                        . '        JOIN {user_enrolments} ue ON ue.enrolid = e.id AND f.userid = ue.userid '
                        . 'WHERE f.filename != :filenamedir '
                        . '      AND c.timecreated >= :timecreatedfrom '
                        . '      AND cm.deletioninprogress = :notdeletioninprogress '
                        . '      AND c.id = :courseid '
                        . '      AND f.userid NOT IN ( ' . $enrolsql . ' ) '
                        . ')';

                $DB->execute($sql, $params);
            }

        }

    }

}

