<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A class to abstract the idea of document contents. This is the first field
 * of the detailed review form.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\objects;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class document_content {

    // Possible values for document_content.
    const ADMINISTRATIVE = 'administrative';
    const OWNED_BY_TEACHERS = 'ownedbyteachers';
    const EDITED_BY_UNIVERSITY = 'editedbyuniversity';
    const PUBLIC_DOMAIN = 'publicdomain';
    const LT10_FRAGMENT = 'lt10fragment';
    const HAVELICENSE = 'havelicense';
    const OTHER = 'other';
    const NOT_PRINTED_OR_PRINTABLE = 'notprintable';

    const UNKNOWN = 'unknown';

    private $value;

    private static $allowedvalues = array(
        self::NOT_PRINTED_OR_PRINTABLE => self::NOT_PRINTED_OR_PRINTABLE,
        self::ADMINISTRATIVE => self::ADMINISTRATIVE,
        self::OWNED_BY_TEACHERS => self::OWNED_BY_TEACHERS,
        self::EDITED_BY_UNIVERSITY => self::EDITED_BY_UNIVERSITY,
        self::PUBLIC_DOMAIN => self::PUBLIC_DOMAIN,
        self::HAVELICENSE => self::HAVELICENSE,
        self::LT10_FRAGMENT => self::LT10_FRAGMENT,
        self::OTHER => self::OTHER,
        self::UNKNOWN => self::UNKNOWN
    );

    public static function get_allowed_values() {
        return self::$allowedvalues;
    }

    /**
     * Returns a document_content from a file_metadata instance.
     *
     * @param file_metadata $filemetadata
     * @return document_content
     */
    public static function get_document_content_by_file_metadata (file_metadata $filemetadata) {
        return self::get_document_content_by_params(
            $filemetadata->get_state(),
            $filemetadata->get_type(),
            $filemetadata->get_property()
        );
    }

    /**
     * Gets the document content based on specific filemetadata fields.
     *
     * @param string $state the state field in filemetadata.
     * @param string $type the type field in filemetadata.
     * @param string $property the property field in filemetadata
     * @return \self
     */
    public static function get_document_content_by_params ($state, $type, $property) {
        if ($state === file_metadata::STATE_UNKNOWN ||
            $state === null ||
            $state === file_metadata::STATE_DOUBT) {
            $value = self::UNKNOWN;
        } else if ($type === file_metadata::TYPE_NOT_PRINTED_OR_PRINTABLE) {
            $value = self::NOT_PRINTED_OR_PRINTABLE;
        } else if ($property === file_metadata::PROPERTY_TEACHERS) {
            $value = self::OWNED_BY_TEACHERS;
        } else if ($property === file_metadata::PROPERTY_UNIVERSITY) {
            $value = self::EDITED_BY_UNIVERSITY;
        } else if ($property === file_metadata::PROPERTY_PUBLICDOMAIN) {
            $value = self::PUBLIC_DOMAIN;
        } else if ($type === file_metadata::TYPE_ADMINISTRATIVE) {
            $value = self::ADMINISTRATIVE;
        } else if ($state === file_metadata::STATE_LOE10) {
            $value = self::LT10_FRAGMENT;
        } else if ($state === file_metadata::STATE_GT10_AND_PERM) {
            $value = self::HAVELICENSE;
        } else if ($state === file_metadata::STATE_GT10) {
            $value = self::OTHER;
        } else if ($state === file_metadata::STATE_FORBIDDEN) {
            $value = self::UNKNOWN;
        } else {
            $value = self::UNKNOWN;
        }
        return new self($value);
    }

    /**
     * Constructor.
     *
     * @param string $value One of the allowed values.
     * @throws \local_lpi\exception\invalid_authorization_exception
     */
    public function __construct($value) {
        if (!in_array($value, $this->get_allowed_values())) {
            throw new \local_lpi\exception\invalid_authorization_exception($value);
        }
        $this->value = $value;
    }

    public function __toString() {
        return (string) $this->value;
    }

    /**
     * Updates the type, state and property fields of file_metadata based on the
     * value of this instance.
     * @param file_metadata $filemetadata
     */
    public function update_file_metadata(file_metadata $filemetadata) {

        switch ($this->value) {
            case self::ADMINISTRATIVE:
                $filemetadata->set_type(file_metadata::TYPE_ADMINISTRATIVE);
                $filemetadata->set_property(null);
                $filemetadata->set_state(file_metadata::STATE_OK);
                break;
            case self::EDITED_BY_UNIVERSITY:
                $filemetadata->set_type(file_metadata::TYPE_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(file_metadata::PROPERTY_UNIVERSITY);
                $filemetadata->set_state(file_metadata::STATE_OK);
                break;
            case self::HAVELICENSE:
                $filemetadata->set_type(file_metadata::TYPE_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(null);
                $filemetadata->set_state(file_metadata::STATE_GT10_AND_PERM);
                break;
            case self::LT10_FRAGMENT:
                $filemetadata->set_type(file_metadata::TYPE_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(null);
                $filemetadata->set_state(file_metadata::STATE_LOE10);
                break;
            case self::OTHER:
                $filemetadata->set_type(file_metadata::TYPE_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(null);
                $filemetadata->set_state(file_metadata::STATE_GT10);
                break;
            case self::OWNED_BY_TEACHERS:
                $filemetadata->set_type(file_metadata::TYPE_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(file_metadata::PROPERTY_TEACHERS);
                $filemetadata->set_state(file_metadata::STATE_OK);
                break;
            case self::PUBLIC_DOMAIN:
                $filemetadata->set_type(file_metadata::TYPE_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(file_metadata::PROPERTY_PUBLICDOMAIN);
                $filemetadata->set_state(file_metadata::STATE_OK);
                break;
            case self::NOT_PRINTED_OR_PRINTABLE:
                $filemetadata->set_type(file_metadata::TYPE_NOT_PRINTED_OR_PRINTABLE);
                $filemetadata->set_property(null);
                $filemetadata->set_state(file_metadata::STATE_OK);
                break;
        }
    }

    public function get_value() {
        return $this->value;
    }

    public function set_value($value) {
        $this->value = $value;
    }
}


