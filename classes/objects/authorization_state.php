<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authorization states.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\objects;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class authorization_state {

    const AUTHORIZATION_NOT_PROCESSED_YET = 1;
    const AUTHORIZATION_WAITING_FOR_LICENSE = 2;
    const AUTHORIZATION_DOUBT = 3;
    const AUTHORIZATION_NOT_NECESSARY_HAVEAUTH = 4;
    const AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN = 5;
    const AUTHORIZATION_NOT_NECESSARY_LT10 = 6;
    const AUTHORIZATION_HAVE_LICENSE = 7;
    const AUTHORIZATION_NOT_AUTHORIZED = 8;
    const AUTHORIZATION_NOT_NECESSARY_TEACHERS = 9;
    const AUTHORIZATION_NOT_NECESSARY_UNIVERSITY = 10;
    const AUTHORIZATION_NOT_NECESSARY_ADMINISTRATIVE = 11;
    const AUTHORIZATION_NOTPRINTABLE = 12;

    private static $statetostring = array(
        self::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH => array('authorization_notnecessary', 'authorization_notnecessary_haveauthz'),
        self::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN => array('authorization_notnecessary', 'authorization_notnecessary_pd'),
        self::AUTHORIZATION_NOT_NECESSARY_LT10 => array('authorization_notnecessary', 'authorization_notnecessary_lt10_desc'),
        self::AUTHORIZATION_HAVE_LICENSE => array('authorization_license', 'authorization_license_desc'),
        self::AUTHORIZATION_WAITING_FOR_LICENSE => array('authorization_waiting', 'authorization_waiting_desc'),
        self::AUTHORIZATION_DOUBT => array('authorization_query', 'authorization_query_desc'),
        self::AUTHORIZATION_NOT_AUTHORIZED => array('authorization_notauthz', 'authorization_notauthz_desc'),
        self::AUTHORIZATION_NOT_PROCESSED_YET => array('authorization_notprocessedyet', 'authorization_notprocessedyet_desc'),
        self::AUTHORIZATION_NOT_NECESSARY_TEACHERS => array('authorization_notnecessary', 'authorization_notnecessary_teachers'),
        self::AUTHORIZATION_NOT_NECESSARY_UNIVERSITY => ['authorization_notnecessary', 'authorization_notnecessary_university'],
        self::AUTHORIZATION_NOT_NECESSARY_ADMINISTRATIVE => array('authorization_notnecessary', 'authorization_notnecessary_administrative'),
        self::AUTHORIZATION_NOTPRINTABLE => array('authorization_notprintable', 'authorization_notprintable')
    );

    private $value;

    public function __construct($value) {
        $this->value = $value;
    }

    public function __toString() {
        return (string) $this->value;
    }

    /**
     * Returns an array with all possible authorization state instances.
     *
     * @return array
     */
    public static function get_all_authorization_state_instances() {
        return array(
            new authorization_state(self::AUTHORIZATION_NOT_PROCESSED_YET),
            new authorization_state(self::AUTHORIZATION_WAITING_FOR_LICENSE),
            new authorization_state(self::AUTHORIZATION_DOUBT),
            new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH),
            new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN),
            new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_LT10),
            new authorization_state(self::AUTHORIZATION_HAVE_LICENSE),
            new authorization_state(self::AUTHORIZATION_NOT_AUTHORIZED),
            new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_TEACHERS),
            new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_UNIVERSITY),
            new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_ADMINISTRATIVE),
            new authorization_state(self::AUTHORIZATION_NOTPRINTABLE)
        );
    }

    /**
     * Gets the authorization state for a given file_metadata.
     *
     * @param file_metadata $filemetadata
     * @return authorization_state The authorization state of this file_metadata
     */
    public static function get_authorization_state (file_metadata $filemetadata) {
        $value = null;

        if ($filemetadata->get_type() === file_metadata::TYPE_NOT_PRINTED_OR_PRINTABLE) {
            $value = self::AUTHORIZATION_NOTPRINTABLE;
        } else if ($filemetadata->get_type() === file_metadata::TYPE_ADMINISTRATIVE) {
            $value = self::AUTHORIZATION_NOT_NECESSARY_ADMINISTRATIVE;
        } else if ($filemetadata->get_property() === file_metadata::PROPERTY_PUBLICDOMAIN) {
            $value = self::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN;
        } else if ($filemetadata->get_property() === file_metadata::PROPERTY_TEACHERS) {
            $value = self::AUTHORIZATION_NOT_NECESSARY_TEACHERS;
        } else if ($filemetadata->get_property() === file_metadata::PROPERTY_UNIVERSITY) {
            $value = self::AUTHORIZATION_NOT_NECESSARY_UNIVERSITY;
        } else {
            switch ($filemetadata->get_state()) {
                case file_metadata::STATE_DOUBT:
                    $value = self::AUTHORIZATION_NOT_PROCESSED_YET;
                    break;

                case file_metadata::STATE_LOE10:
                    if ($filemetadata->get_property() === file_metadata::PROPERTY_PUBLICDOMAIN) {
                        $value = self::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN;
                    } else {
                        $value = self::AUTHORIZATION_NOT_NECESSARY_LT10;
                    }
                    break;

                case file_metadata::STATE_GT10_AND_PERM:
                    $value = self::AUTHORIZATION_HAVE_LICENSE;
                    break;

                case file_metadata::STATE_FORBIDDEN:
                    $value = self::AUTHORIZATION_NOT_AUTHORIZED;
                    break;

                case file_metadata::STATE_GT10:
                    $value = self::AUTHORIZATION_WAITING_FOR_LICENSE;
                    break;
            }
        }
        return $value;
    }

    public static function get_authorization_state_string (file_metadata $filemetadata) {
        $value = self::get_authorization_state($filemetadata);
        return self::authzstate_to_string($value);
    }

    public static function authzstate_to_string ($authzstate) {
        if ($authzstate === null) {
            $authzstate = self::AUTHORIZATION_NOT_PROCESSED_YET;
        }
        if (array_key_exists($authzstate, self::$statetostring)) {
            $name = get_string(self::$statetostring[$authzstate][0], 'local_lpi');
            $desc = get_string(self::$statetostring[$authzstate][1], 'local_lpi');
        } else {
            $name = '';
            $desc = '';
        }
        return array($name, $desc);
    }

    public static function get_authorization_state_string_code($authzstate) {
        if ($authzstate === null) {
            $authzstate = self::AUTHORIZATION_NOT_PROCESSED_YET;
        }
        if (array_key_exists($authzstate, self::$statetostring)) {
            $name = self::$statetostring[$authzstate][0];
            $desc = self::$statetostring[$authzstate][1];
        } else {
            $name = '';
            $desc = '';
        }
        return array($name, $desc);
    }

    public static function get_authorization_states_from_authz_filters($authzstatefilters) {
        $authzstates = array();
        foreach ($authzstatefilters as $stfilter => $value) {
            if (!$value) {
                continue;
            }
            switch ($stfilter) {
                case \local_lpi\review_filters::FILTER_AUTHZ_NOT_PROCESSED_YET:
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_PROCESSED_YET);
                    break;

                case \local_lpi\review_filters::FILTER_AUTHZ_AUTHORIZED:
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_DOUBT);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_HAVE_LICENSE);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_LT10);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_ADMINISTRATIVE);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_TEACHERS);
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_NECESSARY_UNIVERSITY);
                    break;

                case \local_lpi\review_filters::FILTER_AUTHZ_NOT_AUTHORIZED:
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOT_AUTHORIZED);
                    break;

                case \local_lpi\review_filters::FILTER_AUTHZ_WAITING_FOR_LICENSE:
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_WAITING_FOR_LICENSE);
                    break;

                case \local_lpi\review_filters::FILTER_AUTHZ_NOT_PRINTABLE:
                    $authzstates[] = new authorization_state(self::AUTHORIZATION_NOTPRINTABLE);
                    break;
            }
        }
        return $authzstates;
    }
}
