<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An interface to the ignored courses table.
 *
 * @package    local_lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi;

defined('MOODLE_INTERNAL') || die;

class ignored_courses_manager {

    private static $instance;

    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new ignored_courses_manager();
        }
        return self::$instance;
    }

    private function __construct() {
    }

    /**
     * Marks a course as ignored.
     *
     * @param int $courseid
     * @global \moodle_database $BD
     * @throws exception\course_doesnt_exist_exception
     */
    public function mark_as_ignored ($courseid) {
        global $DB;
        if (!$DB->record_exists('course', array('id' => $courseid))) {
            throw new exception\course_doesnt_exist_exception($courseid);
        }
        $DB->insert_record('local_lpi_ignored_courses', array('courseid' => $courseid));
    }

    /**
     * Returns an array of ignored courses.
     */
    public function get_ignored_courses () {
        global $DB;

        $sql = "SELECT c.id, c.shortname, c.fullname "
                . "FROM {course} c"
                . "     JOIN {local_lpi_ignored_courses} ic ON ic.courseid = c.id "
                . "ORDER BY c.id";

        return $DB->get_records_sql($sql);
    }

}