<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Description
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi;

defined('MOODLE_INTERNAL') || die();

class review_filters {

    const FILTER_VISIBILITY_ALL = 'all';
    const FILTER_VISIBILITY_HIDDEN = 'hidden';
    const FILTER_VISIBILITY_VISIBLES = 'visibles';

    const FILTER_AUTHZ_NOT_PROCESSED_YET = 'notprocessedyet';
    const FILTER_AUTHZ_WAITING_FOR_LICENSE = 'waitingforlicense';
    const FILTER_AUTHZ_AUTHORIZED = 'authorized';
    const FILTER_AUTHZ_NOT_AUTHORIZED = 'forbidden';
    const FILTER_AUTHZ_NOT_PRINTABLE = 'notprintable';

    const FILTER_TOPIC_ALL = 'gdpi:alltopics';

    const FILTER_PER_PAGE = 10;

    public static function get_visibility_filter() {
        global $SESSION;
        return isset($SESSION->local_lpi_filter_visibility) ? $SESSION->local_lpi_filter_visibility : self::FILTER_VISIBILITY_ALL;
    }

    public static function set_visibility_filter($value) {
        global $SESSION;
        $SESSION->local_lpi_filter_visibility = $value;
    }

    public static function get_authz_filter_values() {
        global $SESSION;

        $filters = array(
            self::FILTER_AUTHZ_NOT_PROCESSED_YET => 1,
            self::FILTER_AUTHZ_NOT_AUTHORIZED => 1,
            self::FILTER_AUTHZ_WAITING_FOR_LICENSE => 1,
            self::FILTER_AUTHZ_AUTHORIZED => 0,
            self::FILTER_AUTHZ_NOT_PRINTABLE => 0
        );
        $ret = array();
        foreach ($filters as $filter => $defaultvalue) {
            $param = 'local_lpi_filter_authz_' . $filter;
            if (!isset($SESSION->$param)) {
                $SESSION->$param = $defaultvalue;
            }
            $ret[$filter] = $SESSION->$param;
        }
        return $ret;
    }

    public static function set_authz_filter_values ($name, $value) {
        global $SESSION;
        $param = 'local_lpi_filter_authz_' . $name;
        $SESSION->$param = $value ? 1 : 0;
    }

    public static function get_topic_filter() {
        global $SESSION;
        return isset($SESSION->local_lpi_filter_topic) ? $SESSION->local_lpi_filter_topic : null;
    }
    public static function set_topic_filter($topic) {
        global $SESSION;
        $SESSION->local_lpi_filter_topic = $topic;
    }

    public static function get_perpage_filter() {
        return \get_user_preferences('local_lpi_table_perpage', 10);
    }
    public static function set_perpage_filter($perpage) {
        \set_user_preference('local_lpi_table_perpage', $perpage);
    }

    public static function get_filename_filter() {
        global $SESSION;
        return isset($SESSION->local_lpi_filename_filter) ? trim($SESSION->local_lpi_filename_filter) : null;
    }
    public static function set_filename_filter($filename) {
        global $SESSION;
        $SESSION->local_lpi_filename_filter = trim($filename);
    }

    public static function filename_filter_to_like($filenamefilter) {
        if (is_null($filenamefilter) or !trim($filenamefilter)) {
            return null;
        }
        $like = str_replace('*', '%', $filenamefilter);
        $like = str_replace('?', '_', $like);
        return $like;
    }
}
