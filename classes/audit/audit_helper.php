<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A helper to get information for the audit external service.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\audit;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class audit_helper {

    const MAX_LIMITNUM = 100;
    const SALT_LENGTH = 20;

    private static $instance;

    /**
     * A salt used to pseudonymize courseid.
     *
     * @var string
     */
    private $pseudosalt;

    /**
     * Returns the singleton instance of the audit_helper.
     *
     * @return audit_helper
     */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new audit_helper();
        }
        return self::$instance;
    }

    /**
     * Contructor. Generates and sotres a courseid salt for courseid pseudonymisation.
     */
    private function __construct() {
        $config = \local_lpi\config::get_instance();
        $this->pseudosalt = $config->get_courseidsalt();
        if (!$this->pseudosalt) {
            $salt = bin2hex(openssl_random_pseudo_bytes(self::SALT_LENGTH));
            $config->set_courseidsalt($salt);
            $this->pseudosalt = $salt;
        }
    }

    /**
     * Returns an array of file_metadata objects with file_metadata registered
     * at the file_metadata table. It excludes administrative documents, they
     * are not auditable.
     *
     * @global \moodle_database $DB
     * @param type $limitfrom
     * @param type $limitnum
     *
     * @return \local_lpi\orm\file_metadata[] An array of file metadata
     */
    public function get_file_metadata ($limitfrom = 0, $limitnum = self::MAX_LIMITNUM) {

        global $DB;

        if ($limitnum > self::MAX_LIMITNUM) {
            throw new \local_lpi\exception\max_number_of_results_requested_exceeded(self::MAX_LIMITNUM);
        }

        $ret = [];

        $sql = 'SELECT * '
                . 'FROM {local_lpi_file_metadata} fm '
                . 'WHERE ( '
                .     '(fm.type IS NULL OR fm.type != :type_administrative) '
                .     'AND '
                .     '( fm.state != :unknown_state OR fm.state IS NULL ) '
                . ')';

        $rs = $DB->get_recordset_sql(
            $sql,
            [
                'type_administrative' => file_metadata::TYPE_ADMINISTRATIVE,
                'unknown_state' => file_metadata::STATE_UNKNOWN
            ],
            $limitfrom,
            $limitnum
        );
        foreach ($rs as $r) {
            $ret[] = new file_metadata($r);
        }
        $rs->close();

        return $ret;
    }

    /**
     * Pseudonymise the id of courseid in order not to get teacher information
     * from it's id.
     *
     * @param int $courseid
     */
    private function pseudonymise_courseid($courseid) {
        return sha1($this->pseudosalt . dechex($courseid));
    }

    /**
     * Returns a pseudonymised courseid.
     *
     * @global \moodle_database $DB
     * @param int $courseid the id of the course.
     * @return string
     */
    public function get_pseudo_courseid($courseid) {
        global $DB;
        $r = $DB->get_record('local_lpi_pseudo_course', ['courseid' => $courseid]);
        if (!$r) {
            $r = new \stdClass();
            $r->courseid = $courseid;
            $r->pseudocourse = $this->pseudonymise_courseid($courseid);
            $DB->insert_record('local_lpi_pseudo_course', $r);
        }
        return $r->pseudocourse;
    }
}
