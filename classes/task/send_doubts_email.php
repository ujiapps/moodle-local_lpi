<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A task that send the pending doubt emails.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/moodlelib.php');

class send_doubts_email extends \core\task\scheduled_task {

    public function get_name() {
        return get_string('send_doubts_email_task', 'local_lpi');
    }

    public function execute() {
        global $PAGE;

        $config = \local_lpi\config::get_instance();
        if (!$config->get_doubtsemail()) {
            mtrace("        ERROR. Doubt's email not configured.");
            return;
        }

        $renderer = $PAGE->get_renderer('local_lpi');

        $manager = \local_lpi\manager::get_instance();

        $queuemanager = \local_lpi\doubt_queue_manager::get_instance();
        $doubts = $queuemanager->get();
        foreach ($doubts as $doubt) {
            $user = \core_user::get_user($doubt->get_userid());

            if (is_null($doubt->get_files())) {
                mtrace("    ERROR. La consulta {$doubt->get_id()} no tiene ficheros.");
                continue;
            }

            // Send the email to the doubtuser.
            $messagesubject = get_string(
                'doubt_email_subject',
                'local_lpi',
                array(
                    'username' => $user->username,
                    'filename' => $doubt->get_files()[0]->get_filename()
                )
            );

            $messagebody = get_string(
                'doubt_email_body',
                'local_lpi',
                array(
                    'username' => $user->username,
                    'filename' => $doubt->get_files()[0]->get_filename(),
                    'coursename' => $doubt->get_course()->fullname,
                    'reviewweburl' => (string) new \moodle_url('/local/lpi/download_file.php', array(
                        'contenthash' => $doubt->get_contenthash(),
                        'courseid' => $doubt->get_courseid()
                    ))
                )
            );
            $messagebody .= $doubt->get_message();
            $messagebody = format_text($messagebody, FORMAT_HTML);
            $messagetext = html_to_text($messagebody);

            if (!$this->send_email($user, $config->get_doubtsemail(), $messagesubject, $messagebody, $messagetext)) {
                mtrace("    ERROR Enviando consulta\n");
                continue;
            }

            $doubt->set_timesent(time());
            $queuemanager->update($doubt);

            // Trigger a doubt_sent event.
            $event = \local_lpi\event\doubt_sent::create_from_doubt($doubt);
            $event->trigger();

        }
    }

    private function send_email($from, $toemail, $subject, $bodyhtml, $bodytext) {

        $mailer = get_mailer();
        $mailer->setFrom($from->email, fullname($from));
        $mailer->addAddress($toemail, $toemail);
        $mailer->addReplyTo($from->email, fullname($from));
        $mailer->isHTML(true);
        $mailer->Encoding = 'quoted-printable';

        $mailer->Subject = $subject;
        $mailer->Body = $bodyhtml;
        $mailer->AltBody = $bodytext;

        return $mailer->send() ? true : false;
    }


}
