<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This task inserts orphaned files (ie. files with no metadata associated yet)
 * into the lpi_file_metadata table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\task;

defined('MOODLE_INTERNAL') || die();

use local_lpi\ignored_files_manager;
use local_lpi\orm\ignored_file;

class insert_orphaned_files extends \core\task\scheduled_task {

    public function get_name() {
        return get_string('insert_orphaned_files_task', 'local_lpi');
    }

    public function execute() {

        mtrace("    [GDPI] Upating orphaned and ignored files... ");

        $ignoredmanager = ignored_files_manager::get_instante();
        $manager = \local_lpi\manager::get_instance();
        $courses = $manager->get_courses_to_consider();
        foreach ($courses as $c) {
            mtrace("             Curso {$c->id}... ", '');
            $manager->insert_unknown_files_into_file_metadata($c->id);
            $ignoredmanager->update_ignored_files($c->id, ignored_file::REASON_STUDENT_FILE);
            mtrace("OK");
        }
        $courses->close();

        mtrace("    END");
    }
}
