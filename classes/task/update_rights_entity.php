<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Updates the rights entity associated to a file.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\task;

defined('MOODLE_INTERNAL') || die();

class update_rights_entity extends \core\task\scheduled_task {

    // How many files to get each time.
    const DB_COUNT = 100;

    public function get_name() {
        return get_string('updaterightscentre', 'local_lpi');
    }


    public function execute() {

        $client = \local_lpi\ws\client::get_instance();
        $manager = \local_lpi\manager::get_instance();

        $offset = 0;

        $toassign = $manager->get_file_metadata_ids_without_rights_entity($offset, self::DB_COUNT);
        while ($toassign) {

            $entityforfiles = [];

            foreach ($toassign as $filemetadataid) {
                $entity = $client->get_rights_entity($filemetadataid);
                if (!$entity) {
                    mtrace("Unknown entity for " . $filemetadataid);
                    continue;
                }

                if (!array_key_exists($entity, $entityforfiles)) {
                    $entityforfiles[$entity] = array($filemetadataid);
                } else {
                    $entityforfiles[$entity][] = $filemetadataid;
                }
            }

            // Now do a bulk database update.
            if ($entityforfiles) {
                $entities = array_keys($entityforfiles);
                foreach ($entities as $entity) {
                    $manager->update_rights_entity_manager_for_files($entity, $entityforfiles[$entity]);
                }
            }

            $offset += self::DB_COUNT;
            $toassign = $manager->get_file_metadata_ids_without_rights_entity($offset, self::DB_COUNT);
        }
    }
}
