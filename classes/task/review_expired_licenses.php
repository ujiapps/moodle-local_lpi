<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A task to check for expired licenses and transition to state unknown.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\task;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class review_expired_licenses extends \core\task\scheduled_task {

    const DEFAULT_STEP = 50;

    public function get_name() {
        return get_string('license_expired_task', 'local_lpi');
    }

    public function execute() {

        $limitfrom = 0;
        $limitnum = self::DEFAULT_STEP;

        $manager = \local_lpi\manager::get_instance();

        $fms = $manager->get_expired_license_metadata($limitfrom, $limitnum);
        while ($fms) {

            $ids = array_map(function($fm) {
                return $fm->get_id();
            }, $fms);

            $manager->update_state(file_metadata::STATE_UNKNOWN, $ids);

            foreach ($fms as $fm) {
                \local_lpi\event\license_expired::create_from_file_metadata($fm)->trigger();
            }

            $limitfrom += count($fms);
            $fms = $manager->get_expired_license_metadata($limitfrom, $limitnum);

        }
    }
}
