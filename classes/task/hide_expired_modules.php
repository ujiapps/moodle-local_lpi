<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A task to hide expired modules.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/messagelib.php');

class hide_expired_modules extends \core\task\scheduled_task {

    public function get_name() {
        return get_string('hide_expired_modules', 'local_lpi');
    }

    public function execute() {

        $config = \local_lpi\config::get_instance();
        if ($config->get_enableactivityhiding() == 0) {
            mtrace("Activity hidding disabled: look at enableactivityhidding config variable at local/lpi config page");
            return;
        }

        // If it's saturday or sunday in the server timezone, don't hide anything.
        // Wait until monday.
        $servertz = \core_date::get_server_timezone();
        $servertz = new \DateTimeZone($servertz);

        $now = \DateTime::createFromFormat("U", time(), $servertz);
        $weekday = $now->format("w");
        if (in_array($weekday, [6, 0])) {
            mtrace("No activity hidding on weekends");
            return;
        }

        $manager = \local_lpi\manager::get_instance();
        $config = \local_lpi\config::get_instance();

        $period = $config->get_deadlineduration();
        $hidden = array();
        $modules = $manager->get_cms_to_hide(time() - $period);
        foreach ($modules as $module) {

            if (array_key_exists($module->cmid, $hidden)) {
                continue;
            }

            mtrace("        hidding cmid " . $module->cmid);

            set_coursemodule_visible($module->cmid, 0);

            $hidden[$module->cmid] = true;

            $event = \local_lpi\event\module_hidden::create_from_cmid(
                    $module->courseid,
                    $module->cmid,
                    $module->id,
                    $module->userid
            );
            $event->trigger();
        }
    }
}

