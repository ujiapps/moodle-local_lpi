<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Description
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\observers;

use local_lpi\config;

defined('MOODLE_INTERNAL') || die();

class course_viewed {

    /**
     * @param \core\event\course_viewed $event
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws \moodle_exception
     */
    public static function observe(\core\event\course_viewed $event) {

        global $PAGE, $DB;

        if ($event->courseid == SITEID) {
            return;
        }

        $ctx = \context_course::instance($event->courseid, IGNORE_MISSING);
        if (!$ctx) {
            return;
        }
        if (!has_capability('local/lpi:addfilemetadata', $ctx)) {
            return;
        }

        $course = $DB->get_record('course', ['id' => $event->courseid], 'id, fullname');
        $config = config::get_instance();

        $notificationtext = trim($config->get_notificationtext());
        if ($notificationtext) {
            $notificationtext = format_text($notificationtext, FORMAT_HTML);
            $notificationtext = str_replace(
                '{{reviewurl}}',
                (string)new \moodle_url('/local/lpi/review.php', array('courseid' => $event->courseid)),
                $notificationtext
            );
        }

        $PAGE->requires->js_call_amd(
            'local_lpi/review-notification',
            'checkPendingFiles',
              [
                  'courseid' => $event->courseid,
                  'coursefullname' => $course->fullname,
                  'reviewurl' => (string) new \moodle_url('/local/lpi/review.php', array('courseid' => $event->courseid)),
                  'moreinfourl' => $config->get_guideurl(),
                  'notificationtext' => $notificationtext
              ]
        );

    }
}
