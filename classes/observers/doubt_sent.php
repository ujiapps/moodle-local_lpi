<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Observes the event thrown when a doubt has been sent.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\observers;

defined('MOODLE_INTERNAL') || die();

class doubt_sent {
    /**
     * Sends a doubt_sent_confirmation message to the user.
     *
     * @param \local_lpi\event\doubt_sent $event
     */
    public static function observe (\local_lpi\event\doubt_sent $event) {

        $manager = \local_lpi\manager::get_instance();
        $queue = \local_lpi\doubt_queue_manager::get_instance();

        $contenthash = $event->other['contenthash'];
        $filemetadata = $manager->get_file_metadata_by_contenthash($event->courseid, $contenthash);
        $doubt = $queue->get_by_id($event->objectid);

        $message = new \local_lpi\message\doubt_sent_confirmation($doubt, $filemetadata);
        message_send($message);
    }
}

