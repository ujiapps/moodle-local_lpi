<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Observer for the file_reviewed event. Basically notifies about a new file
 * review if necessary (ie. state is STATE_GT10).
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\observers;

defined('MOODLE_INTERNAL') || die();

class file_externally_reviewed {

    public static function observe (\local_lpi\event\file_externally_reviewed $event) {
        $manager = \local_lpi\manager::get_instance();
        $filemetadata = $manager->get_file_metadata_by_id($event->objectid);
        if ($filemetadata->get_userid()) {
            $message = new \local_lpi\message\fileexternallyreviewed($filemetadata);
            message_send($message);
        }
    }

}
