<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An observer for module_hidden event. Basically it sends a message to the
 * related userid.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\observers;

defined('MOODLE_INTERNAL') || die();

class module_hidden {
    public static function observe (\local_lpi\event\module_hidden $event) {

        if (!$event->relateduserid) {
            return;
        }

        $cmid = $event->objectid;
        $fileid = $event->other['fileid'];
        $courseid = $event->other['courseid'];
        $userid = $event->relateduserid;

        $message = new \local_lpi\message\hidden_module_confirmation($courseid, $cmid, $fileid, $userid);
        message_send($message);
    }
}

