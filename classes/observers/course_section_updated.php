<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An observer for the course_module_updated, course_module_created and
 * course_module_deleted.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2020 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\observers;

use local_lpi\manager;

defined('MOODLE_INTERNAL') || die();

class course_section_updated {

    private static function add_to_visibility_log ($visible, $context) {
        global $DB;

        try {
            $sql = 'select distinct contenthash '
                . 'from {files} f '
                . 'where f.contextid = :contextid '
                . '      and f.filename != :dot '
                . '      and f.filearea = :section '
                . '      and f.component = :course';

            $filesinmodule = $DB->get_records_sql($sql, [
                'contextid' => $context->id,
                'dot' => '.',
                'draft' => 'draft',
                'section' => 'section',
                'course' => 'course'
            ]);

            foreach ($filesinmodule as $contenthash) {
                $rec = (object) [
                    'contenthash' => $contenthash->contenthash,
                    'timecreated' => time(),
                    'visible' => $visible,
                    'originalcontextid' => $context->id
                ];
                $DB->insert_record('local_lpi_file_visibility', $rec);
            }
        } catch (\moodle_exception $e) {

        }
    }

    public static function observe_updated (\core\event\course_section_updated $event) {
        global $DB;
        $section = $DB->get_record('course_sections', ['id' => $event->objectid]);
        self::add_to_visibility_log($section->visible, $event->get_context());
    }

    public static function observe_created(\core\event\course_section_created $event) {
        global $DB;
        $section = $DB->get_record('course_sections', ['id' => $event->objectid]);
        self::add_to_visibility_log($section->visible, $event->get_context());
    }

    public static function observe_deleted(\core\event\course_section_deleted $event) {
        die('x');
        self::add_to_visibility_log(0, $event->get_context());
    }

}

