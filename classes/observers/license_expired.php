<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * license_expired observer. It send a notification to teachers.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\observers;

defined('MOODLE_INTERNAL') || die();

class license_expired {
    public static function observe(\local_lpi\event\license_expired $event) {

        $courseid = $event->courseid;
        $modulename = $event->other['modulename'];
        $metadataid = $event->other['metadataid'];
        $userid = $event->relateduserid;

        $message = new \local_lpi\message\license_expired_confirmation(
                $courseid,
                $modulename,
                $metadataid,
                $userid
        );
        message_send($message);
    }
}
