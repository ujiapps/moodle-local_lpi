<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The integration external service.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/user/externallib.php');
require_once($CFG->libdir . '/licenselib.php');

use local_lpi\orm\file_metadata;
use local_lpi\objects\document_content;

class integration_service extends \external_api {

    const PAGE_SIZE = 100;

    const AUTHZ_OK = 'ok';
    const AUTHZ_FORBIDDEN = 'forbidden';

    private static $validauthz = array(
        self::AUTHZ_OK,
        self::AUTHZ_FORBIDDEN
    );

    public static function get_pending_files_parameters() {
        return new \external_function_parameters(
            array(
                'page' => new \external_value(PARAM_INT, 'The page to get', VALUE_REQUIRED, 0, NULL_ALLOWED),
                'count' => new \external_value(
                    PARAM_INT,
                    'How many registers to return per page',
                    VALUE_REQUIRED,
                    self::PAGE_SIZE, NULL_NOT_ALLOWED
                ),
                'search' => new \external_value(PARAM_RAW, 'A search criteria', VALUE_REQUIRED, null, NULL_ALLOWED)
            )
        );
    }

    /**
     * Returns files to review by the external applicacion.
     *
     * @param int $page the page number to retrieve.
     * @param int $count the number of elements per page to return.
     * @param string $search a search criteria.
     */
    public static function get_pending_files(
            $page = 0,
            $count = self::PAGE_SIZE,
            $search = null
    ) {
        $params = self::validate_parameters(
            self::get_pending_files_parameters(),
            array(
                'page' => $page,
                'count' => $count,
                'search' => $search
            )
        );

        $ctx = \context_system::instance();
        \require_capability('local/lpi:addfilemetadata', $ctx);

        $manager = \local_lpi\manager::get_instance();
        $ret = new dto\dto_pending_files(
            $manager->get_pending_files_to_review_externally(
                $params['search'],
                $params['page'] * $params['count'],
                $params['count']
            ),
            $manager->count_pending_files_to_review_externally($params['search'])
        );

        return $ret->get_dto();
    }

    public static function get_pending_files_returns() {
        return dto\dto_pending_files::get_dto_external_description();
    }

    /**************************************************************************
     * get_file_metadata
     */

    public static function get_file_metadata_parameters() {
        return new \external_function_parameters(
            array(
                'id' => new \external_value(PARAM_INT, 'The metadataid')
            )
        );
    }

    /**
     * Get file metadata.
     *
     * @global type $USER
     * @global \moodle_database $DB
     * @param type $courseid
     * @param type $status
     * @param type $page
     */
    public static function get_file_metadata($id) {

        $params = self::validate_parameters(
            self::get_file_metadata_parameters(),
            [
                'id' => $id
            ]
        );

        $manager = \local_lpi\manager::get_instance();
        $filemetadata = $manager->get_file_metadata_by_id($params['id']);

        if (!$filemetadata || $filemetadata->get_state() !== file_metadata::STATE_GT10) {
            throw new \local_lpi\exception\metadata_not_found_exception($id);
        }

        $courseid = $filemetadata->get_courseid();
        $ctx = \context_course::instance($courseid);
        \require_capability('local/lpi:addfilemetadata', $ctx);

        return (new dto\dto_get_file_metadata($filemetadata))->get_dto();
    }

    public static function get_file_metadata_returns() {
        return dto\dto_get_file_metadata::get_dto_external_description();
    }

    /**************************************************************************
     * set_file_authorization
     */

    public static function set_file_authorization_parameters() {
        return new \external_function_parameters(
            array(
                'id' => new \external_value(PARAM_INT, 'The id of the file metadata to update'),
                'authz' => new \external_value(PARAM_ALPHANUMEXT, 'The authorization values'),
                'documentcontent' => new \external_value(PARAM_RAW, 'The document content'),
                'timeendlicense' => new \external_value(PARAM_INT, 'The end license date'),
                'denyreason' => new \external_value(PARAM_RAW, 'The reason to be be denied')
            )
        );
    }

    public static function set_file_authorization($id, $authz, $documentcontent, $timeendlicense = null, $denyreason = null) {

        $params = self::validate_parameters(
            self::set_file_authorization_parameters(),
            array(
                'id' => $id,
                'authz' => $authz,
                'documentcontent' => $documentcontent,
                'timeendlicense' => $timeendlicense,
                'denyreason' => $denyreason
            )
        );

        // LPI manager.
        $manager = \local_lpi\manager::get_instance();
        $filemetadata = $manager->get_file_metadata_by_id($params['id']);
        if (!$filemetadata) {
            throw new \local_lpi\exception\metadata_not_found_exception($params['id']);
        }

        $courseid = $filemetadata->get_courseid();
        $ctx = \context_course::instance($courseid);
        \require_capability('local/lpi:addfilemetadata', $ctx);

        // We can set the state if the current state is STATE_GT10.
        if (!in_array($filemetadata->get_state(), array(file_metadata::STATE_GT10))) {
            throw new \local_lpi\exception\not_supported_state_exception($params['id'], $filemetadata->get_state());
        }

        // Throw an exception if authz is not valid.
        if (!in_array($params['authz'], self::$validauthz)) {
            throw new \local_lpi\exception\invalid_authorization_exception($params['authz']);
        }

        if ($params['authz'] === self::AUTHZ_FORBIDDEN) {
            $filemetadata->set_state(file_metadata::STATE_FORBIDDEN);
            $filemetadata->set_denyreason($params['denyreason']);
        } else if ($params['authz'] === self::AUTHZ_OK) {
            $documentcontent = new document_content($params['documentcontent']);

            switch ($documentcontent->get_value()) {
                case document_content::OTHER:
                    // We're authorizing the file, so document_content must "promote" to HAVELICENSE.
                    $documentcontent->set_value(document_content::HAVELICENSE);

                case document_content::HAVELICENSE:

                    if ($params['timeendlicense'] === null) {
                        throw new \local_lpi\exception\timeendlicense_needed_exception();
                    }
                    $filemetadata->set_timeendlicense($params['timeendlicense']);
                    break;
            }

            $documentcontent->update_file_metadata($filemetadata);
        } else {
            throw new \local_lpi\exception\invalid_authorization_exception($params['authz']);
        }

        $manager->add_file_metadata($filemetadata);

        // Trigger the file_reviewed event.
        $event = \local_lpi\event\file_externally_reviewed::create_from_file_metadata($filemetadata);
        $event->trigger();

        $ret = new dto\dto_set_file_authorization(
            $params['id'],
            $params['authz'],
            $documentcontent,
            $filemetadata->get_timeendlicense(),
            $params['denyreason']
        );
        return $ret->get_dto();
    }

    public static function set_file_authorization_returns() {
        return dto\dto_set_file_authorization::get_dto_external_description();
    }

    /**************************************************************************
     * get_externally_reviewed_logs
     */

    public static function get_externally_reviewed_logs_parameters() {
        return new \external_function_parameters([
            'page' => new \external_value(PARAM_INT, 'The page to get', VALUE_REQUIRED, 0, NULL_ALLOWED),
            'count' => new \external_value(
                PARAM_INT,
                'How many registers to return per page',
                VALUE_REQUIRED,
                self::PAGE_SIZE,
                NULL_NOT_ALLOWED
            )
        ]);
    }

    public static function get_externally_reviewed_logs($page, $count) {
        $params = self::validate_parameters(
            self::get_externally_reviewed_logs_parameters(),
            [
                'page' => $page,
                'count' => $count
            ]
        );

        $ctx = \context_system::instance();
        \require_capability('local/lpi:addfilemetadata', $ctx);

        $manager = \local_lpi\manager::get_instance();
        $events = $manager->get_externally_reviewed_events(
            $params['page'] * $params['count'],
            $params['count']
        );
        $count = $manager->get_externally_reviewed_events_count();

        return dto\dto_get_externally_reviewed_logs::create($events, $count)->get_dto();
    }

    public static function get_externally_reviewed_logs_returns() {
        return dto\dto_get_externally_reviewed_logs::get_dto_external_description();
    }
}
