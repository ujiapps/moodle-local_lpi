<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle files interface for LPI checks.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external\dto;

defined('MOODLE_INTERNAL') || die();

use local_lpi\objects\document_content;

class dto_get_externally_reviewed_logs implements \local_lpi\external\i_dto {

    private $events;
    private $count;

    private $users;

    /**
     * Creates an array of events to an array of dto_get_externally_reviewed_logs.
     *
     * @param \core\event\base[] $events
     */
    public static function create($events, $count) {
        return new self($events, $count);
    }

    /**
     * The constructor.
     *
     * @param \local_lpi\event\file_externally_reviewed[] $events
     * @param int $count The total number of events (not the length of $events).
     */
    public function __construct($events, $count) {
        $this->events = $events;
        $this->count = $count;
    }

    private function get_data(\core\event\base $event) {

        if (array_key_exists($event->relateduserid, $this->users)) {
            $user = $this->users[$event->relateduserid];
        } else {
            $user = \core_user::get_user($event->relateduserid, 'id,idnumber,email', IGNORE_MISSING);
            if ($user) {
                $this->users[$user->id] = $user;
            }
        }

        return (object) [
            'timecreated' => $event->timecreated,
            'metadatauserid' => $user->id,
            'metadatauseridnumber' => $user->idnumber,
            'metadatauseremail' => $user->email,
            'documentcontent' => (string) document_content::get_document_content_by_params(
                $event->other['state'],
                $event->other['type'],
                $event->other['property']
            ),
            'courseid' => $event->courseid,
            'filename' => $event->other['filename']
        ];
    }

    public function get_dto() {
        $data = [];
        foreach ($this->events as $event) {
            $data[] = $this->get_data($event);
        }

        return (object) [
            'count' => $this->count,
            'data' => $data
        ];
    }

    public static function get_dto_external_description() {
        return new \external_single_structure ([
            'data' => new \external_multiple_structure(
                new \external_single_structure([
                    'timecreated' => new \external_value(PARAM_INT),
                    'metadatauserid' => new \external_value(PARAM_INT),
                    'metadatauseridnumber' => new \external_value(PARAM_RAW),
                    'metadatauseremail' => new \external_value(PARAM_EMAIL),
                    'documentcontent' => new \external_value(PARAM_RAW),
                    'courseid' => new \external_value(PARAM_INT),
                    'filename' => new \external_value(PARAM_FILE)
                ])
            ),
            'count' => new \external_value(PARAM_INT)
        ]);
    }
}
