<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * DTO for local_lpi_get_pending_files external function.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external\dto;

defined('MOODLE_INTERNAL') || die();

class dto_pending_files implements \local_lpi\external\i_dto {

    private $data;
    private $count;

    /**
     * The constructor.
     *
     * @param \stdClass[] $data the data retrived by manager::get_pending_files_to_review_externally
     * @param int $count the value returned by manager::count_pending_files_to_review_externally
     */
    public function __construct($data, $count) {
        $this->data = $data;
        $this->count = $count;
    }

    public function get_dto() {
        return (object) [
            'data' => $this->data,
            'count' => $this->count
        ];
    }

    public static function get_dto_external_description() {
        return new \external_single_structure ([
            'data' => new \external_multiple_structure(
                    new \external_single_structure([
                        'id' => new \external_value(PARAM_INT),

                        'title' => new \external_value(PARAM_RAW),
                        'author' => new \external_value(PARAM_RAW),
                        'state' => new \external_value(PARAM_ALPHANUMEXT),

                        'metadatauserid' => new \external_value(PARAM_INT),
                        'metadatauseridnumber' => new \external_value(PARAM_RAW),
                        'metadatauseremail' => new \external_value(PARAM_EMAIL),
                        'metadatatimecreated' => new \external_value(PARAM_INT)
                    ])
                )
            ,
            'count' => new \external_value(PARAM_INT)
        ]);
    }
}
