<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A DTO for the local_lpi_set_file_authorization external function.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external\dto;

defined('MOODLE_INTERNAL') || die();

class dto_set_file_authorization implements \local_lpi\external\i_dto {

    private $id;
    private $authz;
    private $documentcontent;
    private $timeendlicense;
    private $denyreason;

    public function __construct($metadataid, $authz, $documentcontent, $timeendlicense, $denyreason) {
        $this->id = $metadataid;
        $this->authz = $authz;
        $this->documentcontent = $documentcontent ? $documentcontent : '';
        $this->timeendlicense = $timeendlicense;
        $this->denyreason = $denyreason;
    }

    public function get_dto() {
        return (object) [
            'id' => $this->id,
            'authz' => $this->authz,
            'documentcontent' => (string) $this->documentcontent,
            'timeendlicense' => $this->timeendlicense,
            'denyreason' => $this->denyreason
        ];
    }

    public static function get_dto_external_description() {
        return new \external_single_structure(
            [
                'id' => new \external_value(PARAM_INT, 'The id of the file metadata inserted / updated'),
                'authz' => new \external_value(PARAM_ALPHANUMEXT, 'The authorization code'),
                'documentcontent' => new \external_value(PARAM_RAW, 'The document content value'),
                'timeendlicense' => new \external_value(PARAM_INT, 'The end license date', VALUE_OPTIONAL),
                'denyreason' => new \external_value(PARAM_RAW, 'The deny reason')
            ]
        );
    }
}
