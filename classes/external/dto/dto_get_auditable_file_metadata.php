<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A DTO for file_metadata to be sent in an audit.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external\dto;

defined('MOODLE_INTERNAL') || die();

use local_lpi\external\i_dto;
use local_lpi\orm\file_metadata;
use local_lpi\audit\audit_helper;

class dto_get_auditable_file_metadata implements i_dto  {

    /**
     * A reference to the file_metadata instance.
     *
     * @var file_metadata
     */
    private $filemetadata;

    private $pseudocourseid;

    /**
     * Returns an array of dto_file_metadata.
     *
     * @param file_metadata[] $filemetadatas
     * @return dto_file_metadata[]
     */
    public static function create ($filemetadatas) {
        return array_map(function($filemetadata) {
            return new dto_file_metadata($filemetadata);
        }, $filemetadatas);
    }

    public function __construct(file_metadata $filemetadata) {
        $this->filemetadata = $filemetadata;
    }

    public function get_dto() {
        global $CFG, $DB;

        if (!$this->pseudocourseid) {
            $this->pseudocourseid = audit_helper::get_instance()->get_pseudo_courseid($this->filemetadata->get_courseid());
        }

        $course = $DB->get_record('course', ['id' => $this->filemetadata->get_courseid()]);

        return (object) [
            'id' => $this->filemetadata->get_id(),
            'state' => $this->filemetadata->get_document_content()->get_value(),
            'coursetimecreated' => $course->timecreated,
            'timemodified' => $this->filemetadata->get_timemodified(),
            'license' => $this->filemetadata->get_license(),
            'title' => $this->filemetadata->get_title(),
            'author' => $this->filemetadata->get_author(),
            'publisher' => $this->filemetadata->get_publisher(),
            'identification' => $this->filemetadata->get_identification(),
            'pages' => $this->filemetadata->get_pages(),
            'totalpages' => $this->filemetadata->get_pages(),
            'enroled' => $this->filemetadata->get_nenrolments(),
            'timeendlicense' => $this->filemetadata->get_timeendlicense(),
            'comments' => $this->filemetadata->get_comments(),
            'courseid' => $this->pseudocourseid,
            'downloadurl' => (string) new \moodle_url(
                    '/local/lpi/download_audit_file.php',
                    [
                        'id' => $this->filemetadata->get_id(),
                        'token' => 'put your token here'
                    ]
            )
        ];
    }

    /**
     * @return \external_single_structure
     */
    public static function get_dto_external_description() {
        return new \external_single_structure(
            array(
                'id' => new \external_value(PARAM_INT),
                'state' => new \external_value(PARAM_RAW),
                'coursetimecreated' => new \external_value(PARAM_INT),
                'timemodified' => new \external_value(PARAM_INT),
                'license' => new \external_value(PARAM_RAW),
                'title' => new \external_value(PARAM_RAW),
                'author' => new \external_value(PARAM_RAW),
                'publisher' => new \external_value(PARAM_RAW),
                'identification' => new \external_value(PARAM_RAW),
                'pages' => new \external_value(PARAM_RAW),
                'totalpages' => new \external_value(PARAM_INT),
                'enroled' => new \external_value(PARAM_INT),
                'timeendlicense' => new \external_value(PARAM_INT),
                'comments' => new \external_value(PARAM_RAW),
                'courseid' => new \external_value(PARAM_RAW),
                'downloadurl' => new \external_value(PARAM_URL)
            )
        );
    }
}
