<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A DTO for the local_lpi_get_file_metadata external function.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external\dto;

defined('MOODLE_INTERNAL') || die();

class dto_get_file_metadata implements \local_lpi\external\i_dto {

    /**
     * The file metadata.
     *
     * @var \local_lpi\orm\file_metadata
     */
    private $filemetadata;

    /**
     * One physical file from file_metadata that we use to get more info.
     *
     * @var \stored_file
     */
    private $storedfile;

    /**
     * Info about the user that uploaded the stored_file.
     *
     * @var \stdClass
     */
    private $uploaduser;

    /**
     * Info about the user that wrote the file_metadata info.
     *
     * @var \stdClass
     */
    private $metadatauser;

    public function __construct($filemetadata) {
        $this->filemetadata = $filemetadata;

        $this->storedfile = $this->filemetadata->get_files()[0];

        if ($this->storedfile && $this->storedfile->get_userid()) {
            $this->uploaduser = \core_user::get_user(
                $this->storedfile->get_userid(),
                'id,idnumber,email',
                IGNORE_MISSING
            );
        }

        $this->metadatauser = \core_user::get_user(
            $filemetadata->get_userid(),
            'id,idnumber,email',
            IGNORE_MISSING
        );
    }

    public function get_dto() {

        if ($this->filemetadata->get_nenrolments() === null) {
            $manager = \local_lpi\manager::get_instance();
            $nenrolments = $manager->get_real_number_of_enroled_students($this->filemetadata->get_courseid());
        } else {
            $nenrolments = $this->filemetadata->get_nenrolments();
        }

        return (object) [
            'id' => $this->filemetadata->get_id(),
            'title' => $this->filemetadata->get_title(),
            'author' => $this->filemetadata->get_author(),
            'publisher' => $this->filemetadata->get_publisher(),
            'url' => (string) new \moodle_url('/local/lpi/download_file.php', ['id' => $this->filemetadata->get_id()]),
            'identification' => $this->filemetadata->get_identification(),
            'pages' => $this->filemetadata->get_pages(),
            'totalpages' => $this->filemetadata->get_totalpages(),
            'enroled' => $nenrolments,
            'metadatauserid' => $this->metadatauser->id,
            'metadatauseridnumber' => $this->metadatauser->idnumber,
            'metadatauseremail' => $this->metadatauser->email,

            // For timecreated we pass the timemodified date.
            'metadatatimecreated' => $this->filemetadata->get_timemodified(),

            'uploaduserid' => ($this->uploaduser && isset($this->uploaduser->id)) ? $this->uploaduser->id : null,
            'uploaduseridnumber' => ($this->uploaduser) ? $this->uploaduser->idnumber : null,
            'uploaduseremail' => ($this->uploaduser) ? $this->uploaduser->email : null,

            'uploadtimecreated' => $this->storedfile->get_timecreated(),

            'mimetype' => $this->storedfile->get_mimetype(),
            'filename' => $this->storedfile->get_filename(),
            'filesize' => $this->storedfile->get_filesize(),

            'state' => $this->filemetadata->get_state(),

            'timeendlicense' => $this->filemetadata->get_timeendlicense(),
            'comments' => $this->filemetadata->get_comments(),
        ];
    }

    public static function get_dto_external_description() {
        return new \external_single_structure([
                'id' => new \external_value(PARAM_INT),
                'title' => new \external_value(PARAM_RAW),
                'author' => new \external_value(PARAM_RAW),
                'publisher' => new \external_value(PARAM_RAW),
                'url' => new \external_value(PARAM_URL),
                'identification' => new \external_value(PARAM_RAW),
                'pages' => new \external_value(PARAM_RAW),
                'totalpages' => new \external_value(PARAM_INT),
                'enroled' => new \external_value(PARAM_INT),
                'metadatauserid' => new \external_value(PARAM_INT),
                'metadatauseridnumber' => new \external_value(PARAM_RAW),
                'metadatauseremail' => new \external_value(PARAM_EMAIL),
                'metadatatimecreated' => new \external_value(PARAM_INT),
                'uploaduserid' => new \external_value(PARAM_INT),
                'uploaduseridnumber' => new \external_value(PARAM_RAW),
                'uploaduseremail' => new \external_value(PARAM_EMAIL),
                'uploadtimecreated' => new \external_value(PARAM_INT),
                'mimetype' => new \external_value(PARAM_RAW),
                'filename' => new \external_value(PARAM_FILE),
                'filesize' => new \external_value(PARAM_INT),
                'state' => new \external_value(PARAM_ALPHANUM),
                'timeendlicense' => new \external_value(PARAM_INT),
                'comments' => new \external_value(PARAM_RAW)
            ],
            '',
            VALUE_OPTIONAL,
            null
        );
    }

}

