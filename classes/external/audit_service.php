<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The audit external service.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\external;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/externallib.php');

use local_lpi\external\dto\dto_get_auditable_file_metadata;
use local_lpi\audit\audit_helper;

class audit_service extends \external_api {

    const PAGE_SIZE = 100;

    public static function get_auditable_file_metadata_parameters () {
        return new \external_function_parameters(
            array(
                'page' => new \external_value(PARAM_INT, 'The page to get', VALUE_REQUIRED, 0, NULL_ALLOWED),
                'count' => new \external_value(
                    PARAM_INT,
                    'How many registers to return',
                    VALUE_REQUIRED,
                    self::PAGE_SIZE, NULL_NOT_ALLOWED
                )
            )
        );
    }

    public static function get_auditable_file_metadata ($page, $count) {
        $params = self::validate_parameters(
            self::get_auditable_file_metadata_parameters(),
            [
                'page' => $page,
                'count' => $count
            ]
        );

        $ctx = \context_system::instance();
        require_capability('local/lpi:audit', $ctx);

        $audit = audit_helper::get_instance();
        $filemetadatas = $audit->get_file_metadata(
            $params['page'] * $params['count'],
            $params['count']
        );

        return array_map(function ($filemetadata) {
            return (new dto_get_auditable_file_metadata($filemetadata))->get_dto();
        }, $filemetadatas);
    }

    public static function get_auditable_file_metadata_returns () {
        return new \external_multiple_structure(
            dto_get_auditable_file_metadata::get_dto_external_description()
        );
    }
}
