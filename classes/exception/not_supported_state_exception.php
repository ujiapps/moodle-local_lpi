<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An exception thrown when trying to set a transition to a state not supported.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\exception;

defined('MOODLE_INTERNAL') || die();

/**
 * An exception thrown when trying to set a transition to a state not supported.
 */
class not_supported_state_exception extends \moodle_exception {
    public function __construct($metadataid, $state) {
        $a = new \stdClass();
        $a->metadataid = $metadataid;
        $a->state = $state;
        parent::__construct('error_notsupportedstate', 'local_lpi', '', $a);
    }
}
