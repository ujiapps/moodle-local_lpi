<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An exception thrown when an API call requests more results than the allowed
 * maximum.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\exception;

defined('MOODLE_INTERNAL') || die();

class max_number_of_results_requested_exceeded extends \moodle_exception {
    /**
     * Constructor.
     *
     * @param int $liminum the maximum number of results allowed.
     */
    public function __construct($limitnum) {
        parent::__construct(
            'error_max_number_of_results',
            'local_lpi',
            '',
            $limitnum
        );
    }


}
