<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A clients that acts as an interface to the external service responsable of
 * getting and setting information.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\ws;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class client {

    /**
     * The singleton instance.
     * @var client
     */
    private static $instance;

    /**
     * Returns the singleton instance.
     *
     * @return client
     */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Given a file_metadata returns the rights entity that manages the rights
     * of this file.
     *
     * @param string $id the identificator of the file.
     * @return string|null The rights entity. null if not known.
     */
    public function get_rights_entity($id) {
        // Just CEDRO at the moment.
        return "cedro";
    }
}
