<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Description
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;
use local_lpi\objects\authorization_state;

require_once($CFG->libdir . '/tablelib.php');

class pending_table extends \table_sql implements \renderable, \templatable {

    /**
     * A renderer needed to render the activity column
     *
     * @var \local_lpi_renderer
     */
    private $renderer;

    /**
     * The courseid whe are in.
     *
     * @var int
     */
    private $courseid;

    /**
     * Elements per page.
     * @var int
     */
    private $perpage;

    /**
     * A cache to speed-up things.
     *
     * @var file_metadata[]
     */
    private $filemetadatas;

    /**
     * Constructor.
     *
     * @param string $uniqueid
     * @param int $courseid
     * @param int $perpage number of elements per page.
     * @param authorization_state[] $authzfilters
     * @param int $visibilityfilter
     * @param \local_lpi_renderer
     */
    public function __construct($uniqueid, $courseid, $perpage, $authzfilters, $visibilityfilter, $renderer) {
        parent::__construct($uniqueid);

        $this->renderer = $renderer;
        $this->courseid = $courseid;
        $this->perpage = $perpage;

        $this->filemetadatas = array();

        $this->set_attribute('id', $uniqueid);

        $this->define_baseurl(new \moodle_url('/local/lpi/review.php', array('courseid' => $courseid)));

        // Columns.
        $this->define_columns(
            array(
                'check',
                'uploaddate',
                'filename',
                'visible',
                'authz',
                'action'
            )
        );

        // Headers.
        $this->define_headers(
            array(
                '<div style="text-align: center;">' . \html_writer::checkbox('checkall', 'check', false, '') . '</div>',
                get_string('uploaddate', 'local_lpi'),
                get_string('filename', 'local_lpi'),
                get_string('visible', 'local_lpi'),
                get_string('state', 'local_lpi'),
                null
            )
        );

        $manager = \local_lpi\manager::get_instance();
        list($fields, $from, $where, $params) = $manager->get_pending_table_sql(
                $courseid,
                $authzfilters,
                $visibilityfilter,
                \local_lpi\review_filters::get_topic_filter(),
                \local_lpi\review_filters::filename_filter_to_like(\local_lpi\review_filters::get_filename_filter())
        );
        $this->set_sql($fields, $from, $where, $params);

        $this->no_sorting('check');
        $this->column_class('check', 'pending-table-check-col');

        $this->collapsible(false);
        $this->sortable(true, 'authz');
        $this->pageable(true);

    }

    public function get_perpage() {
        return $this->perpage;
    }

    public function get_row_class($row) {
        return 'lpi-pending-table-row';
    }

    /**
     * This is to waranty the correct order by.
     *
     * @return string
     */
    public function get_sort_columns() {
        $sortcolumns = parent::get_sort_columns();
        if (!array_key_exists('filename', $sortcolumns)) {
            $sortcolumns['filename'] = 'ASC';
        }
        return $sortcolumns;
    }

    public function col_check($data) {
        $name = 'check';
        return '<div style="text-align: center;">' . \html_writer::checkbox($name, $data->contenthash, false) . '</div>';
    }

    /**
     * The upload date of the file (it's the most recent timemodified column
     * at the files table).
     *
     * @return string
     */
    protected function col_uploaddate($data) {
        return userdate($data->uploaddate, get_string('strftimedatefullshort'));
    }
    public function query_db($pagesize, $useinitialsbar = true) {
        parent::query_db($pagesize, $useinitialsbar);

        // To speed up things, now get all cmids and fileids to render the page.
        $manager = \local_lpi\manager::get_instance();

        $this->filemetadatas = array();
        $contenthashes = array();
        foreach ($this->rawdata as $data) {
            $contenthashes[] = $data->contenthash;
            if (count($contenthashes) == 80) {
                $aux = $manager->get_file_metadata_by_contenthashes($this->courseid, $contenthashes);
                $this->filemetadatas = array_merge($this->filemetadatas, $aux);
                $contenthashes = array();
            }
        }
        if (count($contenthashes) > 0) {
                $aux = $manager->get_file_metadata_by_contenthashes($this->courseid, $contenthashes);
                $this->filemetadatas = array_merge($this->filemetadatas, $aux);
        }
    }

    /**
     * Column filename.
     *
     * @return string
     */
    protected function col_filename($data) {
        $manager = \local_lpi\manager::get_instance();
        $filemetadata = $this->filemetadatas[$data->contenthash];

        $activities = array_map(function ($cminfo) {
            return $cminfo->name;
        }, $filemetadata->get_modules());

        $sections = array_map(function ($sectioninfo) {
            if ($sectioninfo->name) {
                $sectionname = $sectioninfo->name;
            } else {
                $sectionname = get_string('section') . " " . $sectioninfo->section;
            }
            return $sectionname;
        }, $filemetadata->get_sections());

        $pendingfile = new pending_table_col_file(
                $filemetadata->get_files()[0],
                array_merge($activities, $sections)
        );
        return $this->renderer->render($pendingfile);
    }

    /**
     * Column state
     *
     * @return string
     */
    protected function col_authz($data) {

        list($authorization, $authorizationdesc) = authorization_state::authzstate_to_string($data->authz);

        if ($data->authz == (string) authorization_state::AUTHORIZATION_NOT_AUTHORIZED) {
            $authorizationdesc = get_string('authorization_notauthz_desc', 'local_lpi', $data->denyreason);
        }

        if ($data->authz == (string) authorization_state::AUTHORIZATION_NOT_PROCESSED_YET) {
            $class = 'pending-table-authz-col authz_notprocessed';
        } else {
            $class = 'pending-table-authz-col';
        }

        if ($data->authz != authorization_state::AUTHORIZATION_NOTPRINTABLE) {
            list($name, $desc) = authorization_state::get_authorization_state_string_code($data->authz);
            $helpicon = $this->renderer->help_icon($desc, 'local_lpi');
        } else {
            $helpicon = '';
        }
        return \html_writer::div(
                $authorization,
                $class,
                array(
                    'data-fileid' => $data->contenthash,
                    'title' => $authorizationdesc,
                    'style' => 'display: inline-block'
                )
        ) . $helpicon;
    }


    /**
     * Column hidden.
     *
     * @param \local_lpi\orm\file_metadata $data
     * @return string
     */
    protected function col_visible($data) {
        return $data->visible ? get_string('yes') : get_string('no');
    }

    /**
     * Action column.
     *
     * @param \local_lpi\orm\file_metadata $data
     * @return string
     */
    protected function col_action($data) {
        $colaction = new pending_table_col_action(
                $data,
                new \moodle_url('/local/lpi/detailed_review.php')
        );

        return $this->renderer->render($colaction);
    }

    /**
     * Parameters to render the table.
     *
     * @param \local_lpi_renderer $output
     * @return array
     */
    public function export_for_template(\renderer_base $output) {
        return array(
            'pendingtable' => $output->render($this)
        );
    }

}
