<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A pending_file renderable for the table pending_table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/filelib.php');

/**
 * Description of pending_file
 *
 * @author juan
 */
class pending_table_col_file implements \renderable, \templatable {

    /**
     * The file metadata.
     *
     * @var \stored_file
     */
    private $file;

    /**
     * An array of activity names.
     *
     * @var string[]
     */
    private $activities;

    public function __construct(\stored_file $file, $activities) {
        $this->file = $file;
        $this->activities = $activities;
    }

    public function export_for_template(\renderer_base $output) {

        $ret = array(
            'icon' => file_file_icon($this->file),
            'fileurl' => (string) \moodle_url::make_pluginfile_url(
                            $this->file->get_contextid(),
                            $this->file->get_component(),
                            $this->file->get_filearea(),
                            $this->file->get_filearea() == 'intro' ? null : $this->file->get_itemid(),
                            //$this->file->get_itemid() == 0 ? null : $this->file->get_itemid(),
                            $this->file->get_filepath(),
                            $this->file->get_filename(),
                            true
                        ),
            'filename' => $this->file->get_filename(),
            'activities' => implode(",", $this->activities)
        );

        return $ret;
    }
}

