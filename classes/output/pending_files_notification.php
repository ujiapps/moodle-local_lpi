<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A renderable used to show a notification about files to be reviewed at
 * course page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

class pending_files_notification implements \renderable, \templatable {

    private $course;
    private $reviewurl;
    private $moreinfourl;

    /**
     * Creates the pendingfilesnot (pending files notification) renderable.
     *
     * @param string $course a string representing the course name or shortname.
     * @param int $courseid the url to the filemetada page.
     */
    public function __construct($course, $courseid) {
        global $CFG;
        $this->course = $course;
        $this->reviewurl = new \moodle_url('/local/lpi/review.php', array('courseid' => $courseid));
        $this->moreinfourl = \local_lpi\config::get_instance()->get_guideurl();
    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'course' => $this->course,
            'reviewurl' => (string) $this->reviewurl,
            'moreinfourl' => (string) $this->moreinfourl
        );
    }
}
