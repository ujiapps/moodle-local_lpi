<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File preview renderable for template local_lpi/file_preview.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

class file_preview implements \renderable, \templatable {

    /**
     * The instance of the file_metadata we're going to show.
     *
     * @var \local_lpi\orm\file_metadata
     */
    private $filemetadata;

    /**
     * The url to download the file.
     *
     * @var string
     */
    private $fileurl;

    /**
     * Indicates if it's possible to show a preview.
     *
     * @var boolean
     */
    private $previewprossible;

    private $ispdf;
    private $isimage;

    private $mimetype;
    private $filename;

    /**
     *
     * @param \local_lpi\orm\file_metadata $filemetadata
     */
    public function __construct($filemetadata) {
        $this->filemetadata = $filemetadata;

        $storedfile = $filemetadata->get_files()[0];

        $this->mimetype = $storedfile->get_mimetype();
        $this->fileurl = \moodle_url::make_pluginfile_url(
                $storedfile->get_contextid(),
                $storedfile->get_component(),
                $storedfile->get_filearea(),
                $storedfile->get_itemid(),
                $storedfile->get_filepath(),
                $storedfile->get_filename()
        );
        $this->filename = $storedfile->get_filename();
        $this->previewprossible = $this->is_preview_possible();
        $this->ispdf = $this->is_pdf();
        $this->isimage = $this->is_image();
    }

    /**
     * Indica si para un fichero dado podemos generar una previsualización.
     *
     * @return boolean
     */
    private function is_preview_possible() {
        if ($this->mimetype == 'application/pdf') {
            $is = true;
        } else if (substr($this->mimetype, 0, 6) == 'image/') {
            $is = true;
        } else {
            $is = false;
        }
        return $is;
    }
    private function is_pdf() {
        return $this->mimetype == 'application/pdf';
    }

    private function is_image() {
        return substr($this->mimetype, 0, 6) == 'image/';
    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'file_url' => $this->fileurl,
            'preview_possible' => $this->previewprossible,
            'is_pdf' => $this->ispdf,
            'is_image' => $this->isimage,
            'filename' => $this->filename
        );
    }
}
