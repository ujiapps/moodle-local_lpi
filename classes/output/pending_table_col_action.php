<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable for the action column in pending_table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class pending_table_col_action implements \renderable, \templatable {

    private $reviewurl;
    private $courseid;
    private $metadataid;
    private $filename;
    private $reviewdisabled;
    private $reviewhelp;
    private $doubtdisabled;
    private $doubthelp;

    private $uniqueid;
    private $useremail;

    public function __construct($data, \moodle_url $reviewurl) {
        $this->reviewurl = (string) $reviewurl;
        $this->courseid = $data->courseid;
        $this->metadataid = $data->id;
        $this->filename = $data->filename;

        $this->uniqueid = $data->contenthash;

        global $USER;
        $this->useremail = \core_user::get_user($USER->id, 'email');
        $this->useremail = $this->useremail->email;

        switch ($data->state) {
            case file_metadata::STATE_DOUBT:
                $this->doubtdisabled = false;
                $this->doubthelp = get_string('cannot_doubt_in_doubt', 'local_lpi');
                break;

            case file_metadata::STATE_GT10:
                $this->reviewdisabled = true;
                $this->reviewhelp = get_string('cannot_review_in_doubt', 'local_lpi');
                break;
            default:
                $this->reviewdisabled = false;
                $this->reviewhelp = get_string('review', 'local_lpi');

                $this->doubtdisabled = false;
                $this->doubthelp = get_string('doubt_help', 'local_lpi');
        }

    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'reviewurl' => $this->reviewurl,
            'courseid' => $this->courseid,
            'metadataid' => $this->metadataid,
            'filename' => $this->filename,
            'reviewdisabled' => $this->reviewdisabled,
            'reviewhelp' => $this->reviewhelp,
            'doubtdisabled' => $this->doubtdisabled,
            'doubthelp' => $this->doubthelp,
            'uniqueid' => $this->uniqueid,
            'useremail' => $this->useremail
        );
    }

}
