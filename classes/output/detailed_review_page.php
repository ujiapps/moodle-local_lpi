<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defailed review page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

class detailed_review_page implements \renderable, \templatable {

    /**
     * The detailed_review_navigation renderable. Used to build navigation.
     *
     * @var detailed_review_navigation
     */
    private $navigation;

    /**
     * The metadata form for the file.
     *
     * @var file_metadata_form
     */
    private $filemetadataform;

    /**
     * A file_preview renderable.
     *
     * @var file_preview
     */
    private $filepreview;

    public function __construct(
            detailed_review_navigation $navivation,
            file_metadata_form $metadataform,
            file_preview $filepreview
    ) {
        $this->navigation = $navivation;
        $this->filemetadataform = $metadataform;
        $this->filepreview = $filepreview;
    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'review_navigation' => $this->navigation->export_for_template($output),
            'form' => $output->render($this->filemetadataform),
            'file_preview' => $this->filepreview->export_for_template($output),
        );
    }
}
