<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An implementation of a renderable for the review page. This is the first
 * page when a teacher wants to review files. Basically, it consists of a
 * table (a pending_table) with the files and its status.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

class review_page implements \renderable, \templatable {

    /**
     * The url of the help guide.
     *
     * @var string
     */
    private $guideurl;

    /**
     * The pending table.
     *
     * @var pending_table
     */
    private $pendingtable;

    /**
     * The filter form.
     *
     * @var review_data_filter_form
     */
    private $filterform;

    /**
     * The massive actions form.
     *
     * @var massive_actions_form
     */
    private $massiveactionsform;

    /**
     * Constructor.
     *
     * @param pending_table $pendingtable
     */
    public function __construct($guideurl, $pendingtable, review_data_filter_form $filterform, $courseid) {
        $this->guideurl = $guideurl;
        $this->pendingtable = $pendingtable;
        $this->filterform = $filterform;
        $this->massiveactionsform = new massive_actions_form($courseid);
    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'guideurl' => $this->guideurl,
            'pendingtable' => $output->render($this->pendingtable),
            'filterform' => $output->render($this->filterform),
            'massiveactionsform' => $output->render($this->massiveactionsform)
        );
    }
}
