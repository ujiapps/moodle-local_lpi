<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used to perfom massive actions at review page.
 *
 * @package    local/lpi
 * @copyright  2017 Universitat Jaume I (https://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();


class massive_actions_form extends \moodleform implements \renderable {

    private $courseid;

    public function __construct($courseid) {
        $this->courseid = $courseid;
        parent::__construct(null, null, 'post', '', array('id' => 'massive-actions-form'));
    }

    protected function definition() {
        $mform = &$this->_form;

        $mform->addElement('header', 'massive', get_string('massive_actions', 'local_lpi'));

        $options = array(
            \local_lpi\objects\document_content::NOT_PRINTED_OR_PRINTABLE => get_string('action_notprintable', 'local_lpi'),
            \local_lpi\objects\document_content::ADMINISTRATIVE => get_string('action_administrative', 'local_lpi'),
            \local_lpi\objects\document_content::OWNED_BY_TEACHERS => get_string('action_teachers', 'local_lpi'),
            \local_lpi\objects\document_content::EDITED_BY_UNIVERSITY => get_string('action_university', 'local_lpi'),
            \local_lpi\objects\document_content::PUBLIC_DOMAIN => get_string('action_public_domain', 'local_lpi')
        );

        $elements = array();
        $elements[] =& $mform->createElement('select', 'actions', get_string('review_as', 'local_lpi'), $options);
        $elements[] =& $mform->createElement('submit', 'review', get_string('send', 'local_lpi'));
        $mform->addElement('group', 'actiongroup', get_string('review_as', 'local_lpi'), $elements, ' ', false);

        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'confirmed', 0);
        $mform->setType('confirmed', PARAM_INT);

        $mform->addElement('hidden', 'files', '');

        // We can't use PARAM_SEQUENCE for this.
        $mform->setType('files', PARAM_RAW);
    }

    public function validation($data, $files) {
        $err = parent::validation($data, $files);

        if (!array_key_exists('courseid', $data)) {
            $err['courseid'] = 'Must set courseid';
        }

        if (!array_key_exists('files', $data)) {
            $err['files'] = 'Files must be set before submitting';
        } else if (!preg_match('/^[a-f0-9,]+/', $data['files'])) {
            $err['contenthashes'] = 'Invalid format for contenthashes';
        }

        return $err;
    }


}

