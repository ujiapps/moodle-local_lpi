<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A form with filter options for the review page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

use local_lpi\review_filters;

class review_data_filter_form extends \moodleform implements \renderable, \templatable {

    private $id;
    private $courseid;
    private $page;

    public function __construct($id, $courseid, $page) {
        $this->id = $id;
        $this->courseid = $courseid;
        $this->page = $page;

        parent::__construct(null, null, 'post', '', array('id' => $id));
    }

    public function set_filter_data() {
        $visibility = review_filters::get_visibility_filter();
        $authzstates = review_filters::get_authz_filter_values();
        $topic = review_filters::get_topic_filter();
        $perpage = review_filters::get_perpage_filter();
        $filename = review_filters::get_filename_filter();

        $data = array(
            'visibilitygroup[visibility]' => $visibility
        );
        foreach ($authzstates as $authz => $value) {
            $data['authzgroup[authz_' . $authz . ']'] = $value;
        }
        if ($topic !== null) {
            $data['topicsfilter'] = $topic;
        }
        $data['perpage'] = $perpage;
        $data['filename'] = $filename;

        $this->set_data($data);
    }

    /**
     *
     * @global \moodle_database $DB
     */
    public function definition() {
        $mform = &$this->_form;

        // Header.
        $mform->addElement('header', 'general', get_string('review_table_filter_options', 'local_lpi'));

        // Visibility filter.
        $options = array(
            review_filters::FILTER_VISIBILITY_ALL,
            review_filters::FILTER_VISIBILITY_HIDDEN,
            review_filters::FILTER_VISIBILITY_VISIBLES
        );
        $elements = array();
        foreach ($options as $option) {
            $element = $mform->createElement('radio');
            $element->setName('visibility');
            $element->setText(get_string('filter_visibility_' . $option, 'local_lpi'));
            $element->setValue($option);

            $elements[] = $element;
        }
        $group = $mform->addGroup($elements, 'visibilitygroup', get_string('filter_visibility', 'local_lpi'));
        $group->setAttributes(array('class' => 'lpi_visibility_filter'));

        // Authorization filter.
        $options = array(
            review_filters::FILTER_AUTHZ_NOT_PROCESSED_YET => get_string('authorization_notprocessedyet', 'local_lpi'),
            review_filters::FILTER_AUTHZ_WAITING_FOR_LICENSE => get_string('authorization_waiting', 'local_lpi'),
            review_filters::FILTER_AUTHZ_NOT_AUTHORIZED => get_string('authorization_notauthz', 'local_lpi'),
            review_filters::FILTER_AUTHZ_AUTHORIZED => get_string('authorization_authorized', 'local_lpi'),
            review_filters::FILTER_AUTHZ_NOT_PRINTABLE => get_string('authorization_notprintable', 'local_lpi')
        );

        $elements = [];
        foreach ($options as $option => $value) {
            $element = $mform->createElement('checkbox');
            $element->setName('authz_' . $option);
            $element->setText($value);
            $element->setValue($option);

            $elements[] = $element;
        }
        $group = $mform->addGroup($elements, 'authzgroup', get_string('state', 'local_lpi'));
        $group->setAttributes(array('class' => 'lpi_authz_filter'));

        // Filename filter.
        $mform->addElement('text', 'filename', get_string('filename', 'local_lpi'), array('class' => 'lpi_filename_filter'));
        $mform->setType('filename', PARAM_RAW);
        $mform->addHelpButton('filename', 'filename', 'local_lpi');

        // Elements per page.
        $options = array(
            10 => 10,
            50 => 50,
            100 => 100,
            200 => 200,
            400 => 400
        );
        $mform->addElement('select', 'perpage', get_string('perpage', 'local_lpi'), $options, ['class' => 'lpi_perpage_filter']);
        $mform->setType('perpage', PARAM_INT);

        // Topics filter.
        $format = course_get_format($this->courseid);
        if ( course_format_uses_sections($format->get_format())) {

            global $DB;
            $sections = $DB->get_records('course_sections', array('course' => $this->courseid));
            $options = array(
                'gdpi:alltopics' => get_string('all')
            );
            foreach ($sections as $section) {
                $options[$section->section] = get_section_name($this->courseid, $section->section);
            }
            $mform->addElement('select', 'topicsfilter', get_string('topic'), $options, array('class' => 'lpi_topics_filter'));
        }

        // Hidden elements.
        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'page', $this->page);
        $mform->setType('page', PARAM_INT);
        $this->add_action_buttons(false, get_string('applyfilter', 'local_lpi'));
    }

    public function validation($data, $files) {
        $err = parent::validation($data, $files);

        if (array_key_exists('perpage', $data)) {
            if ($data['perpage'] < 0 || $data['perpage'] > 400) {
                $err['perpage'] = 'Cannot set such a page size!!!!';
            }
        }
        return $err;
    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'filterform' => $output->render($this)
        );
    }
}

