<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The renderable for the detailed review navigation.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

use \local_lpi\objects\authorization_state;

class detailed_review_navigation implements \renderable, \templatable {

    private $modtitles;

    private $filename;
    private $fileurl;

    private $reviewurl;

    private $fileids;
    private $currentfileid;
    private $state;
    private $statedesc;

    private $isnotprocessed;

    /**
     *
     * @param \local_lpi\orm\file_metadata $filemetadata
     * @param type $reviewurl
     * @param type $fileids
     * @param type $currentfileid
     * @param type $state
     * @param type $statedesc
     */
    public function __construct(
            $filemetadata,
            $reviewurl,
            $fileids,
            $currentfileid,
            $state,
            $statedesc
    ) {

        $modules = array_map(function ($mod) {
            return $mod->name;
        }, $filemetadata->get_modules());
        $sections = array_map(function ($section) {
            if ($section->name) {
                return $section->name;
            } else {
                return get_string('section') . " " . $section->section;
            }
        }, $filemetadata->get_sections());

        $this->modtitles = implode(',', array_merge($modules, $sections));

        $storedfile = $filemetadata->get_files()[0];
        $this->filename = $storedfile->get_filename();

        $this->fileurl = \moodle_url::make_pluginfile_url(
                $storedfile->get_contextid(),
                $storedfile->get_component(),
                $storedfile->get_filearea(),
                $storedfile->get_itemid(),
                $storedfile->get_filepath(),
                $storedfile->get_filename()
        );
        $this->reviewurl = (string) $reviewurl;

        $this->fileids = $fileids;
        $this->currentfileid = $currentfileid;

        list($this->state, $this->statedesc) = authorization_state::get_authorization_state_string($filemetadata);

        $this->isnotprocessed = ($filemetadata->get_state() === \local_lpi\orm\file_metadata::STATE_UNKNOWN
                                ||
                                $filemetadata->get_state() === null);

    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'modtitles' => $this->modtitles,
            'filename' => $this->filename,
            'fileurl' => $this->fileurl,
            'reviewurl' => $this->reviewurl,

            'fileids' => $this->fileids,
            'current_fileid' => $this->currentfileid,

            'state' => $this->state,
            'state_desc' => $this->statedesc,
            'isnotprocessed' => $this->isnotprocessed
        );
    }
}
