<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A form displayed at the detailed_review_page template. This is the form
 * where teachers can fill the file metadata.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\output;

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;
use local_lpi\objects\document_content;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/licenselib.php');

class file_metadata_form extends \moodleform implements \renderable, \templatable {

    private $courseid;
    private $contenthash;
    private $filename;

    public function __construct($id, $courseid, $contenthash, $filename) {
        $this->courseid = $courseid;
        $this->contenthash = $contenthash;
        $this->filename = $filename;

        parent::__construct(null, null, 'post', '', array('id' => $id, 'class' => 'file-metadata-form'));
    }

    protected function definition() {

        global $USER, $OUTPUT;

        $mform = &$this->_form;

        // Document content group.
        $elements = array();
        foreach (document_content::get_allowed_values() as $documentcontent) {
            if ($documentcontent === document_content::UNKNOWN) {
                continue;
            }
            $radioelement = $mform->createElement('radio');
            $radioelement->setName('document_content');
            $radioelement->_label = get_string('document_content_' . $documentcontent, 'local_lpi');
            $radioelement->setText(get_string('document_content_' . $documentcontent . '_help', 'local_lpi'));
            $radioelement->setValue($documentcontent);

            $elements[] = $radioelement;
        }

        $url = new \moodle_url('/help.php', array('identifier' => 'document_content', 'component' => 'local_lpi'));

        $group = $mform->addGroup(
            $elements,
            'document_content_group',
            get_string('document_content', 'local_lpi'),
            ""
        );
        $mform->addRule('document_content_group', get_string('required'), 'required', null, 'server');
        $group->setAttributes(array(
            'class' => 'document_content_field'
        ));

        // Timeendlicense.
        $usertz = \core_date::get_user_timezone($USER);
        $timefrom = new \DateTime("now", new \DateTimeZone($usertz));
        $timefrom->add(new \DateInterval("P1Y"));

        $elements = array();
        $elements[] = $mform->createElement(
                'date_selector',
                'timeendlicense',
                '',
                array(
                    'timezone' => \core_date::get_user_timezone($USER),
                    'startyear' => date('Y', time())
                    ),
                array(
                    'class' => 'lpi_metadata_field'
                )
        );
        $elements[] = $mform->createElement(
                'checkbox',
                'perpetuallicense',
                get_string('perpetuallicense', 'local_lpi')
        );

        $mform->addGroup(
                $elements,
                'licensetime',
                get_string('timeendlicense', 'local_lpi')
        );

        $mform->setType('timeendlicense', PARAM_INT);
        $mform->setDefault('timeendlicense', $timefrom->getTimestamp());
        $mform->disabledIf('licensetime', 'document_content_group[document_content]', 'neq', document_content::HAVELICENSE);

        // Hack to disable the whole timelicense field in boost. This should be reviewed in other themes / renderers.
        $what = array('day', 'month', 'year', 'calendar');
        foreach ($what as $w) {
            $mform->disabledIf('licensetime[timeendlicense][' . $w . ']', 'licensetime[perpetuallicense]', 'checked');
        }
        $mform->disabledIf('licensetime[timeendlicense]', 'licensetime[perpetuallicense]', 'checked');

        // Title.
        $mform->addElement(
                'text',
                'title',
                get_string('title', 'local_lpi'),
                array(
                    'placeholder' => get_string('title_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('title', PARAM_RAW);
        $mform->disabledIf('title', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('title', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('title', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('title', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf('title', 'document_content_group[document_content]', 'eq', document_content::NOT_PRINTED_OR_PRINTABLE);

        // Identification.
        $mform->addElement(
                'textarea',
                'identification',
                get_string('identification', 'local_lpi'),
                array(
                    'placeholder' => get_string('identification_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field',
                    'rows' => 5
                )
        );
        $mform->setType('identification', PARAM_RAW);
        $mform->addHelpButton('identification', 'identification', 'local_lpi');
        $mform->disabledIf('identification', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf(
            'identification',
            'document_content_group[document_content]',
            'eq',
            document_content::EDITED_BY_UNIVERSITY
        );
        $mform->disabledIf('identification', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('identification', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf(
            'identification',
            'document_content_group[document_content]',
            'eq',
            document_content::NOT_PRINTED_OR_PRINTABLE
        );

        // Author.
        $mform->addElement(
                'text',
                'author',
                get_string('author', 'local_lpi'),
                array(
                    'placeholder' => get_string('author_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('author', PARAM_RAW);
        $mform->disabledIf('author', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('author', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('author', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('author', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf('author', 'document_content_group[document_content]', 'eq', document_content::NOT_PRINTED_OR_PRINTABLE);

        // License.
        $mform->addElement(
                'text',
                'license',
                get_string('license', 'repository'),
                array(
                    'placeholder' => get_string('license_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('license', PARAM_RAW);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::OTHER);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::NOT_PRINTED_OR_PRINTABLE);
        $mform->disabledIf('license', 'document_content_group[document_content]', 'eq', document_content::LT10_FRAGMENT);

        // Publisher.
        $mform->addElement(
                'text',
                'publisher',
                get_string('publisher', 'local_lpi'),
                array(
                    'placeholder' => get_string('publisher_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('publisher', PARAM_RAW);
        $mform->disabledIf('publisher', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('publisher', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('publisher', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('publisher', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf(
            'publisher',
            'document_content_group[document_content]',
            'eq',
            document_content::NOT_PRINTED_OR_PRINTABLE
        );

        // Pages.
        $mform->addElement(
                'text',
                'pages',
                get_string('pages', 'local_lpi'),
                array(
                    'placeholder' => get_string('pages_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('pages', PARAM_RAW);
        $mform->addHelpButton('pages', 'pages', 'local_lpi');
        $mform->disabledIf('pages', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('pages', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('pages', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('pages', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf('pages', 'document_content_group[document_content]', 'eq', document_content::NOT_PRINTED_OR_PRINTABLE);

        // Total pages.
        $mform->addElement(
                'text',
                'totalpages',
                get_string('totalpages', 'local_lpi'),
                array(
                    'placeholder' => get_string('totalpages_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('totalpages', PARAM_INT);
        $mform->addHelpButton('totalpages', 'totalpages', 'local_lpi');
        $mform->disabledIf('totalpages', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('totalpages', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('totalpages', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('totalpages', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf(
            'totalpages',
            'document_content_group[document_content]',
            'eq',
            document_content::NOT_PRINTED_OR_PRINTABLE
        );

        // Number of enrolments.
        $mform->addElement(
                'text',
                'nenrolments',
                get_string('nenrolments', 'local_lpi'),
                array(
                    'placeholder' => get_string('nenrolments_placeholder', 'local_lpi'),
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('nenrolments', PARAM_INT);
        $mform->addHelpButton('nenrolments', 'nenrolments', 'local_lpi');
        $mform->disabledIf('nenrolments', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('nenrolments', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('nenrolments', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('nenrolments', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf(
            'nenrolments',
            'document_content_group[document_content]',
            'eq',
            document_content::NOT_PRINTED_OR_PRINTABLE
        );
        $mform->disabledIf('nenrolments', 'document_content_group[document_content]', 'eq', document_content::HAVELICENSE);

        // Comments.
        $mform->addElement(
                'textarea',
                'comments',
                get_string('comments', 'local_lpi'),
                array(
                    'placeholder' => get_string('comments_placeholder', 'local_lpi'),
                    'rows' => 8,
                    'class' => 'lpi_metadata_field'
                )
        );
        $mform->setType('comments', PARAM_RAW);
        $mform->disabledIf('comments', 'document_content_group[document_content]', 'eq', document_content::ADMINISTRATIVE);
        $mform->disabledIf('comments', 'document_content_group[document_content]', 'eq', document_content::EDITED_BY_UNIVERSITY);
        $mform->disabledIf('comments', 'document_content_group[document_content]', 'eq', document_content::OWNED_BY_TEACHERS);
        $mform->disabledIf('comments', 'document_content_group[document_content]', 'eq', document_content::PUBLIC_DOMAIN);
        $mform->disabledIf(
            'comments',
            'document_content_group[document_content]',
            'eq',
            document_content::NOT_PRINTED_OR_PRINTABLE
        );

        // Hidden params.
        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'contenthash', $this->contenthash);
        $mform->setType('contenthash', PARAM_ALPHANUM);

        /*
         * The nextfileid to show. We will fill this value from javascript.
         * see form-metadata-controller amd module.
         */

        $mform->addElement('hidden', 'nextfileid', '');
        $mform->setType('nextfileid', PARAM_ALPHANUM);

        // Review button.

        global $USER;

        $elements = array();
        $elements[] = $mform->createElement('submit', 'savebtn', get_string('send', 'local_lpi'));
        $elements[] = $mform->createElement('button', 'doubtbtn', get_string('doubt', 'local_lpi'), array(
            'title' => get_string('doubt_help', 'local_lpi'),
            'data-fileid' => $this->contenthash,
            'data-courseid' => $this->courseid,
            'data-useremail' => $USER->email,
            'data-filename' => $this->filename
        ));
        $mform->addGroup($elements, 'submitgroup', '');;

        $mform->disable_form_change_checker();
    }

    public function definition_after_data() {
        parent::definition_after_data();
        $data = $this->get_data();
        if (!$data || !isset($data->document_content_group)) {
            $validgroups = ['document_content', 'document_content_group', 'submitgroup', 'courseid', 'fileid', 'metadataid'];
            foreach ($this->_form->_elements as $element) {
                if (!in_array($element->getName(), $validgroups)) {
                    $element->updateAttributes(array('class' => 'lpi_metadata_field form_metadata_hidden'));
                }
            }
        }
    }

    /**
     * We do some extra validation on fields that we want.
     *
     * @param array $data
     * @param array $files
     */
    public function validation($data, $files) {
        $err = parent::validation($data, $files);

        if ($data['document_content_group']['document_content'] === document_content::HAVELICENSE) {
            if (!array_key_exists('licensetime', $data) || !array_key_exists('timeendlicense', $data['licensetime'])) {
                $err['licensetime'] = get_string('required');
            } else {
                $isperpetual = array_key_exists('perpetuallicense', $data['licensetime'])
                               && $data['licensetime']['perpetuallicense'];
                if (!$isperpetual && ($data['licensetime']['timeendlicense'] < time())) {
                    $err['licensetime'] = get_string('error_timeendlicense_in_the_past', 'local_lpi');
                }
            }
        }

        // Title and identification are mandatory if document_content_group is set to have license, is a fragment or other.
        $contenttotest = array(
            document_content::HAVELICENSE,
            document_content::OTHER,
            document_content::LT10_FRAGMENT
        );

        if (in_array($data['document_content_group']['document_content'], $contenttotest)) {
            if (!array_key_exists('identification', $data) || !(trim($data['identification']))) {
                $err['identification'] = get_string('required');
            }
        }

        return $err;
    }

    /**
     * Performs a set_data() in this form from a file_metadata instance.
     *
     * @param \local_lpi\orm\file_metadata $filemetadata
     */
    public function set_file_metadata (\local_lpi\orm\file_metadata $filemetadata) {

        if (!$filemetadata) {
            return;
        }

        if ($filemetadata->get_nenrolments() === null) {
            $manager = \local_lpi\manager::get_instance();
            $nenrolments = $manager->get_real_number_of_enroled_students($filemetadata->get_courseid());
        } else {
            $nenrolments = $filemetadata->get_nenrolments();
        }

        $documentcontent = (string) document_content::get_document_content_by_file_metadata($filemetadata);

        $data = array(
            'document_content_group[document_content]' => $documentcontent,
            'title' => $filemetadata->get_title(),
            'identification' => $filemetadata->get_identification(),
            'author' => $filemetadata->get_author(),
            'license' => $filemetadata->get_license(),
            'publisher' => $filemetadata->get_publisher(),
            'pages' => $filemetadata->get_pages(),
            'totalpages' => $filemetadata->get_totalpages(),
            'courseid' => $filemetadata->get_courseid(),
            'comments' => $filemetadata->get_comments(),
            'nenrolments' => $nenrolments,
            'contenthash' => $filemetadata->get_contenthash()
        );
        if ($filemetadata->get_state() === file_metadata::STATE_GT10_AND_PERM) {
            if (is_null($filemetadata->get_timeendlicense())) {
                $data['licensetime']['perpetuallicense'] = 1;
            } else {
                $data['licensetime']['timeendlicense'] = $filemetadata->get_timeendlicense();
            }
        }

        $this->set_data($data);
    }

    /**
     * Updates a file_metadata object with the data submitted and validated from
     * the form.
     *
     * @param \local_lpi\orm\file_metadata $data
     */
    public function update_file_metadata_with_data(\local_lpi\orm\file_metadata $filemetadata) {
        $data = $this->get_data();
        if (!$data) {
            return;
        }

        $documentcontent = new document_content($data->document_content_group['document_content']);
        $documentcontent->update_file_metadata($filemetadata);

        if (isset($data->title) && $data->title !== $filemetadata->get_title()) {
            $filemetadata->set_title($data->title);
        }
        if (isset($data->identification) && $data->identification !== $filemetadata->get_identification()) {
            $filemetadata->set_identification($data->identification);
        }
        if (isset($data->author) && $data->author !== $filemetadata->get_author()) {
            $filemetadata->set_author($data->author);
        }
        if (isset($data->license) && $data->license !== $filemetadata->get_license()) {
            $filemetadata->set_license($data->license);
        }
        if (isset($data->publisher) && $data->publisher !== $filemetadata->get_publisher()) {
            $filemetadata->set_publisher($data->publisher);
        }

        // If property is mine or public domain the state is ok.
        if ($filemetadata->get_property() === file_metadata::PROPERTY_PUBLICDOMAIN) {
            $filemetadata->set_state(file_metadata::STATE_OK);
        }

        if (isset($data->pages) && $data->pages !== $filemetadata->get_pages()) {
            $filemetadata->set_pages($data->pages);
        }
        if (isset($data->totalpages) && $data->totalpages !== $filemetadata->get_totalpages()) {
            $filemetadata->set_totalpages($data->totalpages);
        }

        if (isset($data->licensetime)) {
            if (array_key_exists('perpetuallicense', $data->licensetime) && $data->licensetime['perpetuallicense']) {
                $filemetadata->set_timeendlicense(null);
            } else {
                $filemetadata->set_timeendlicense($data->licensetime['timeendlicense']);
            }
        }
        if (isset($data->timeendlicense) && $data->timeendlicense !== $filemetadata->get_timeendlicense()) {
            $filemetadata->set_timeendlicense($data->timeendlicense);
        }
        if (isset($data->comments)) {
            $filemetadata->set_comments($data->comments);
        }
        if (isset($data->nenrolments)) {
            $filemetadata->set_nenrolments($data->nenrolments);
        }
    }

    public function export_for_template(\renderer_base $output) {
        return array(
            'form' => $this->render()
        );
    }
}
