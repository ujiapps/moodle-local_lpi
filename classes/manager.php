<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle files interface for LPI checks.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/assign/feedbackplugin.php');
require_once($CFG->dirroot . '/mod/assign/feedback/file/locallib.php');

use local_lpi\orm\file_metadata;

class manager {

    private static $instance = null;

    /**
     * The timestamp from with to filter courses (ie. courses created before this
     * date will be filtered by default).
     *
     * @var int
     */
    private $timecreatedfrom = null;

    /**
     * The ignored files manager.
     *
     * @var ignored_files_manager
     */
    private $ignoredfilesmanager;

    /**
     * Returns a manager instance
     *
     * @return manager
     */
    public static function get_instance($timecreatedfrom = null) {
        if (self::$instance == null) {
            if (is_null($timecreatedfrom)) {
                $timecreatedfrom = config::get_instance()->get_datefrom();
            }
            self::$instance = new manager($timecreatedfrom);
        }
        return self::$instance;
    }

    /**
     * A new instance based on timecreated.
     * @param type $timecreatedfrom
     */
    private function __construct($timecreatedfrom) {
        $this->timecreatedfrom = $timecreatedfrom;
        $this->ignoredfilesmanager = ignored_files_manager::get_instante();
    }

    /**
     * Returns an array. The first component is the sql itself. The second
     * component are the parameters.
     *
     * @param int $courseid
     * @param string $fields the fields to retrieve.
     * @returns array
     */
    public function get_file_metadata_sql($fields = null, $courseid = null) {
        global $DB;

        if (!$fields) {

            $fields = "f.id as fileid, "
                    . "f.cmid, "
                    . "f.section, "

                    . "f.contenthash, "

                    . "fm.id, "
                    . "fm.userid, "
                    . "fm.property, "
                    . "fm.type, "
                    . "fm.title, "
                    . "fm.identification, "
                    . "fm.state, "
                    . "fm.timecreated, "
                    . "fm.timemodified, "
                    . "fm.publisher, "
                    . "fm.pages, "
                    . "fm.totalpages, "
                    . "fm.rightsentity, "
                    . "fm.timeendlicense, "
                    . "fm.comments, "
                    . "fm.denyreason, "
                    . "fm.nenrolments,"
                    . "fm.lastdoubttime, "

                    . "CASE "
                    . "     WHEN fm.id IS NULL THEN f.author"
                    . "     ELSE fm.author "
                    . "END author,  "

                    . "fm.license, "

                    . "f.courseid ";
        }

        $select = "SELECT DISTINCT " . $fields . " ";

        list($selectm, $fromm, $wherem, $paramsmodules) = $this->get_module_files_sql($courseid);
        $sqlmodules = "SELECT f.*, cm.id as cmid, cs.section, cs.id as sectionid, cm.visible, "
                      .      "c.id as courseid, c.visible as coursevisible "
                      . $fromm
                      . $wherem;

        list($selectc, $fromc, $wherec, $paramscourse) = $this->get_course_files_sql($courseid);
        $sqlcourse = "SELECT f.*, NULL as cmid, cs.section, cs.id as sectionid, cs.visible, "
                      .     "c.id as courseid, c.visible as coursevisible "
                      . $fromc
                      . $wherec;

        $from = "FROM ( $sqlmodules UNION $sqlcourse ) f"
                . "   LEFT JOIN {local_lpi_file_metadata} fm ON fm.contenthash = f.contenthash and fm.courseid = f.courseid ";

        $params = array_merge($paramsmodules, $paramscourse);

        $where = "WHERE f.filename != :dirfilename_a "
                   . " AND " . $DB->sql_like('f.mimetype', ':likeaudio_a', true, true, true)
                   . " AND " . $DB->sql_like('f.mimetype', ':likevideo_a', true, true, true)
                   . " AND " . $DB->sql_like('f.mimetype', ':likeimage_a', true, true, true)
                   . " AND f.mimetype != :likeh5p_a";

        $params['dirfilename_a'] = '.';
        $params['likeaudio_a'] = 'audio/%';
        $params['likevideo_a'] = 'video/%';
        $params['likeimage_a'] = 'image/%';
        $params['likeh5p_a'] = 'application/zip.h5p';

        /*
         * Exclude files owned by students. Problem: if a student unerolls from
         * the course, a teacher may have to review its uploaded files so
         * be careful.
         */

        if (!is_null($courseid)) {
            $context = \context_course::instance($courseid);
            list($studentssql, $studentsparams) = get_enrolled_sql($context, 'local/lpi:countasstudent');

            $where .= " AND f.courseid = :courseidfm "
                    . " AND ( f.userid IS NULL "
                    . "       OR f.userid NOT IN ( $studentssql ) "
                    . "     ) ";

            $params['courseidfm'] = $courseid;
            $params = array_merge($params, $studentsparams);
        }

        // Exclude ignored files.
        list($ignoredsql, $ignoredparams) = $this->ignoredfilesmanager->get_ignored_files_sql($courseid, 'f');
        $where .= " AND f.id NOT IN ( $ignoredsql ) ";

        $params = array_merge($params, $ignoredparams);

        return array($select, $from, $where, '', $params);
    }

    /**
     * Returns SQL for module files.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @return string
     */
    private function get_module_files_sql($courseid = null) {
        global $DB;

        $select = "SELECT "
                . "f.*, "
                . "c.id as courseid, "
                . "cm.visible,"
                . "cs.section,"
                . "cs.id as sectionid ";
        $from = "FROM {files} f"
                . "   JOIN {context} ctx ON ctx.id = f.contextid AND ctx.contextlevel = :levelmodule_m "
                . "   JOIN {course_modules} cm ON cm.id = ctx.instanceid "
                . "   JOIN {course_sections} cs ON cm.section = cs.id "
                . "   JOIN {course} c ON c.id = cm.course ";
        $where = "WHERE c.id NOT IN ( SELECT courseid FROM {local_lpi_ignored_courses} ) "
                . "     AND c.timecreated >= :timecreated_m "
                . "     AND cm.deletioninprogress = 0 "
                . "     AND f.component != :component_backup "
                // Exclude assignfeedback_pdf.
                . "     AND ( ( f.component != :mod_dialogue "
                . "             AND f.component != :assignfeedbackcomponent "
                . "             AND f.filearea != :assignfeedback_file "
                . "             AND f.filearea != :assignfeedback_file_batch "
                . "             AND f.filearea != :assignfeedback_file_import ) "
                . "             OR "
                // Exclude dialogue (attachment and message).
                . "            ( f.component = 'mod_dialogue' AND f.filearea != 'attachment' AND f.filearea != 'message' )"
                . "     )";

        $params = array(
            'component_backup' => 'backup',
            'mod_dialogue' => 'mod_dialogue',
            'assignfeedbackcomponent' => 'assignfeedback_editpdf',
            'levelmodule_m' => CONTEXT_MODULE,
            'timecreated_m' => $this->timecreatedfrom,
            'assignfeedback_file' => ASSIGNFEEDBACK_FILE_FILEAREA,
            'assignfeedback_file_batch' => ASSIGNFEEDBACK_FILE_BATCH_FILEAREA,
            'assignfeedback_file_import' => ASSIGNFEEDBACK_FILE_IMPORT_FILEAREA
        );

        if ($courseid !== null) {
            $where .= ' AND c.id = :courseid_m';
            $params['courseid_m'] = $courseid;
        }

        return array($select, $from, $where, $params);
    }

    /**
     * Returns SQL for course section files.
     *
     * @param type $courseid
     * @return type
     */
    private function get_course_files_sql($courseid = null) {

        $select = "SELECT "
                . "f.*, "
                . "c.id as courseid, "
                . "cs.visible,"
                . "cs.section, "
                . "cs.id as sectionid ";
        $from = "FROM {files} f "
                . "   JOIN {context} ctx ON ctx.id = f.contextid AND ctx.contextlevel = :levelcourse_c "
                . "   JOIN {course_sections} cs ON cs.id = f.itemid "
                . "   JOIN {course} c ON c.id = cs.course AND c.id = ctx.instanceid ";
        $where = "WHERE c.timecreated >= :timecreated_c "
                . "     AND f.component != :component_backup_c "
                . "     AND f.component != :component_tool_recyclebin "
                . "     AND c.id NOT IN ( SELECT courseid FROM {local_lpi_ignored_courses} )";

        $params = array(
            'component_backup_c' => 'backup',
            'component_tool_recyclebin' => 'tool_recyclebin',
            'levelcourse_c' => CONTEXT_COURSE,
            'timecreated_c' => $this->timecreatedfrom
        );

        if ($courseid !== null) {
            $params['courseid_c'] = $courseid;
            $where .= " AND c.id = :courseid_c ";
        }

        return array($select, $from, $where, $params);
    }

    /**
     * Returns SQL for all files within a course.
     *
     * @global \moodle_database $DB
     * @param type $courseid
     * @param type $topicfilter
     * @param type $filenamefilter
     * @return type
     */
    private function get_all_files_sql($courseid, $topicfilter = null, $filenamefilter = null) {

        global $DB;

        list($select, $from, $where, $params) = $this->get_module_files_sql($courseid);
        list($selectc, $fromc, $wherec, $paramsc) = $this->get_course_files_sql($courseid);

        $subq = $select . $from . $where
                . " UNION "
                . $selectc . $fromc . $wherec;

        $select = "SELECT "
                . "aux.contenthash, "
                . "aux.courseid, "
                . "MAX(aux.filename) filename, "
                . "MAX(aux.visible) visible,"
                . "MAX(aux.timemodified) uploaddate ";

        $from = "FROM ( $subq ) aux ";

        $where = "WHERE aux.filename != :dirfilename_a "
                   . " AND " . $DB->sql_like('aux.mimetype', ':likeaudio_a', true, true, true)
                   . " AND " . $DB->sql_like('aux.mimetype', ':likevideo_a', true, true, true)
                   . " AND " . $DB->sql_like('aux.mimetype', ':likeimage_a', true, true, true)
                   . " AND aux.mimetype != :likeh5p_a";
        /*
         * Exclude files owned by students. Problem: if a student unerolls from
         * the course, a teacher may have to review its uploaded files so
         * be careful.
         */
        $context = \context_course::instance($courseid);
        list($studentssql, $studentsparams) = get_enrolled_sql($context, 'local/lpi:countasstudent');
        $where .= " AND ( aux.userid NOT IN ( $studentssql ) "
                . "          OR aux.userid IS NULL "
                . "        ) ";

        // Exclude ignored files.
        list($ignoredsql, $ignoredparams) = $this->ignoredfilesmanager->get_ignored_files_sql($courseid);
        $where .= " AND aux.id NOT IN ( $ignoredsql ) ";

        $groupby = " GROUP BY aux.contenthash, aux.courseid";

        $params = array_merge($params, $paramsc, $studentsparams, $ignoredparams);
        $params['dirfilename_a'] = '.';
        $params['likeaudio_a'] = 'audio/%';
        $params['likevideo_a'] = 'video/%';
        $params['likeimage_a'] = 'image/%';
        $params['likeh5p_a'] = 'application/zip.h5p';

        // Topic filter.
        if ($topicfilter !== null && $topicfilter !== review_filters::FILTER_TOPIC_ALL) {
            $where .= " AND aux.section = :coursesection ";
            $params['coursesection'] = $topicfilter;
        }
        // Filename filter.
        if ($filenamefilter !== null) {
            $where .= ' AND ' . $DB->sql_like('aux.filename', ':filename_filter', false, false) . ' ';
            $params['filename_filter'] = $filenamefilter;
        }

        return array($select . $from . $where . $groupby, $params);
    }

    /**
     * Gets the pending_table sql.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @param objects\authorization_state[] $authzfilters
     * @param int $visibilityfilter
     * @param string topic_filter course section to filter.
     * @return string
     */
    public function get_pending_table_sql($courseid, $authzfilters, $visibilityfilter, $topicfilter, $filenamefilter = null) {
        global $DB;

        $courseid = (int) $courseid;

        list($sqlallfiles, $params) = $this->get_all_files_sql($courseid, $topicfilter, $filenamefilter);

        $subquery = "SELECT DISTINCT "

                . " pt.contenthash, "

                . $DB->sql_cast_char2int(" CASE "
                . "    WHEN fm.type = :typenotprintable THEN :authznotprintable "
                . "    WHEN fm.type = :typeadministrative THEN :authznotnecessaryadmin "
                . "    WHEN fm.property = :propertypd THEN :authznotnecessarypublicdomain "
                . "    WHEN fm.property = :propertyteachers THEN :authznotnecessaryteachers "
                . "    WHEN fm.property = :propertyuniversity THEN :authznotnecessaryuniversity "
                . "    WHEN fm.state = :stateloe10 THEN :authznotnecessaryloe10 "
                . "    WHEN fm.state = :stategt10perm THEN :authzhavelicense "
                . "    WHEN fm.state = :stategt10 THEN :authzwaitingforlicense "
                . "    WHEN fm.state = :stateforbidden THEN :authzforbidden "
                . "    ELSE :authznotprocessedyet "
                . " END ") . " as authz, "

                . "pt.visible, "
                . "pt.filename, "
                . "pt.uploaddate, "

                . "pt.courseid, "
                . "fm.id, "
                . "fm.property, "
                . "fm.type, "
                . "fm.state, "
                . "fm.denyreason ";

        $subquery .= " FROM ( $sqlallfiles ) pt "
                . " LEFT JOIN {local_lpi_file_metadata} fm ON fm.contenthash = pt.contenthash AND fm.courseid = :courseid_pt ";

        $subquery .= "GROUP BY "
                    . "pt.contenthash, "
                    . "authz, "
                    . "pt.visible, "
                    . "pt.filename, "
                    . "pt.uploaddate, "
                    . "pt.courseid, "
                    . "fm.id, "
                    . "fm.property, "
                    . "fm.type, "
                    . "fm.state, "
                    . "fm.denyreason ";

        $params = array_merge($params, array(
            'courseid_pt' => $courseid,
            'propertypd' => file_metadata::PROPERTY_PUBLICDOMAIN,
            'stateloe10' => file_metadata::STATE_LOE10,
            'stategt10perm' => file_metadata::STATE_GT10_AND_PERM,
            'stategt10' => file_metadata::STATE_GT10,
            'stateforbidden' => file_metadata::STATE_FORBIDDEN,
            'propertyteachers' => file_metadata::PROPERTY_TEACHERS,
            'propertyuniversity' => file_metadata::PROPERTY_UNIVERSITY,

            'typeadministrative' => file_metadata::TYPE_ADMINISTRATIVE,
            'typenotprintable' => file_metadata::TYPE_NOT_PRINTED_OR_PRINTABLE,

            'authznotnecessaryplastic' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH,
            'authznotprocessedyet' => objects\authorization_state::AUTHORIZATION_NOT_PROCESSED_YET,
            'authznotnecessaryhaveauth' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH,
            'authznotnecessarypublicdomain' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN,
            'authznotnecessaryteachers' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_TEACHERS,
            'authznotnecessaryuniversity' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_UNIVERSITY,
            'authznotnecessaryadmin' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_ADMINISTRATIVE,
            'authznotprintable' => objects\authorization_state::AUTHORIZATION_NOTPRINTABLE,

            'authznotnecessaryloe10' => objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_LT10,
            'authzhavelicense' => objects\authorization_state::AUTHORIZATION_HAVE_LICENSE,
            'authzwaitingforlicense' => objects\authorization_state::AUTHORIZATION_WAITING_FOR_LICENSE,
            'authzforbidden' => objects\authorization_state::AUTHORIZATION_NOT_AUTHORIZED,
        ));

        $fields = " q.contenthash, "
                . $DB->sql_cast_char2int('q.authz').  " as authz, "
                . " q.visible, "
                . " q.filename, "
                . " q.uploaddate, "
                . " q.courseid, "
                . " q.id, "
                . " q.property, "
                . " q.type, "
                . " q.state, "
                . " q.denyreason ";

        $from = " ( $subquery ) q ";

        if ($authzfilters) {
            $authzstates = array_map(function($st) {
                return (string) $st;
            }, $authzfilters);
            list($authzsql, $authzparams) = $DB->get_in_or_equal($authzstates, SQL_PARAMS_NAMED, 'authzst');
            $where = "q.authz $authzsql ";
            $params = array_merge($params, $authzparams);
        } else {
            $where = "1 = 0 ";
        }

        if ($visibilityfilter !== null) {
            $where .= " AND q.visible = :visibility ";
            $params['visibility'] = $visibilityfilter ? 1 : 0;
        }

        return array($fields, $from, $where, $params);
    }

    /**
     * Checks wether we should notify a teacher if it has pending files to be
     * reviewed. We just notify teachers if course modules are visible too.
     *
     * @param {int} $courseid the courseid of the course to check.
     * @return {boolean} true if it has or false if not.
     */
    public function should_notify_teachers ($courseid) {
        global $DB;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql('f.id', $courseid);

        // Just UNKNOWN STATE and visible course module.
        list($insql, $inparams) = $DB->get_in_or_equal(
                array(
                    orm\file_metadata::STATE_UNKNOWN
                ),
                SQL_PARAMS_NAMED,
                'state_'
        );
        $params = array_merge($params, $inparams);

        $where .= " AND (fm.state IS NULL OR fm.state $insql) "
                . " AND f.visible = 1 ";

        return $DB->record_exists_sql($select . $from . $where, $params);
    }

    /**
     * Returns an array of file_metadata given the following conditions.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @param string|array $state a state or an array of states.
     * @param int $visibility filter by module visibility: 0 means just not visible, 1 visible and null both.
     * @param int $offset if we want pagination.
     * @param int $count the number of elements to retrieve.
     * @return orm\file_metadata[]
     */
    public function get_file_metadata (
            $courseid,
            $state = null,
            $search = null,
            $visibility = null,
            $authorizationstates = null,
            $offset = 0,
            $count = 10
    ) {
        global $DB;

        $ret = array();

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(null, $courseid);

        if ($state) {
            if (is_string($state)) {
                $state = array($state);
            }
            list($instatesql, $instateparams) = $DB->get_in_or_equal($state, SQL_PARAMS_NAMED);
            $where .= ' AND fm.state ' . $instatesql . ' ';
            $params = array_merge($params, $instateparams);
        }

        if ($search) {
            $where .= " AND ("
                    . "  fm.id IS NOT NULL "
                    . "  AND ( "
                    .          $DB->sql_like('fm.title', ':searchtitle', false, false) . " "
                    . "        OR "
                    .          $DB->sql_like('f.author', ':searchauthor', false, false) . " "
                    . "  )"
                    . ") ";

            $params['searchtitle'] = '%' . $search . '%';
            $params['searchauthor'] = '%' . $search . '%';
        }

        if ($visibility !== null) {
            $visibility = ($visibility) ? 1 : 0;
            $where .= " AND f.visible = :visibility ";
            $params['visibility'] = $visibility;
        }

        if ($authorizationstates) {
            $states = array();
            $properties = array();
            $hasnotprocessed = '';

            foreach ($authorizationstates as $authorizationstate) {
                switch ((string) $authorizationstate) {

                    case objects\authorization_state::AUTHORIZATION_NOT_PROCESSED_YET:
                        $hasnotprocessed = ' OR fm.state IS NULL ';
                        $states[] = file_metadata::STATE_UNKNOWN;
                        break;

                    case objects\authorization_state::AUTHORIZATION_DOUBT:
                        $states[] = file_metadata::STATE_DOUBT;
                        break;

                    case objects\authorization_state::AUTHORIZATION_HAVE_LICENSE:
                        $states[] = file_metadata::STATE_GT10_AND_PERM;
                        break;

                    case objects\authorization_state::AUTHORIZATION_NOT_AUTHORIZED:
                        $states[] = file_metadata::STATE_FORBIDDEN;
                        break;

                    case objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH:
                    case objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_LT10:
                    case objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN:
                        $states[] = file_metadata::STATE_LOE10;
                        $properties[] = file_metadata::PROPERTY_PUBLICDOMAIN;
                        break;

                    case objects\authorization_state::AUTHORIZATION_WAITING_FOR_LICENSE:
                        $states[] = file_metadata::STATE_GT10;
                        break;
                }
            }

            list($authzstsql, $authzstparams) = $DB->get_in_or_equal($states, SQL_PARAMS_NAMED, 'authz_state_');
            if ($properties) {
                list($authzpropsql, $authzpropparams) = $DB->get_in_or_equal($properties, SQL_PARAMS_NAMED, 'authz_prop');
                $where .= " AND ( fm.state $authzstsql OR fm.property $authzpropsql $hasnotprocessed) ";
                $params = array_merge($params, $authzpropparams, $authzstparams);
            } else {
                if ($hasnotprocessed) {
                    $where .= " AND ( fm.state $authzstsql $hasnotprocessed)";
                } else {
                    $where .= " AND fm.state $authzstsql ";
                }
                $params = array_merge($params, $authzstparams);
            }

        }

        // Get the contenthashes in the proper order.
        $files = $DB->get_records_sql(
                "SELECT DISTINCT aux.contenthash, aux.courseid, aux.orderaux, f.filename "
                . "FROM ( "
                .   "SELECT "
                    . " f.contenthash, "
                    . " f.courseid, "
                    . " CASE fm.state "
                        . " WHEN NULL THEN '0' "
                        . " WHEN 'unknown' THEN '0' "
                        . " WHEN 'doubt' THEN '0' "
                        . " WHEN 'gt10' THEN '1' "
                    . " ELSE fm.state END as orderaux, "
                    . " MIN(f.id) as minfileid "
                    . $from
                    . $where
                    . " GROUP BY f.contenthash, f.courseid, orderaux "
                . ") aux JOIN {files} f ON f.id = aux.minfileid "

                . ' ORDER BY aux.courseid, aux.orderaux, f.filename',

                $params,
                $offset,
                $count
        );
        if (!$files) {
            return array();
        }

        $contenthashes = array_keys($files);

        // Filter by contenthash.
        list($insql, $inparams) = $DB->get_in_or_equal(
                $contenthashes,
                SQL_PARAMS_NAMED,
                'pch'
        );
        $where .= " AND f.contenthash $insql ";
        $params = array_merge($params, $inparams);

        // Now get full file metadata info and group them.
        $files = $DB->get_records_sql(
                $select . $from . $where . $orderby,
                $params
        );

        $cmids = array();
        $fileids = array();

        $filesbycontenthash = array();
        foreach ($files as $f) {
            if (!array_key_exists($f->contenthash, $filesbycontenthash)) {
                $filesbycontenthash[$f->contenthash] = new file_metadata($f);
            }
            $filemetadata = $filesbycontenthash[$f->contenthash];
            $cmids = $filemetadata->get_cmids();
            $cmids[] = $f->cmid;
            $filemetadata->set_cmids($cmids);

            $fileids = $filemetadata->get_fileids();
            $fileids[] = $f->fileid;
            $filemetadata->set_fileids($fileids);
        }

        $ret = array();
        foreach ($filesbycontenthash as $key => $filemetadata) {
            $ret[] = $filemetadata;
        }
        return $ret;
    }

    /**
     *
     * @global \moodle_database $DB
     * @param type $courseid
     * @param type $state
     * @param type $search
     * @param type $visibility
     * @param objects\authorization_state[] $authorizationstates
     * @return type
     */
    public function count_file_metadata(
            $courseid,
            $state = null,
            $search = null,
            $visibility = null,
            $authorizationstates = null
    ) {
        global $DB;

        $ret = array();

        $course = $DB->get_record('course', array('id' => $courseid), 'id, idnumber', MUST_EXIST);
        $coursecontext = \context_course::instance($courseid);

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(null, $courseid);

        if ($state) {
            if (is_string($state)) {
                $state = array($state);
            }
            list($instatesql, $instateparams) = $DB->get_in_or_equal($state, SQL_PARAMS_NAMED);
            $where .= ' AND fm.state ' . $instatesql . ' ';
            $params = array_merge($params, $instateparams);
        }

        if ($search) {
            $where .= " AND ("
                    . "  fm.id IS NOT NULL "
                    . "  AND ( "
                    .          $DB->sql_like('fm.title', ':searchtitle', false, false) . " "
                    . "        OR "
                    .          $DB->sql_like('f.author', ':searchauthor', false, false) . " "
                    . "  )"
                    . ") ";

            $params['searchtitle'] = '%' . $search . '%';
            $params['searchauthor'] = '%' . $search . '%';
        }

        if ($visibility !== null) {
            $visibility = ($visibility) ? 1 : 0;
            $where .= " AND f.visible = :visibility ";
            $params['visibility'] = $visibility;
        }

        if ($authorizationstates) {
            $states = array();
            $properties = array();
            $hasnotprocessed = '';

            foreach ($authorizationstates as $authorizationstate) {
                switch ((string) $authorizationstate) {

                    case objects\authorization_state::AUTHORIZATION_NOT_PROCESSED_YET:
                        $hasnotprocessed = ' OR fm.state IS NULL ';
                        $states[] = file_metadata::STATE_UNKNOWN;
                        break;

                    case objects\authorization_state::AUTHORIZATION_DOUBT:
                        $states[] = file_metadata::STATE_DOUBT;
                        break;

                    case objects\authorization_state::AUTHORIZATION_HAVE_LICENSE:
                        $states[] = file_metadata::STATE_GT10_AND_PERM;
                        break;

                    case objects\authorization_state::AUTHORIZATION_NOT_AUTHORIZED:
                        $states[] = file_metadata::STATE_FORBIDDEN;
                        break;

                    case objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_HAVEAUTH:
                    case objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_LT10:
                    case objects\authorization_state::AUTHORIZATION_NOT_NECESSARY_PUBLIC_DOMAIN:
                        $states[] = file_metadata::STATE_LOE10;
                        $properties[] = file_metadata::PROPERTY_PUBLICDOMAIN;
                        break;

                    case objects\authorization_state::AUTHORIZATION_WAITING_FOR_LICENSE:
                        $states[] = file_metadata::STATE_GT10;
                        break;
                }
            }

            list($authzstsql, $authzstparams) = $DB->get_in_or_equal($states, SQL_PARAMS_NAMED, 'authz_state_');
            if ($properties) {
                list($authzpropsql, $authzpropparams) = $DB->get_in_or_equal($properties, SQL_PARAMS_NAMED, 'authz_prop');
                $where .= " AND ( fm.state $authzstsql OR fm.property $authzpropsql $hasnotprocessed) ";
                $params = array_merge($params, $authzpropparams, $authzstparams);
            } else {
                if ($hasnotprocessed) {
                    $where .= " AND ( fm.state $authzstsql $hasnotprocessed)";
                } else {
                    $where .= " AND fm.state $authzstsql ";
                }
                $params = array_merge($params, $authzstparams);
            }

        }

        $sql = "SELECT COUNT(DISTINCT f.contenthash) " . $from . $where;
        return $DB->count_records_sql($sql, $params);
    }

    /**
     * Returns an array of fileids to review per course.
     *
     * @param int $courseid
     */
    public function get_fileids($courseid) {

        global $DB;

        $ret = array();

        $authz = review_filters::get_authz_filter_values();
        $authzstates = objects\authorization_state::get_authorization_states_from_authz_filters($authz);

        $topic = review_filters::get_topic_filter();
        if ($topic === review_filters::FILTER_TOPIC_ALL) {
            $topic = null;
        }

        $filename = review_filters::get_filename_filter();

        $visibility = review_filters::get_visibility_filter();
        switch ($visibility) {
            case review_filters::FILTER_VISIBILITY_VISIBLES:
                $visibility = 1;
                break;
            case review_filters::FILTER_VISIBILITY_HIDDEN:
                $visibility = 0;
                break;
            default:
                $visibility = null;
                break;
        }
        $sort = output\pending_table::get_sort_for_table('pending-table');
        if (!$sort) {
            $sort = 'filename DESC';
        } else if (strpos($sort, 'filename') === false) {
            $sort .= ',filename DESC';
        }

        list($fields, $from, $where, $params) = $this->get_pending_table_sql(
            $courseid,
            $authzstates,
            $visibility,
            $topic,
            review_filters::filename_filter_to_like($filename)
        );

        $files = $DB->get_records_sql(
                "SELECT contenthash FROM " . $from . " WHERE " . $where . ' ORDER BY ' . $sort,
                $params
        );

        foreach ($files as $f) {
            $ret[] = $f->contenthash;
        }
        return $ret;
    }

    /**
     * File metadata.
     *
     * @global \moodle_database $DB
     * @param int $id the id of the file metadata to get.
     * @return null|file_metadata
     */
    public function get_file_metadata_by_id($id) {
        global $DB;

        // For performance reasons.
        $fm = $DB->get_record('local_lpi_file_metadata', array('id' => $id));
        if (!$fm) {
            return null;
        }
        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(null, $fm->courseid);
        $where .= " AND fm.id = :metadataid ";
        $params['metadataid'] = $id;

        $dbrecs = $DB->get_records_sql($select . $from . $where, $params);
        if (!$dbrecs) {
            return null;
        }

        $cmids = array();
        $fileids = array();
        foreach ($dbrecs as $r) {
            $cmids[] = $r->cmid;
            $fileids[] = $r->fileid;
        }
        $ret = new file_metadata(array_pop($dbrecs));
        $ret->set_fileids($fileids);
        $ret->set_cmids($cmids);

        return $ret;
    }

    /**
     * Returns a file_metadata object searched by courseid and contenthash.
     *
     * @global \moodle_database $DB
     * @param type $courseid
     * @param type $contenthash
     * @return null|file_metadata
     */
    public function get_file_metadata_by_contenthash($courseid, $contenthash) {
        global $DB;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(null, $courseid);
        $where .= ' AND f.contenthash = :contenthash ';

        $params['contenthash'] = $contenthash;

        $dbrec = $DB->get_records_sql($select . $from . $where, $params);
        if (!$dbrec) {
            return null;
        }

        $cmids = [];
        $fileids = [];
        $sections = [];
        foreach ($dbrec as $r) {
            $cmids[] = $r->cmid;
            $fileids[] = $r->fileid;
            if (is_null($r->cmid) && !is_null($r->section)) {
                $sections[] = $r->section;
            }
        }
        $ret = new file_metadata(array_pop($dbrec));
        $ret->set_fileids($fileids);
        $ret->set_cmids($cmids);
        $ret->set_sectionnums($sections);

        return $ret;
    }

    /**
     * Given a courseid and a list of contenthashes, returns an array of
     * file_metadata.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @param string[] $contenthash
     * @return null|file_metadata[]
     */
    public function get_file_metadata_by_contenthashes($courseid, $contenthashes) {

        global $DB;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(null, $courseid);

        list($insql, $inparams) = $DB->get_in_or_equal($contenthashes, SQL_PARAMS_NAMED, 'pch');

        $where .= " AND f.contenthash $insql ";

        $params = array_merge($params, $inparams);

        $dbrec = $DB->get_records_sql($select . $from . $where, $params);
        if (!$dbrec) {
            return null;
        }

        $ret = array();

        foreach ($dbrec as $r) {
            if (!array_key_exists($r->contenthash, $ret)) {
                $ret[$r->contenthash] = array(
                    'cmids' => array(),
                    'fileids' => array(),
                    'sectionnums' => array()
                );
            }
            $ret[$r->contenthash]['cmids'][] = $r->cmid;
            $ret[$r->contenthash]['fileids'][] = $r->fileid;
            if (is_null($r->cmid) && !is_null($r->section)) {
                $ret[$r->contenthash]['sectionnums'][] = $r->section;
            }
            if (!array_key_exists('file_metadata', $ret[$r->contenthash])) {
                $ret[$r->contenthash]['file_metadata'] = new file_metadata($r);
            }
        }
        $aux = array();
        foreach ($ret as $contenthash => $fm) {
            $fm['file_metadata']->set_fileids($fm['fileids']);
            $fm['file_metadata']->set_cmids($fm['cmids']);
            $fm['file_metadata']->set_sectionnums($fm['sectionnums']);
            $aux[$contenthash] = $fm['file_metadata'];
        }

        return $aux;
    }

    /**
     * A helper function to get the sql of the files to review sql.
     *
     * @global \moodle_database $DB
     * @param string $search
     * @return string
     */
    private function get_pending_files_to_review_externally_sql($search) {
        global $DB;

        // We write this query like this for performance reasons.
        $sql = "SELECT DISTINCT fm.id, fm.title, fm.author, fm.state, fm.userid as metadatauserid, "
                . "u.idnumber as metadatauseridnumber, u.email as metadatauseremail, fm.timemodified as metadatatimecreated "
                . "FROM {local_lpi_file_metadata} fm "
                . "JOIN {files} f ON fm.contenthash = f.contenthash "
                . "JOIN {course} c ON fm.courseid = c.id "
                . "JOIN {context} cctx ON cctx.instanceid = c.id AND cctx.contextlevel = :contextcourse_1 "
                . "JOIN {context} ctx ON f.contextid = ctx.id AND ( "
                . "     ( ctx.path LIKE " . $DB->sql_concat('\'%/\'', 'cctx.id', '\'/%\'')
                . " AND ctx.contextlevel > :contextcourse_2) OR (ctx.contextlevel = :contextcourse_3 AND cctx.id = ctx.id) ) "
                . "JOIN {user} u ON fm.userid = u.id "
                . "WHERE fm.state = :stategt10 "
                . "      AND f.filename != :dirfilename_a "
                . "      AND " . $DB->sql_like('f.mimetype', ':likeaudio_a', true, true, true)
                . "      AND " . $DB->sql_like('f.mimetype', ':likevideo_a', true, true, true)
                . "      AND " . $DB->sql_like('f.mimetype', ':likeimage_a', true, true, true)
                . "      AND f.mimetype != :likeh5p_a "
                . "      AND c.timecreated >= :timecreated "
                . "      AND c.id NOT IN ( SELECT courseid FROM {local_lpi_ignored_courses} ) "
                . "      AND f.id NOT IN ( "
                .               " SELECT ifiles.fileid FROM {local_lpi_ignored_files} ifiles WHERE ifiles.courseid = f.id "
                .        ") ";

        $params = array();
        $params['dirfilename_a'] = '.';
        $params['likeaudio_a'] = 'audio/%';
        $params['likevideo_a'] = 'video/%';
        $params['likeimage_a'] = 'image/%';
        $params['likeh5p_a'] = 'application/zip.h5p';
        $params['contextcourse_1'] = CONTEXT_COURSE;
        $params['contextcourse_2'] = CONTEXT_COURSE;
        $params['contextcourse_3'] = CONTEXT_COURSE;
        $params['stategt10'] = 'gt10';
        $params['timecreated'] = $this->timecreatedfrom;

        if ($search) {
            $sql .= " AND ( "
                 .          $DB->sql_like('fm.title', ':searchtitle', false, false) . " "
                 . "        OR "
                 .          $DB->sql_like('fm.author', ':searchauthor', false, false) . " "
                 . "      )";
            $params['searchtitle'] = '%' . $search . '%';
            $params['searchauthor'] = '%' . $search . '%';
        }

        return array($sql, $params);
    }

    /**
     * Gets information about metadata that must be reviewed externally.
     * This method is used mainly from \local_lpi_external::get_pending_files().
     *
     * @param string $search a string to search in author or title.
     * @param int $limitfrom
     * @param int $limitnum
     * @return \stdClass[]
     */
    public function get_pending_files_to_review_externally($search = '', $limitfrom = 0, $limitnum = 10) {
        global $DB;
        list($sql, $params) = $this->get_pending_files_to_review_externally_sql($search);
        return $DB->get_records_sql($sql, $params, $limitfrom, $limitnum);
    }

    /**
     * Counts the total number of files to review externally.
     *
     * @global \moodle_database $DB
     * @param string $search
     */
    public function count_pending_files_to_review_externally($search = '') {
        global $DB;
        list($sql, $params) = $this->get_pending_files_to_review_externally_sql($search);
        return $DB->count_records_sql("SELECT COUNT(*) FROM ($sql) aux", $params);
    }

    /**
     * Updates the fields of the {files} table.
     *
     * @param \local_lpi\orm\file_metadata $fm
     */
    private function update_stored_file_data(orm\file_metadata $fm) {
        $files = $fm->get_files();
        foreach ($files as $storedfile) {
            if ($storedfile->get_author() !== $fm->get_author()) {
                $storedfile->set_author($fm->get_author());
            }
        }
    }

    /**
     * Adds or updates a file_metadata instance to the db.
     *
     * @global \moodle_database $DB
     * @param orm\file_metadata $filemetadata the metadata to be updated.
     */
    public function add_file_metadata(orm\file_metadata $filemetadata) {
        global $DB;

        $trx = $DB->start_delegated_transaction();

        if ($filemetadata->get_id() === null) {
            $filemetadata->set_timecreated(time());
            $filemetadata->set_timemodified($filemetadata->get_timecreated());
            $id = $DB->insert_record('local_lpi_file_metadata', $filemetadata->to_db());
            $filemetadata->set_id($id);
        } else {
            $filemetadata->set_timemodified(time());
            $DB->update_record('local_lpi_file_metadata', $filemetadata->to_db());
        }

        // Log the change.
        $filemetadatalog = new orm\file_metadata_log($filemetadata);
        $DB->insert_record('local_lpi_file_metadata_log', $filemetadatalog->to_db());

        $trx->allow_commit();
    }

    /**
     * Gets the metadata that hasn't been assigned a rights management entity.
     *
     * @global \moodle_database
     * @param int $offset
     * @param int $count
     * @return int[]
     */
    public function get_file_metadata_ids_without_rights_entity($offset = 0, $count = 0) {

        global $DB;

        list($stateinsql, $params) = $DB->get_in_or_equal(
                array(
                    orm\file_metadata::STATE_GT10,
                    orm\file_metadata::STATE_GT10_AND_PERM,
                    orm\file_metadata::STATE_LOE10,
                    orm\file_metadata::STATE_OK
                ),
                SQL_PARAMS_NAMED,
                'state'
        );

        $metadataids = [];

        $sql = 'SELECT fm.id '
                . 'FROM {local_lpi_file_metadata} fm '
                . 'WHERE fm.state ' . $stateinsql . ' '
                . '      AND fm.rightsentity IS NULL';

        $rs = $DB->get_recordset_sql($sql, $params, $offset, $count);
        foreach ($rs as $r) {
            $metadataids[] = $r->id;
        }
        $rs->close();

        return $metadataids;
    }

    /**
     * This is a function to do a bulk rights entity manager setup.
     *
     * @global \moodle_database $DB
     * @param string $entity the rights entity manager to set.
     * @param int[] $metadataids the ids of the file_metadata
     */
    public function update_rights_entity_manager_for_files($entity, $metadataids) {

        global $DB;

        list($sqlinfileids, $params) = $DB->get_in_or_equal($metadataids, SQL_PARAMS_NAMED);

        $sql = "UPDATE {local_lpi_file_metadata} "
                . "SET rightsentity = :entity "
                . "WHERE id $sqlinfileids";

        $params['entity'] = $entity;

        $DB->execute($sql, $params);
    }

    /**
     * Devuelve un array con datos sobre los módulos que hay que ocultar. Cada
     * elemento del array es un objeto con los siguientes campos:
     *
     *     cmid. El id del módulo de curso que tenemos que ocultar.
     *
     *     timecreated. El timestamp del momento en que se creó el fichero o
     *                  se rellenaron los metadatos de los ficheros del módulo.
     *
     *     userid. El userid que creó el fichero o que rellenó metadatos.
     *
     *     timezone. The timezone for the user.
     *
     * Esta función no excluye los fines de semana. Hay que hacerlo
     * externamente.
     *
     * @global \moodle_database $DB
     * @param int $duetime
     * @return \stdClass[] array \stdClass
     */
    public function get_cms_to_hide ($duetime) {
        global $DB;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql();
        $select = "SELECT DISTINCT "
                    . "f.id, "
                    . "f.cmid, "
                    . "( CASE WHEN fm.id IS NULL THEN f.timecreated ELSE fm.timecreated END ) AS timecreated, "
                    . "( CASE WHEN fm.id IS NULL THEN f.userid ELSE fm.userid END ) AS userid, "
                    . "f.courseid ";

        $states = [null, file_metadata::STATE_UNKNOWN, file_metadata::STATE_GT10];

        list($instatessql, $instateparams) = $DB->get_in_or_equal($states, SQL_PARAMS_NAMED, 'state');
        $params = array_merge($params, $instateparams);

        $where .= " AND f.coursevisible = :coursevisible "
                . " AND ( "
                               // If it has metadata, use the timecreated field
                               // in {local_lpi_file_metadata} table.
                . "            (fm.id IS NOT NULL "
                . "             AND ( "
                . "                    (fm.timecreated < :duetime2 "
                . "                     AND ( fm.state IS NULL OR fm.state $instatessql) )"
                . "                    OR "
                . "                    (fm.state = :forbiddenstate) "
                . "                  ) "
                . "            ) "
                . " ) ";

        $params['coursevisible'] = 1;
        $params['duetime1'] = $duetime;
        $params['duetime2'] = $duetime;
        $params['forbiddenstate'] = file_metadata::STATE_FORBIDDEN;

        return $DB->get_records_sql($select . $from . $where, $params);
    }

    /**
     * Returns an array of file metadata with expired licenses.
     *
     * @param int $limitfrom the offset from wich to start.
     * @param int $limitnum the page size.
     * @return orm\file_metadata[] array of ids of expired metadata.
     */
    public function get_expired_license_metadata($limitfrom, $limitnum) {

        global $DB;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql();

        list($instatessql, $instatesparams) = $DB->get_in_or_equal(
            array(
                orm\file_metadata::STATE_GT10_AND_PERM
            ),
            SQL_PARAMS_NAMED,
            'param_state'
        );

        $where .= " AND fm.state $instatessql "
                . " AND fm.timeendlicense IS NOT NULL "
                . " AND fm.timeendlicense < :now "
                . " AND fm.id IS NOT NULL";

        $params = array_merge($params, $instatesparams);
        $params['now'] = time();

        $dbrecs = $DB->get_records_sql($select . $from . $where, $params, $limitfrom, $limitnum);
        return array_map(function($dbrec) {
            return new orm\file_metadata($dbrec);
        }, $dbrecs);
    }

    /**
     * Sets the state of a set of file_metadatas.
     *
     * @param \moodle_database $DB
     * @param string $state the state to set.
     * @param int[] $ids an array of ids of file_metadata to set.
     */
    public function update_state ($state, $ids) {

        global $DB;

        if (!in_array($state, orm\file_metadata::get_allowedstates())) {
            throw new exception\invalid_state_exception(null, $state);
        }

        list($sqlinid, $params) = $DB->get_in_or_equal($ids, SQL_PARAMS_NAMED);

        $DB->set_field_select(
                'local_lpi_file_metadata',
                'state',
                $state,
                "id $sqlinid",
                $params
        );
    }

    /**
     * Calculates the number of enrolled students (ie. those with
     * local/lpi:countasstudent) at present.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @return int
     */
    public function get_real_number_of_enroled_students($courseid) {
        global $DB;
        $context = \context_course::instance($courseid);
        list($sql, $params) = get_enrolled_sql(
                $context,
                'local/lpi:countasstudent',
                0,
                true,
                false
        );
        $sql = "SELECT COUNT(*) FROM ( $sql ) e";
        return $DB->count_records_sql($sql, $params);
    }

    /**
     * Inserts file_metadata files not inserted yet into file_metadata table
     * as file_metadata in unkwnown state.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     */
    public function insert_unknown_files_into_file_metadata($courseid) {

        global $DB, $USER;

        $now = time();

        $courseid = (int) $courseid;
        $userid = (int) $USER->id;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(
            'f.contenthash, '
            . ':unknownstate as state, '
            . "f.courseid, "
            . "{$USER->id} as userid, "
            . "{$now} as timecreated, "
            . "{$now} as timemodified ",

            (int) $courseid
        );

        $params['unknownstate'] = file_metadata::STATE_UNKNOWN;
        $params['userid'] = (int) (get_admin()->id);

        $query = $select . $from . $where . " AND fm.id IS NULL";
        if ($DB->record_exists_sql($query, $params)) {
            // Courseid and contenthash have a unique index so no duplicate records can be inserted.
            $sql = "INSERT INTO {local_lpi_file_metadata} "
                    . "(contenthash, state, courseid, userid, timecreated, timemodified) "
                    . $select
                    . $from
                    . $where
                    . " AND fm.id IS NULL ";

            $DB->execute($sql, $params);
        }

        $this->update_files_table_by_courseid($courseid);
    }

    /**
     * Inserts files into the local_lpi_files that has not been inserted yet.
     *
     * @param int $courseid
     */
    private function update_files_table_by_courseid($courseid) {

        global $DB;

        list($select, $from, $where, $orderby, $params) = $this->get_file_metadata_sql(
                'fm.id as metadataid, '
                . 'f.id as fileid, '
                . 'f.userid as userid, '
                . 'f.timecreated as timecreated, '
                . 'f.filename as filename ',
                $courseid
        );

        $from .= " LEFT JOIN {local_lpi_files} lf ON lf.metadataid = fm.id AND lf.fileid = f.id ";
        $where .= " AND fm.id IS NOT NULL "
                . " AND lf.id IS NULL";

        $params['lfcourseid'] = $courseid;
        if ($DB->record_exists_sql($select . $from . $where, $params)) {
            $DB->execute("INSERT INTO {local_lpi_files} "
                    . "(metadataid, fileid, userid, timecreated, filename) "
                    . $select . $from .$where,
                    $params
            );
        }
    }

    /**
     * Returns a recordset with courses that must be considered in order to
     * check for files. It is used mainly from insert_orphaned_files task.
     *
     * @return \moodle_recordset
     */
    public function get_courses_to_consider() {
        global $DB;
        return $DB->get_recordset_sql(
                'SELECT c.id '
                . 'FROM {course} c '
                . 'WHERE c.timecreated >= :timecreated',
                array('timecreated' => $this->timecreatedfrom)
        );
    }

    /**
     * Sets the document content to the given files with contenthashes specified
     * at the give course.
     *
     * @global \moodle_database $DB
     * @param \local_lpi\objects\document_content $content
     * @param int $courseid
     * @param array $contenthashes
     *
     */
    public function apply_massive_document_content(objects\document_content $content, $courseid, $contenthashes) {

        global $DB, $USER;

        // All or nothing.
        $trx = $DB->start_delegated_transaction();

        $filemetadatas = $this->get_file_metadata_by_contenthashes($courseid, $contenthashes);
        foreach ($filemetadatas as $filemetadata) {
            $content->update_file_metadata($filemetadata);
            $filemetadata->set_userid($USER->id);

            $this->add_file_metadata($filemetadata);
            // Trigger the file_reviewd event.
            $event = \local_lpi\event\file_reviewed::create_from_file_metadata($filemetadata);
            $event->trigger();
        }

        $trx->allow_commit();
    }

    /**
     * Determine if a course is an ignored course or not.
     *
     * @param boolean true if is an ignored course or false if not.
     */
    public function is_ignored_course ($courseid) {
        global $DB;
        return $DB->record_exists('local_lpi_ignored_courses', array('courseid' => $courseid));
    }

    /**
     * Returns a list of externally reviewed events.
     *
     * @param int $limitfrom
     * @param int $limitnum
     * @global \moodle_database
     * @return \core\event\base
     */
    public function get_externally_reviewed_events($limitfrom, $limitnum) {

        $ret = [];

        $logmanager = \get_log_manager();
        $readers = $logmanager->get_readers('\core\log\sql_reader');
        foreach ($readers as $reader) {
            $events = $reader->get_events_select('eventname = :eventname', [
                'eventname' => '\local_lpi\event\file_externally_reviewed'
            ], 'id', $limitfrom, $limitnum);

            $ret = array_merge($ret, $events);
            if (count($ret) > $limitnum) {
                break;
            }
        }

        return $ret;
    }

    /**
     * Counts the number of externally reviewed events.
     *
     * @returns int
     */
    public function get_externally_reviewed_events_count() {

        $ret = 0;

        $logmanager = \get_log_manager();
        $readers = $logmanager->get_readers('\core\log\sql_reader');

        foreach ($readers as $reader) {
            $count = $reader->get_events_select_count('eventname = :eventname', [
                'eventname' => '\local_lpi\event\file_externally_reviewed'
            ]);
            $ret += $count;
        }
        return $ret;
    }
}
