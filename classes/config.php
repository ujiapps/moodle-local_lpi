<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A helper to access plugin configuration.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi;

defined('MOODLE_INTERNAL') || die();

class config {

    /**
     * The singleton instance.
     * @var config
     */
    private static $instance;

    /**
     * The text for the notification at course page.
     * @var string
     */
    private $notificationtext;

    /**
     * The period of time before a file without metadata is goind to be hidden.
     * @var int
     */
    private $deadlineduration;

    /**
     * The date from which courses will be processed,
     * @var int
     */
    private $datefrom;

    /**
     * An email address to send doubt messages to.
     * @var type
     */
    private $doubtsemail;

    /**
     * An email for sending pending to review files notification externally.
     *
     * @var string
     */
    private $pendingreviewemail;

    /**
     * The url of the external web tool used to review files externally.
     *
     * @var strings
     */
    private $reviewweburl;

    /**
     * The url to the help guide.
     *
     * @var string
     */
    private $guideurl;

    /**
     * A salt used to pseudonymise the courseid of a course. Used in the
     * audit subsystem.
     *
     * @var string
     */
    private $courseidsalt;

    /**
     * Enable/Disable the \local_lpi\task\hide_expired_modules task.
     *
     * @var int
     */
    private $enableactivityhiding;

    /**
     * Returns a config instance.
     *
     * @return config
     */
    public static function get_instance() {
        return new config();
    }

    private function __construct() {
        $config = get_config('local_lpi');

        $this->notificationtext = $config->notificationtext;
        $this->deadlineduration = $config->deadlineduration;
        $this->datefrom = 1498780800;
        $this->doubtsemail = $config->doubtsemail;
        $this->pendingreviewemail = $config->pendingreviewemail;
        $this->reviewweburl = $config->reviewweburl;
        $this->guideurl = $config->guideurl;
        $this->courseidsalt = isset($config->courseidsalt) ? $config->courseidsalt : null;
        $this->enableactivityhiding = isset($config->enableactivityhiding) ? $config->enableactivityhiding : 0;
    }

    public function get_notificationtext() {
        return $this->notificationtext;
    }

    public function get_deadlineduration() {
        return $this->deadlineduration;
    }

    public function get_datefrom() {
        return $this->datefrom;
    }

    public function get_doubtsemail() {
        return $this->doubtsemail;
    }

    public function get_pendingreviewemail() {
        return $this->pendingreviewemail;
    }

    public function get_reviewweburl() {
        return $this->reviewweburl;
    }

    public function get_guideurl() {
        return $this->guideurl;
    }

    public function get_enableactivityhiding() {
        return $this->enableactivityhiding;
    }

    public function set_enableactivityhiding($value) {
        $value = $value ? 1 : 0;
        set_config('enableactivityhiding', $value, 'local_lpi');
        $this->enableactivityhiding = $value;
    }

    public function get_courseidsalt() {
        return $this->courseidsalt;
    }
    /**
     * Sets the salt used to pseudonymise the courseid returned in auditing
     * system.
     *
     * @param string $salt
     */
    public function set_courseidsalt($salt) {
        if ($this->courseidsalt) {
            throw new exception\courseid_salt_already_exists_exception();
        }
        $this->courseidsalt = $salt;
        set_config('courseidsalt', $salt, 'local_lpi');
    }
}
