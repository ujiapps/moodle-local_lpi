<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A queue manager.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi;

defined('MOODLE_INTERNAL') || die();

class doubt_queue_manager {

    const DOUBT_STEP = 10;

    /**
     * The singleton instance.
     *
     * @var doubt_queue_manager
     */
    private static $instance;

    /**
     * Returns the singleton instance.
     *
     * @return doubt_queue_manager
     */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
    }

    /**
     * Gets pending messages.
     *
     * @global \moodle_database $DB
     * @return orm\doubt[]
     */
    public function get($limitnum = self::DOUBT_STEP) {
        global $DB;
        $dbrecs = $DB->get_records('local_lpi_doubt_queue', array('timesent' => null), 'id ASC', '*', 0, $limitnum);
        return array_map(function($doubt) {
            return new orm\doubt($doubt);
        }, $dbrecs);
    }

    /**
     * Updates a doubt record.
     *
     * @global \moodle_database $DB
     * @param \local_lpi\orm\doubt $doubt
     */
    public function update(orm\doubt $doubt) {
        global $DB;
        $DB->update_record('local_lpi_doubt_queue', $doubt->to_db());
    }

    /**
     * Deletes sent messages.
     *
     * @global \moodle_database $DB
     */
    public function delete_sent() {
        global $DB;
        $DB->delete_records('local_lpi_doubt_queue', array('sent' => 1));
    }

    /**
     * Enqueues a new doubt.
     *
     * @global \moodle_database $DB
     * @param \local_lpi\orm\doubt $doubt
     */
    public function enqueue(orm\doubt $doubt) {
        global $DB;
        $id = $DB->insert_record('local_lpi_doubt_queue', $doubt->to_db());
        $doubt->set_id($id);
    }

    /**
     * Gets a doubt instance by id.
     *
     * @param orm\doubt $id
     */
    public function get_by_id ($id) {
        global $DB;
        $dbrec = $DB->get_record('local_lpi_doubt_queue', array('id' => $id), '*', IGNORE_MISSING);
        if (!$dbrec) {
            return null;
        }
        return new orm\doubt($dbrec);
    }
}
