<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A file has been externally reviewed.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\message;

defined('MOODLE_INTERNAL') || die();

class fileexternallyreviewed extends \core\message\message {

    const NAME = 'fileexternallyreviewed';

    /**
     *
     * @global \moodle_database $DB
     * @param \local_lpi\orm\file_metadata $filemetadata
     */
    public function __construct(\local_lpi\orm\file_metadata $filemetadata) {

        global $DB;

        $this->component = 'local_lpi';
        $this->name = 'fileexternallyreviewed';
        $this->userfrom = get_admin();
        $this->userto = \core_user::get_user($filemetadata->get_userid());
        $this->subject = get_string('external_review_subject', 'local_lpi');
        $this->courseid = $filemetadata->get_courseid();

        $a = new \stdClass();
        $a->filename = $filemetadata->get_files()[0]->get_filename();

        $courseid = $filemetadata->get_courseid();
        $course = $DB->get_record('course', array('id' => $courseid), 'id, fullname');
        if ($course) {
            $a->coursefullname = \html_writer::link(
                new \moodle_url('/course/view.php', array('id' => $courseid)),
                $course->fullname
            );
        } else {
            $a->coursefullname = '';
        }

        if ($filemetadata->get_state() === \local_lpi\orm\file_metadata::STATE_GT10_AND_PERM) {
            $a->reviewresolution = get_string('review_state_ok', 'local_lpi');
            $a->timeendlicense = userdate($filemetadata->get_timeendlicense());
        } else {
            if ($filemetadata->get_state() === \local_lpi\orm\file_metadata::STATE_FORBIDDEN) {
                $a->reviewresolution = get_string('review_state_forbidden', 'local_lpi');
            } else {
                $a->reviewresolution = get_string('review_state_ok', 'local_lpi');
            }
            $a->timeendlicense = '-';
        }

        $message = get_string('external_review_message', 'local_lpi', $a);
        if ($filemetadata->get_state() == \local_lpi\orm\file_metadata::STATE_FORBIDDEN) {
            $message .= get_string('external_review_message_deny_reason', 'local_lpi', $filemetadata->get_denyreason());
        }

        $this->fullmessage = html_to_text($message);
        $this->fullmessagehtml = format_text($message);
        $this->smallmessage = $this->fullmessage;
    }
}
