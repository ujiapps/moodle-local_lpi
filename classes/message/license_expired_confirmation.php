<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A message sent to the teachers when a license has expired.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\message;

defined('MOODLE_INTERNAL') || die();

class license_expired_confirmation extends \core\message\message {

    const NAME = 'license_expired_confirmation';

    public function __construct($courseid, $modulename, $metadataid, $userid) {
        $this->courseid = $courseid;
        $this->component = 'local_lpi';
        $this->name = self::NAME;
        $this->userfrom = get_admin();
        $this->userto = \core_user::get_user($userid);

        $this->subject = get_string(
                'license_expired_subject',
                'local_lpi',
                array(
                    'modulename' => $modulename
                )
        );

        $this->fullmessagehtml = get_string(
                'license_expired_message',
                'local_lpi',
                array(
                    'modulename' => $modulename,
                    'reviewfileurl' => (string) new \moodle_url(
                            '/local/lpi/detailed_review.php',
                            array('courseid' => $courseid, 'metadataid' => $metadataid)
                    )
                )
        );

        $this->fullmessage = format_text($this->fullmessagehtml);
        $this->smallmessage = $this->fullmessage;

        $this->notification = 1;
    }
}
