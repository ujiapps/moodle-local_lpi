<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A confirmation sent to the user when a doubt is sent.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\message;

defined('MOODLE_INTERNAL') || die();

class doubt_sent_confirmation extends \core\message\message {

    const NAME = 'doubt_sent_confirmation';

    /**
     * A confirmation message received by a user that has sent a doubt.
     *
     * @param \local_lpi\orm\doubt $doubt
     * @param \local_lpi\orm\file_metadata $filemetadata
     */
    public function __construct(\local_lpi\orm\doubt $doubt, \local_lpi\orm\file_metadata $filemetadata) {

        $this->component = 'local_lpi';
        $this->name = 'doubt_sent_confirmation';
        $this->userfrom = get_admin();
        $this->userto = \core_user::get_user($doubt->get_userid());
        $this->courseid = $doubt->get_courseid();

        $this->subject = get_string(
            'your_doubt_sent_subject',
            'local_lpi',
            array(
                'filename' => $doubt->get_files()[0]->get_filename()
            )
        );

        $messagehtml = get_string(
            'your_doubt_sent_message',
            'local_lpi',
            array(
                'filename' => $doubt->get_files()[0]->get_filename(),
                'modulename' => $filemetadata->get_modules()[0]->name,
                'doubttext' => format_text($doubt->get_message(), $doubt->get_messageformat())
            )
        );
        $messagetext = html_to_text($messagehtml);

        $this->fullmessage = $messagetext;
        $this->fullmessageformat = FORMAT_PLAIN;
        $this->fullmessagehtml = $messagehtml;
        $this->smallmessage = $messagetext;
    }
}

