<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A message sent when a module has been automatically hidden.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

namespace local_lpi\message;

defined('MOODLE_INTERNAL') || die();

class hidden_module_confirmation extends \core\message\message {

    const NAME = 'hidden_module_confirmation';

    public function __construct($courseid, $cmid, $fileid, $userid) {

        list($course, $cm) = \get_course_and_cm_from_cmid($cmid);

        $this->courseid = $courseid;
        $this->component = 'local_lpi';
        $this->name = 'hidden_module_confirmation';
        $this->userfrom = get_admin();
        $this->userto = \core_user::get_user($userid);

        $this->subject = get_string(
                'message_modulehidden_subject',
                'local_lpi',
                array(
                    'modulename' => $cm->name,
                    'courseshortname' => $course->shortname
                )
        );

        $this->fullmessagehtml = get_string(
                'message_modulehidden_message',
                'local_lpi',
                array(
                    'modulename' => $cm->name,
                    'courseshortname' => $course->fullname,
                    'reviewfileurl' => (string) new \moodle_url(
                            '/local/lpi/detailed_review.php',
                            array('courseid' => $courseid, 'fileid' => $fileid)
                    )
                )
        );

        $this->fullmessage = format_text($this->fullmessagehtml);
        $this->smallmessage = $this->fullmessage;

        $this->notification = 1;
    }

}
