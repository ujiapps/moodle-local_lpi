<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define('CLI_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/clilib.php');

use local_lpi\objects\document_content;
use local_lpi\orm\file_metadata;

/**
 * Prints the help message
 */
function print_help() {
    global $argv;

    cli_writeln("Uso: {$argv[0]} (--help) (--seed=num) --count=num\n", STDERR);
    cli_writeln("    -h, --help       Print this help message", STDERR);
    cli_writeln("    -i, --idnumber=int  The idnumber of the course to export", STDERR);
    cli_writeln("", STDERR);
    cli_writeln("Examples:\n", STDERR);
    cli_writeln("        $ {$argv[0]} --courseid=2312", STDERR);
    cli_writeln("", STDERR);
}


function get_directory_from_course_shortname($courseshortname) {
    $ret = str_replace('/', '_', $courseshortname);
    $ret = str_replace(' ', '_', $ret);
    $ret = str_replace('..', '_', $ret);
    return $ret;
}

$opts = cli_get_params([
    'help' => false,
    'idnumber' => false,
], [
    'h' => 'help',
    'i' => 'idnumber',
]);

if (count($opts[1]) > 0) {
    cli_writeln("ERROR. Unrecognized options: " . implode(",", $opts[1]), STDERR);
    print_help();
    exit(1);
}
if ($opts[0]['help']) {
    print_help();
    exit(1);
}
if (!$opts[0]['idnumber']) {
    cli_writeln("ERROR. idnumber parameter is mandatory");
    print_help();
    exit(1);
}


cli_logo();
cli_writeln("         Rocks!\n\n");

$idnumber = (int) $opts[0]['idnumber'];
$seed = ($opts[0]['seed']) ? (int) $opts[0]['seed'] : 1;

// Creo directorio temporal.
$tmpdirname = 'local_lpi_audit_' . date('Ymd');
$basetmp = make_temp_directory($tmpdirname);
if ($basetmp == null or strlen($basetmp) === 0) {
    throw new \Exception("Cannot create tmp directory");
}
$course = $DB->get_record('course', ['idnumber' => $idnumber], 'id,idnumber,shortname', MUST_EXIST);
$dirname = $basetmp . '/' . get_directory_from_course_shortname($course->shortname);
mkdir($dirname);
$fs = get_file_storage();
$manager = \local_lpi\manager::get_instance();
$file_metadatas = $manager->get_file_metadata($course->id, null, null, null, null, 0, 1000000);

foreach ($file_metadatas as $fm) {
    $documentcontent = $fm->get_document_content();
    $destdirectory = $dirname . '/' . get_directory_from_course_shortname($documentcontent->get_value());
    if (!file_exists($destdirectory)) {
        mkdir($destdirectory);
    }

    $fileids = $fm->get_fileids();
    foreach ($fileids as $fileid) {

        $storedfile = $fs->get_file_by_id($fileid);

        $filename = $storedfile->get_filename();
        $filename = pathinfo($filename, PATHINFO_BASENAME);
        $destfile = $destdirectory . '/' . $filename;
        $prefix = 0;
        while (file_exists($destfile)) {
            $filename = sprintf("%d_", $prefix) . $storedfile->get_filename();
            $destfile = $destdirectory . '/' . $filename;
            ++$prefix;
        }

        $storedfile->copy_content_to($destfile);
    }
}


cli_writeln("\nDONE!!! See you next time :-)\n");

