<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script takes random samples of each of the document types already
 * sampled and creates a folder with them.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

define('CLI_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/clilib.php');

use local_lpi\objects\document_content;
use local_lpi\orm\file_metadata;

/**
 * Prints the help message
 */
function print_help() {
    global $argv;

    cli_writeln("Uso: {$argv[0]} (--help) (--seed=num) --count=num\n", STDERR);
    cli_writeln("    -h, --help       Print this help message", STDERR);
    cli_writeln("    -s, --seed=int   A seed for the congruential number generator", STDERR);
    cli_writeln("    -c, --count=int  The number of samples to take", STDERR);
    cli_writeln("", STDERR);
    cli_writeln("Examples:\n", STDERR);
    cli_writeln("    To take 100 files for each type with seed 1:", STDERR);
    cli_writeln("        $ {$argv[0]} --seed=1 --count=100", STDERR);
    cli_writeln("", STDERR);
}

$opts = cli_get_params([
    'help' => false,
    'seed' => false,
    'count' => false
], [
    'h' => 'help',
    's' => 'seed',
    'c' => 'count'
]);

if (count($opts[1]) > 0) {
    cli_writeln("ERROR. Unrecognized options: " . implode(",", $opts[1]), STDERR);
    print_help();
    exit(1);
}
if ($opts[0]['help']) {
    print_help();
    exit(1);
}
if (!$opts[0]['count']) {
    cli_writeln("ERROR. count parameter is mandatory");
    print_help();
    exit(1);
}


cli_logo();
cli_writeln("         Rocks!\n\n");

$count = (int) $opts[0]['count'];
$seed = ($opts[0]['seed']) ? (int) $opts[0]['seed'] : 1;

/***
 * Voy a coger count muestras por cada tipo de documento. Para hacerlo haré
 * lo siguiente:
 *
 *    * Divido el conjunto de muestras de cada grupo por 1000
 *
 *    * Determino cuántas muestras (n) debo tomar de cada grupo para satisfacer
 *      count muestras totales
 *
 *    * Tomo n muestras del primer grupo del tipo escogido, n del segundo, etc.
 */

$paramsbydocumentcontent = [
    document_content::ADMINISTRATIVE => [
        'type' => local_lpi\orm\file_metadata::TYPE_ADMINISTRATIVE,
    ],

    document_content::EDITED_BY_UNIVERSITY => [
        'type' => file_metadata::TYPE_PRINTED_OR_PRINTABLE,
        'property' => file_metadata::PROPERTY_UNIVERSITY
    ],

    document_content::HAVELICENSE => [
        'type' => file_metadata::TYPE_PRINTED_OR_PRINTABLE,
        'state' => file_metadata::STATE_GT10_AND_PERM
    ],

    document_content::LT10_FRAGMENT => [
        'type' => file_metadata::TYPE_PRINTED_OR_PRINTABLE,
        'state' => file_metadata::STATE_LOE10
    ],

    document_content::NOT_PRINTED_OR_PRINTABLE => [
        'type' => file_metadata::TYPE_NOT_PRINTED_OR_PRINTABLE
    ],

    document_content::OTHER => [
        'type' => file_metadata::TYPE_PRINTED_OR_PRINTABLE,
        'state' => file_metadata::STATE_GT10
    ],

    document_content::OWNED_BY_TEACHERS => [
        'type' => file_metadata::TYPE_PRINTED_OR_PRINTABLE,
        'property' => file_metadata::PROPERTY_TEACHERS
    ],

    document_content::PUBLIC_DOMAIN => [
        'type' => file_metadata::TYPE_PRINTED_OR_PRINTABLE,
        'property' => file_metadata::PROPERTY_PUBLICDOMAIN
    ]
];

srand($seed);

$basetmp = make_temp_directory('local_lpi_samples');
remove_dir($basetmp, true);

$fs = get_file_storage();


foreach ($paramsbydocumentcontent as $documentcontent => $params) {

    $tmpdirname = 'local_lpi_samples/' . $documentcontent;
    if (is_dir($tmpdirname)) {
        if (rmdir($tmpdirname) === false) {
            cli_error("Cannot remove temp directory: " . $tmpdirname);
        }
    }
    $tmpdir = make_temp_directory($tmpdirname);
    if ($tmpdir === false) {
        cli_error("ERROR. Cannot create temp directory: ");
    }

    $select = "SELECT DISTINCT MAX(fm.id) as id, fm.contenthash, MAX(f.id) fileid ";
    $from = "FROM {local_lpi_file_metadata} fm "
           . " JOIN {files} f ON fm.contenthash = f.contenthash ";
    $where = "WHERE fm.type = :type ";
    if (array_key_exists('state', $params)) {
        $where .= " AND state = :state ";
    }
    if (array_key_exists('property', $params)) {
        $where .= " AND property = :property ";
    }
    $groupby = "GROUP BY fm.contenthash ";

    $recs = $DB->get_records_sql($select . $from . $where . $groupby, $params);
    if (count($recs) == 0) {
        cli_writeln("Sampling $documentcontent documents: NO DOCUMENTS");
        continue;
    }

    $fmids = array_keys($recs);

    $chosencount = (count($fmids) < $count) ? count($fmids) : $count;
    $chosenids = [];
    while ( count($chosenids) < $chosencount) {
        $pos = rand(0, count($fmids) - 1);
        $aux = array_slice($fmids, $pos, 1, true);
        $chosenids[] = current($aux);
        unset($fmids[key($aux)]);
    }

    cli_writeln("Sampling $documentcontent documents: " . count($chosenids) . " documents");

    foreach ($chosenids as $fmid) {
        $storedfile = $fs->get_file_by_id($recs[$fmid]->fileid);

        $filename = $storedfile->get_filename();
        $filename = pathinfo($filename, PATHINFO_BASENAME);
        $destfile = $tmpdir . '/' . $filename;
        $prefix = 0;
        while (file_exists($destfile)) {
            $filename = sprintf("%d_", $prefix) . $storedfile->get_filename();
            $destfile = $tmpdir . '/' . $filename;
            ++$prefix;
        }
        $storedfile->copy_content_to($tmpdir . '/' . $filename);
    }
}

cli_writeln("\nDONE!!! See you next time :-)\n");

