<?php

define('CLI_SCRIPT', true);

require_once(dirname(__FILE__) . '/../../../config.php');


$logmanager = \get_log_manager();
$readers = $logmanager->get_readers('\core\log\sql_reader');

$imports = [];


$lpimanager = \local_lpi\manager::get_instance();

$trx = $DB->start_delegated_transaction();

$coursesrs = $DB->get_recordset_sql(
    'SELECT DISTINCT courseid FROM {local_lpi_file_metadata} WHERE userid = 0 AND state IS NOT NULL AND state != :state ORDER BY courseid DESC',
    ['state' => \local_lpi\orm\file_metadata::STATE_UNKNOWN]
);
foreach ($coursesrs as $c) {
    echo "Arreglando curso {$c->courseid}:\n";

    foreach ($readers as $reader) {

        $limitfrom = 0;

        do {
            $events = $reader->get_events_select('eventname = :eventname AND courseid = :courseid', [
                'eventname' => '\\core\\event\\course_restored',
                'courseid' => $c->courseid
            ], 'timecreated', $limitfrom, 100);

            if (!$events) {
                break;
            }

            foreach ($events as $event) {
                $originalcourseid = $event->other['originalcourseid'];

                $fms = $DB->get_recordset_sql(
                    'SELECT * from {local_lpi_file_metadata} where courseid = :courseid AND userid = 0 AND state IS NOT NULL AND state != :state',
                    ['courseid' => $c->courseid, 'state' => \local_lpi\orm\file_metadata::STATE_UNKNOWN]
                );
                foreach ($fms as $fmdestino) {
                    $fmoriginal = $DB->get_record('local_lpi_file_metadata', ['courseid' => $originalcourseid, 'contenthash' => $fmdestino->contenthash]);
                    if ($fmoriginal && $fmoriginal->userid != 0) {
                        $fmdestino->userid = $fmoriginal->userid;
                        echo "    Actualizando {$fmdestino->id} / {$fmdestino->courseid} / {$fmdestino->contenthash} a usuario {$fmoriginal->userid} de curso {$fmoriginal->courseid}... ";
                        $DB->update_record('local_lpi_file_metadata', $fmdestino);
                        echo "OK\n";
                    }
                }
                $fms->close();
            }

            $limitfrom += 100;

        } while (true);

    }
}

$trx->allow_commit();

