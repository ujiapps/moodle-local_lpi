<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A command that marks a course as ignored.
 *
 * @package    local_lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

define('CLI_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/clilib.php');

/**
 * Prints help about a this command.
 */
function print_help($out = STDOUT) {
    global $argv;

    cli_writeln("Uso: {$argv[0]} --help | --courseid={courseid} | --list", $out);
    cli_writeln("", $out);
    cli_writeln("    -h, --help                  Imprime este mensaje de ayuda.", $out);
    cli_writeln("    -l, --list                  Lists the currently ignored files.", $out);
    cli_writeln("    -c, --courseid={courseid}   The id of the course to ignore.", $out);
    cli_writeln("", $out);
    cli_writeln("EJEMPLOS:", $out);
    cli_writeln("", $out);
    cli_writeln("     . Para ignorar el curso con id 50:", $out);
    cli_writeln("         ${argv[0]} --courseid=50", $out);
    cli_writeln("", $out);
    cli_writeln("     . Para listas los cursos ignorados:", $out);
    cli_writeln("         ${argv[0]} --list", $out);
    cli_writeln("", $out);
}

list($opts, $err) = cli_get_params(
    array(
        'help' => false,
        'courseid' => false,
        'list' => false
    ),
    array(
        'h' => 'help',
        'c' => 'courseid',
        'l' => 'list'
    )
);

if (count($err) > 0) {
    cli_writeln("ERROR. Opciones no reconocidas: " . implode(", ", $err), STDERR);
    print_help(STDERR);
    exit(1);
}

if ($opts['help']) {
    print_help();
    exit(0);
}

$courseid = false;
$dolist = false;

if ($opts['courseid']) {
    $courseid = (int) $opts['courseid'];
}
if ($opts['list']) {
    $dolist = true;
}

if ($courseid && $dolist) {
    cli_writeln("ERROR. No puedes marcar y listas al mismo tiempo.", STDERR);
    print_help(STDERR);
    exit(1);
}

$icmanager = \local_lpi\ignored_courses_manager::get_instance();
if ($dolist) {

    $courses = $icmanager->get_ignored_courses();
    if (count($courses) == 0) {
        cli_writeln("No hay cursos ignorados.");
        exit(0);
    }

    foreach ($courses as $c) {
        cli_writeln(sprintf("%-10s\t%-30s\t%-40s", $c->id, $c->shortname, $c->fullname));
    }

} else if ($courseid) {
    $icmanager->mark_as_ignored($courseid);
}


