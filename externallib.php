<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External functions.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/user/externallib.php');
require_once($CFG->libdir . '/licenselib.php');

use local_lpi\orm\file_metadata;
use local_lpi\objects\document_content;

class local_lpi_external extends \external_api {

    const PAGE_SIZE = 100;

    const AUTHZ_OK = 'ok';
    const AUTHZ_FORBIDDEN = 'forbidden';

    private static $validauthz = array(
        self::AUTHZ_OK,
        self::AUTHZ_FORBIDDEN
    );

    /**************************************************************************
     * local_lpi_get_pending_fileids
     */

    public static function get_pending_fileids_parameters() {
        return new external_function_parameters (
                array(
                    'courseid' => new external_value(PARAM_INT, 'The id of the course', VALUE_REQUIRED)
                )
        );
    }

    public static function get_pending_fileids($courseid) {

        $params = self::validate_parameters(
                self::get_pending_fileids_parameters(),
                array(
                    'courseid' => $courseid
                )
        );

        $context = context_course::instance($params['courseid']);
        self::validate_context($context);
        \require_capability('local/lpi:addfilemetadata', $context);

        $manager = \local_lpi\manager::get_instance();
        $fileids = $manager->get_fileids($params['courseid']);

        $ret = new \stdClass();
        $ret->count = count($fileids);
        $ret->data = $fileids;

        return $ret;
    }

    public static function get_pending_fileids_returns() {
        return new external_single_structure(
                        array(
                            'count' => new external_value(PARAM_INT),
                            'data' => new external_multiple_structure(
                                    new external_value(PARAM_ALPHANUM)
                            )
                        )
                );
    }

    /**************************************************************************
     * get_file_metadata_by_fileid
     */

    public static function add_doubt_message_parameters() {
        return new external_function_parameters(
                array(
                    'courseid' => new external_value(PARAM_INT, 'The courseid'),
                    'contenthash' => new external_value(PARAM_ALPHANUM, 'The contenthash of the file'),
                    'message' => new external_value(PARAM_RAW, 'The doubt message'),
                    'messageformat' => new external_value(PARAM_INT, 'The message format (ie FORMAT_HTML, etc.)')
                )
        );
    }
    public static function add_doubt_message($courseid, $contenthash, $message, $messageformat) {

        global $USER, $DB;

        $params = self::validate_parameters(
                self::add_doubt_message_parameters(),
                array(
                    'courseid' => $courseid,
                    'contenthash' => $contenthash,
                    'message' => $message,
                    'messageformat' => $messageformat
                )
        );

        $context = context_course::instance($params['courseid']);
        self::validate_context($context);
        \require_capability('local/lpi:addfilemetadata', $context);

        if (trim($message) === '') {
            throw new \local_lpi\exception\empty_doubt_not_allowed_exception();
        }

        // Do it transactionally.
        $trx = $DB->start_delegated_transaction();

        $lpi = local_lpi\manager::get_instance();
        $filemetadata = $lpi->get_file_metadata_by_contenthash($params['courseid'], $params['contenthash']);
        if (!$filemetadata) {
            throw new local_lpi\exception\file_not_found_exception($params['contenthash']);
        }
        $filemetadata->set_lastdoubttime(time());

        $lpi->add_file_metadata($filemetadata);

        // Finally enqueue the file.
        $doubt = new \local_lpi\orm\doubt();
        $doubt->set_message($params['message']);
        $doubt->set_messageformat($params['messageformat']);
        $doubt->set_timecreated(time());
        $doubt->set_userid($USER->id);
        $doubt->set_contenthash($params['contenthash']);
        $doubt->set_courseid($params['courseid']);

        $queuemanager = \local_lpi\doubt_queue_manager::get_instance();
        $queuemanager->enqueue($doubt);

        // Commit changes.
        $trx->allow_commit();

        return $doubt->to_db();
    }
    public static function add_doubt_message_returns() {
        return new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT),
                    'contenthash' => new external_value(PARAM_ALPHANUM),
                    'timecreated' => new external_value(PARAM_INT),
                    'userid' => new external_value(PARAM_INT),
                    'message' => new external_value(PARAM_RAW),
                    'messageformat' => new external_value(PARAM_INT)
                )
        );
    }

    /**************************************************************************
     * local_lpi_get_pending_fileids
     */

    public static function should_notify_parameters() {
        return new external_function_parameters([
            'courseid' => new external_value(PARAM_INT, 'The courseid')
        ]);
    }

    public static function should_notify($courseid) {
        $params = self::validate_parameters(self::should_notify_parameters(), [
            'courseid' => $courseid
        ]);

        $context = context_course::instance($params['courseid']);
        self::validate_context($context);
        \require_capability('local/lpi:addfilemetadata', $context);

        $manager = \local_lpi\manager::get_instance();
        return $manager->should_notify_teachers($courseid);
    }

    public static function should_notify_returns() {
        return new external_value(PARAM_BOOL, 'If we should notify a teacher that she has pending files to review');
    }

}

