Description of PDFjs library import 2.9.426
===========================================
* Download Pre-buil from https://mozilla.github.io/pdf.js/getting_started/#download

2018/05/24
----------
Updated to v2.9.426
by Juan Segarra Montesinos <juan.segarra@uji.es>
