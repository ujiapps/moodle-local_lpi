<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for the plugin.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

class local_lpi_renderer extends \plugin_renderer_base {

    /**
     * Renders the review page. Shows a table to quickly review the state of
     * the files.
     *
     * @param \local_lpi\output\review_page $renderable the renderable for the page.
     * @return string
     */
    public function render_review_page(\local_lpi\output\review_page $renderable) {
        return $this->render_from_template(
                'local_lpi/review_page',
                $renderable->export_for_template($this)
        );
    }

    public function render_pending_table (local_lpi\output\pending_table $pendingtable) {
        ob_start();
        $pendingtable->out($pendingtable->get_perpage(), true);
        $table = ob_get_contents();
        ob_end_clean();

        return $table;
    }

    public function render_reviewpage_detailed(\local_lpi\output\reviewpage_detailed $renderable) {
        return $this->render_from_template(
                'local_lpi/reviewpage_detailed',
                $renderable->export_for_template($this)
        );
    }

    public function render_detailed_review_page(\local_lpi\output\detailed_review_page $renderable) {
        return $this->render_from_template(
                'local_lpi/detailed_review_page',
                $renderable->export_for_template($this)
        );
    }

    public function render_file_metadata_form(local_lpi\output\file_metadata_form $renderable) {
        return $renderable->render();
    }

    public function render_pending_table_col_file(local_lpi\output\pending_table_col_file $renderable) {
        return $this->render_from_template(
                'local_lpi/pending_file',
                $renderable->export_for_template($this)
        );
    }

    public function render_pending_table_col_action(local_lpi\output\pending_table_col_action $renderable) {
        return $this->render_from_template(
                'local_lpi/pending_table_col_action',
                $renderable->export_for_template($this)
        );
    }

    public function render_review_data_filter_form(local_lpi\output\review_data_filter_form $renderable) {
        return $renderable->render();
    }

    public function render_massive_actions_form (local_lpi\output\massive_actions_form $renderable) {
        return $renderable->render();
    }
}
