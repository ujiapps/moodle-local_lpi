<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Paquet d'idioma català.
 *
 * @package    local
 * @subpackage lpi
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 * @copyright  2017 Universitat Jaume I (https://www.uji.es/)
 */

defined('MOODLE_INTERNAL') || die();

$string['action'] = 'Acció';
$string['applyfilter'] = 'Aplica filtres';
$string['author'] = 'Autoria';
$string['authorization'] = 'Autorització';
$string['authorization_authorized'] = 'Autoritzat';
$string['authorization_help'] = '<ul><li><em>No en necessita per extensió</em>: capítol de llibre, article de revista o extensió de fins a un 10% de l\'obra</li><li><em>Disposo de llicència en vigor</em></li><li><em>Necessito tramitar una llicència</em></li></ul>';
$string['authorization_license'] = 'Autoritzat';
$string['authorization_license_desc'] = 'S\'ha adquirit una llicència per a l\'explotació.';
$string['authorization_license_desc_help'] = 'S\'ha adquirit una llicència per a l\'explotació.';

$string['authorization_notauthz'] = 'No autoritzat';
$string['authorization_notauthz_desc'] = 'El resultat de la tramitació es negatiu i el fitxer romandrà ocult. Pot iniciar una nova tramitació. Motiu: {$a}';
$string['authorization_notauthz_desc_help'] = '<p>El resultat de la tramitació es negatiu i el fitxer romandrà ocult. Pot iniciar una nova tramitació.</p><p>---</p><p>Motiu: {$a}</p>';

$string['authorization_notnecessary'] = 'Autoritzat';
$string['authorization_notnecessary_haveauthz'] = 'Disposo d\'autorització dels autors o la universitat és titular dels drets.';
$string['authorization_notnecessary_haveauthz_help'] = 'Disposo d\'autorització dels autors o la universitat és titular dels drets.';

$string['authorization_notnecessary_lt10_desc'] = 'Per extensió de l\'obra (capítol, article o extensió de fins al 10%) no necessita autorizació. Remuneració equitativa via conveni CEDRO-VEGAP.';
$string['authorization_notnecessary_lt10_desc_help'] = 'Per extensió de l\'obra (capítol, article o extensió de fins al 10%) no necessita autorizació. Remuneració equitativa via conveni CEDRO-VEGAP.';

$string['authorization_notnecessary_pd'] = 'L\'obra està en el domini públic.';
$string['authorization_notnecessary_pd_help'] = 'L\'obra està en el domini públic.';

$string['authorization_notprocessedyet'] = 'Sense tramitar';
$string['authorization_notprocessedyet_desc'] = 'Encara no s\'ha realitzat cap acció sobre el fitxer.';
$string['authorization_notprocessedyet_desc_help'] = 'Encara no s\'ha realitzat cap acció sobre el fitxer.';

$string['authorization_query'] = 'Consulta';
$string['authorization_query_desc'] = 'Consulta formulada a especialista en drets de propietat intel·lectual. Resposta per correu electrònic.';
$string['authorization_query_desc_help'] = 'Consulta formulada a especialista en drets de propietat intel·lectual. Resposta per correu electrònic.';

$string['authorization_waiting'] = 'En tramitació';
$string['authorization_waiting_desc'] = 'Heu iniciat una tramitació que està processant el personal especialitzat en drets de propietat intel·lectual.';
$string['authorization_waiting_desc_help'] = 'Heu iniciat una tramitació que està processant el personal especialitzat en drets de propietat intel·lectual.';

$string['authorization_notnecessary_teachers'] = 'Obra els autors de la qual són professorat de la universitat i no han cedit els drets d\'explotació a tercers';
$string['authorization_notnecessary_teachers_help'] = 'Obra els autors de la qual són professorat de la universitat i no han cedit els drets d\'explotació a tercers';

$string['authorization_notnecessary_university'] = 'Obra editada pel servei de publicacions d\'aquesta universitat';
$string['authorization_notnecessary_university_help'] = 'Obra editada pel servei de publicacions de la universitat';

$string['authorization_notnecessary_administrative'] = 'Conté informació administrativa o disposicions legals';
$string['authorization_notnecessary_administrative_help'] = 'El fitxer conté normes pròpies de l’assignatura, llistats de notes, etc. O conté exclusivament el text o part del text de disposicions legals o reglamentàries, o dels seus corresponents projectes, o de resolucions d’òrgans jurisdiccionals (article 13 de l’LPI).';

$string['authorization_notnecessary_notprintable'] = 'No és una obra textual protegida ni conté fragments d’obres textuals protegides';
$string['authorization_notnecessary_notprintable_help'] = 'No és una obra textual protegida ni conté fragments d’obres textuals protegides';

$string['authorization_notprintable'] = 'No és una obra textual protegida ni conté fragments d’obres textuals protegides';
$string['authorization_notprintable_help'] = 'No és una obra textual protegida ni conté fragments d’obres textuals protegides';

$string['author_placeholder'] = 'Autoria de l\'obra';
$string['beginreview'] = 'Inicia revisió';
$string['cachedef_pendingfilesnot'] = 'Utilitzada per a recordar si un usuari ja ha estat notificat que té fitxers pendents de revisió en el curs';
$string['cannot_doubt_in_doubt'] = 'No podeu realitzar una nova consulta fins que no es resolgui l\'anterior.';
$string['cannot_edit_state_doubt'] = 'No podeu editar les metadades d\'un fitxer mentre estigui en estat de dubte. Espereu fins que els dubtes plantejats s\'hagin resolt.';
$string['cannot_review_in_doubt'] = 'No podeu editar les metadades fins que es resolgui el seu estat de revisió.';
$string['close'] = 'Tanca';
$string['comments'] = 'Observacions';
$string['comments_placeholder'] = 'Podeu escriure alguns comentaris';
$string['comments_placeholder_other'] = 'Indiqueu si disposeu d\'estructura pressupòstaria per a tramitar la llicència';
$string['deadlineduration'] = 'Termini d\'ocultació';
$string['deadlinedurationdesc'] = 'Període de temps que l\'usuari té per a emplenar les metadades del fitxer abans que l\'activitat o recurs associat s\'oculten automàticament.';
$string['detailedreviewpage'] = 'Pàgina de tramitació';
$string['document_content'] = 'Aquest fitxer';
$string['doubt'] = 'Consulta';
$string['doubtsemail'] = 'Correu electrònic per als dubtes';
$string['doubtsemaildesc'] = 'L\'adreça de correu electrònic utilitzada per a enviar els dubtes plantejats pel professorat';
$string['doubt_asked'] = 'Consulta enviada';
$string['doubt_email_body'] = '<p><b>Avís del sistema de gestió de drets de propietat intel·lectual (GDPI) de l\'Aula Virtual:</b></p> <p>L\'usuari/ària {$a->username} ha realitzat una consulta sobre el fitxer {$a->filename}:</p> <ul> <li>Nom del fitxer: {$a->filename}</li> <li>Nom complet del curs: {$a->coursename}</li> <li>Accés al fitxer: {$a->reviewweburl}</li> </ul> <p>El text de la consulta realitzada és:</p>';
$string['doubt_email_subject'] = '[GDPI@Aula Virtual] Consulta de l\'usuari/ària {$a->username} sobre el fitxer {$a->filename}';
$string['doubt_enqueued_error'] = 'No s\'ha pogut enviar el missatge. Torneu a intentar-ho més tard i, si el problema persisteix, comuniqueu-ho a l\'administració de l\'Aula Virtual.';
$string['doubt_enqueued_successfully'] = 'El missatge s\'enviarà aviat. En rebreu una còpia per correu electrònic.';
$string['doubt_help'] = 'Envieu un correu electrònic per a sol·licitar assessorament';
$string['doubt_modal_cancel'] = 'Cancel·la';
$string['doubt_modal_desc'] = 'La vostra consulta s\'enviarà por correu a un especialista en gestió de drets de propietat intel·lectual. Obtindreu la resposta en la bústia {$a->useremail}.';
$string['doubt_modal_send'] = 'Envia consulta';
$string['doubt_modal_title'] = 'Consulta a especialista en propietat intel·lectual';
$string['downloadurl'] = 'URL de baixada';
$string['error_cannot_send_empty_doubt'] = 'Heu d\'escriure algun text en el camp Consulta perquè aquesta es pugui enviar.';
$string['error_cannot_send_empty_doubt_title'] = 'Atenció';
$string['error_doubt'] = 'S\'ha produït un error i no ha estat possible enviar la consulta. Detalls de l\'error:';
$string['error_file_not_found'] = 'No es pot trobar el fitxer amb id {$a}';
$string['error_invalidstate'] = 'No es pot baixar un fitxer en estat {$a}';
$string['error_ivalid_authorization_exception'] = 'Codi d\'autorització invàlid: {$a}';
$string['error_metadatanotfound'] = 'No es poden obtenir les metadades amb id {$a}';
$string['error_notsupportedstate'] = 'No es pot aplicar l\'estat {$a->state} a les metadades amb id {$a->metadataid}';
$string['error_timeendlicense_in_the_past'] = 'No es pot definir la data de fi de llicència en el passat';
$string['error_timeendlicense_needed'] = 'La data de fi de llicència és obligatòria';
$string['error_course_doesnt_exists'] = 'El curs no existeix';
$string['error_max_number_of_results'] = 'El nombre màxim de resultats solicitats ha excedit el límit màxim ({$a})';
$string['error_courseid_salt_already_exists'] = 'El salt del courseid ja es va establir i no pot ser actualitzar';
$string['external_review_message'] = '<p>El fitxer següent ha estat revisat externament amb aquestes dades:</p><ul> <li>Nom del fitxer: {$a->filename}</li> <li>Curs: {$a->coursefullname}</li> <li>Estat: {$a->reviewresolution}</li> <li>Data de fi de llicència (si escau): {$a->timeendlicense}</li></ul>';
$string['external_review_message_deny_reason'] = '<p>Motiu de denegació:</p><div class="denyreason">{$a}</div>';
$string['external_review_subject'] = 'S\'ha resolt l\'estat d\'un fitxer';
$string['filemetadata'] = 'Metadades de fitxer';
$string['filename'] = 'Fitxer';
$string['filepreview'] = 'Previsualització del fitxer';
$string['filestoreview'] = 'Fitxers per revisar';
$string['filteroptions'] = 'Opcions de filtrat';
$string['filter_authz'] = 'Autorització';
$string['filter_visibility'] = 'Visibilitat';
$string['filter_visibility_all'] = 'Mostra tots';
$string['filter_visibility_hidden'] = 'Ocults';
$string['filter_visibility_visibles'] = 'Visibles';
$string['forbidden'] = 'Prohibit';
$string['gt10'] = 'Necessite tramitar una llicència';
$string['gt10perm'] = 'Disposo de llicència en vigor';
$string['guide_url'] = 'URL guia d\'ajuda';
$string['guide_url_desc'] = 'URL que apunta a la guia d\'ajuda';
$string['hidden'] = 'Ocult';
$string['hide_expired_modules'] = 'Oculta mòduls expirats';
$string['identification'] = 'Identificador de l\'obra';
$string['identificationhelp'] = 'Identificador de fitxer únic (ISBN, ISSN, etc.)';
$string['identification_help'] = '<p>Identificador de l\'obra. Pot ser:</p> <ul> <li>ISBN per a llibres</li> <li>ISSN per a publicacions periòdiques</li> <li>Etc.</li> </ul>';
$string['identification_placeholder'] = 'OBLIGATORIO: ISBN, ISSN, DOI, URL,... (formato libre)';
$string['insert_orphaned_files_task'] = 'Inserta fitxers orfes';
$string['isbn'] = 'ISBN';
$string['issn'] = 'ISSN';
$string['license_expired_message'] = '<p><b>Avís del sistema de gestió de drets de propietat intel·lectual (GDPI) de l\'Aula Virtual:</b></p> <p>El mòdul {$a->modulename} ha deixat d\'estar accessible per a l\'estudiantat perquè la llicència de drets de propietat intel·lectual d\'un dels fitxers ha expirat.</p> <p>Podeu tramitar la renovació de la llicència en el formulari associat <a href="{$a->reviewfileurl}">fent clic aquí</a>.</p>';
$string['license_expired_subject'] = '[GDPI@Aula Virtual] Ha expirat la llicència d\'un fitxer del mòdul {$a->modulename}';
$string['license_expired_task'] = 'Comprova llicències expirades';
$string['license_placeholder'] = 'Text descriptiu de la llicència de l\'obra, p. ex. Creative Commons';
$string['loe10'] = 'No en necessita per extensió';
$string['lpi:addfilemetadata'] = 'Gestiona les metadades dels fitxers';
$string['lpi:countasstudent'] = 'L\'usuari es considera estudiant del curs';
$string['lpi:notifymodulehidden'] = 'Notifica que s\'ha ocultat un mòdul';
$string['lpi:notifydoubtsentconfirmation'] = 'Notifica que una consulta ha sigut enviada amb èxit';
$string['lpi:fileexternallyreviewed'] = 'Notifica que un fitxer ha sigut revisat externament';
$string['lpi:notifyexpiredlicenses'] = 'Notifica que una llicència ha caducat per a un fitxer';
$string['lpi:audit'] = 'Auditar les metadades introduïdes pel professorat';
$string['lpisettings'] = 'Paràmetres LPI';

$string['messageprovider:fileexternallyreviewed'] = 'Notificació de fitxer revisat externament';
$string['messageprovider:doubt_sent_confirmation'] = 'Consulta enviada';
$string['messageprovider:hidden_module_confirmation'] = 'Notificació d\'activitats i recursos ocultats automàticament';
$string['messageprovider:license_expired_confirmation'] = 'Notificació de Llicència expirada';

$string['message_modulehidden_message'] = '<p><b>Avís del sistema de gestió de drets de propietat intel·lectual (GDPI) de l\'Aula Virtual:</b></p> <p>El mòdul {$a->modulename} del curs {$a->courseshortname} s\'ha ocultat automàticament por no haver-se tramitat la gestió de drets de propietat intel·lectual en el termini establert.</p> <p>Per favor, tramiteu el formulari associat <a href="{$a->reviewfileurl}">fent clic aquí</a>.</p>';
$string['message_modulehidden_subject'] = '[GDPI@Aula Virtual] El mòdul {$a->modulename} del curs {$a->courseshortname} s\'ha ocultat a l\'estudiantat';
$string['navigation_category'] = 'Propietat intel·lectual';
$string['navigation_title'] = 'Gestió de drets de propietat intel·lectual';
$string['nenrolments'] = 'Nombre de matriculats';
$string['nenrolments_help'] = 'Estimació del nombre de matriculats (opcional)';
$string['nenrolments_placeholder'] = 'Estimació del nombre de matriculats (opcional)';
$string['new_file_to_review_email_body'] = '<p><b>Avís del sistema de gestió de drets de propietat intel·lectual (GDPI) de l\'Aula Virtual:</b></p> <p>L\'usuari {$a->username} ha iniciat la tramitació del fitxer {$a->filename}.</p> <p>Accediu a l\'aplicació de gestió per a iniciar la revisió del fitxer <a href="{$a->reviewweburl}">fent clic aquí</a>.</p>';
$string['new_file_to_review_email_subject'] = 'GDPI@Aula Virtual] Nova tramitació de fitxer';
$string['next'] = 'Següent';
$string['nofilepreview'] = 'No hi ha previsualització disponible';
$string['notificationtext'] = 'Text per a la notificació';
$string['notificationtextdesc'] = 'El text que llegirà el professorat en la notificació de fitxers pendents de revisar en el curs. Si no s\'especifica s\'utilitzarà la cadena pendingfilesnotification del connector local_lpi.';
$string['ok'] = 'El fitxer és meu o de domini públic';
$string['pages'] = 'Pàgines utilitzades';
$string['pages_help'] = 'Pàgines utilitzades. Podeu especificar-les com vulgueu, però us recomanem un format com aquest: 1-10, 44, 50-55.';
$string['pages_placeholder'] = '1-4, xxx o una altra descripció dels límits';
$string['pendingalerttitle'] = 'Avís sobre gestió de drets de propietat intel·lectual';
$string['pendingchangestext'] = 'Hi ha canvis pendents de desar. Si contesteu <em>Sí</em> es perdran els canvis. Voleu abandonar la pàgina?';
$string['pendingchangestitle'] = 'Canvis pendents de desar';
$string['pendingfilesnotification'] = 'El curs <em>{$a->coursefullname}</em> té fitxers pendents de revisió per a complir amb la Llei de Propietat Intel·lectual. Aquesta revisió és obligatòria. Podeu trobar més informació <a href="{$a->moreinfourl}" target="_blank">fent clic aquí</a>.';
$string['pendingfilestoreview'] = 'Fitxers pendents de revisar';
$string['pending_changes_dlg_body'] = 'Les metadades del fitxer han canviat. Si contineu es perdran els canvis. Voleu continuar?';
$string['pending_changes_dlg_title'] = 'Els canvis pendents es perdran';
$string['pending_review_email'] = 'Correu electrònic de tramitació pendent';
$string['pending_review_email_desc'] = 'Adreça de correu electrònic utilitzada per a enviar un missatge quan cal tramitar una nova llicència per a un fitxer.';
$string['pluginname'] = 'Gestió de drets de propietat intel·lectual';
$string['previous'] = 'Anterior';
$string['processform'] = 'Formulari de tramitació';
$string['property_mine'] = 'Meva o de la universitat';
$string['property_of'] = 'Titularitat de drets';
$string['property_of_help'] = '<p>Indiqueu la titularitat del dret de comunicació pública. Valors possibles:</p><ul><li><em>Meva o de la universitat</em>: en sóc l\'autor, els autores són professorat de la universitat, l\'obra és editada pel Servei de Publicacions de la universitat o hi ha acord específic que autoritza l\'ús sense remuneració equitativa</li><li><em>Domini públic</em></li><li><em>Obra plàstica aïllada</em>: obra plàstica aïllada o fotografia figurativa</li><li><em>Obra impresa o susceptible de ser-ho</em></li></ul>';
$string['property_plastic'] = 'Obra plàstica aïllada';
$string['property_printedprintable'] = 'Obra impresa o susceptible de ser-ho';
$string['property_publicdomain'] = 'Domini públic';
$string['publisher'] = 'Editorial';
$string['publisher_placeholder'] = 'Editorial';
$string['realstate'] = 'Estat actual';
$string['returntoreview'] = 'Torna';
$string['review'] = 'Tramita';
$string['reviewedstate'] = 'Estat de revisió';
$string['reviewpendingfiles'] = 'Fitxers pendents de revisar';
$string['review_page_description'] = 'Aquí apareixen tots els fitxers del curs afegits pel professorat del curs per a la gestió dels drets de propietat intel·lectual. Si teniu dubtes podeu consultar la guia <a href="{$a}" target="_new">fent clic aquí</a>.';
$string['review_page_title'] = 'Gestió de drets de propietat intel·lectual';
$string['review_state_forbidden'] = 'Prohibit';
$string['review_state_ok'] = 'D\'acord';
$string['review_state_pending'] = 'Pendent';
$string['review_web_url'] = 'URL de l\'eina de revisió';
$string['review_web_url_desc'] = 'URL de l\'eina externa utilitzada per a tramitar fitxers';
$string['send_doubts_email_task'] = 'Envia missatges de dubte';
$string['state'] = 'Estat';
$string['status'] = 'Estat del fitxer';
$string['statushelp'] = 'L\'estat del fitxer expressat com un percentatge o un estat final';
$string['timeendlicense'] = 'Data de fi de llicència';
$string['title'] = 'Títol';
$string['title_placeholder'] = 'Títol';
$string['totalpages'] = 'Pàgines totals de l\'obra';
$string['totalpages_help'] = 'El nombre de pàgines de l\'obra';
$string['totalpages_placeholder'] = '300 o una altra descripció de l\'extensió';
$string['typeofcontent'] = 'Tipus de contingut';
$string['typeplastic'] = 'Obra plàstica aïllada';
$string['typetext'] = 'Text';
$string['unknown'] = 'Per revisar';
$string['updaterightscentre'] = 'Actualitza l\'entitat de gestió de drets per a cada fitxer';
$string['updatesuccess'] = 'S\'ha actualitzat el fitxer {$a}';
$string['your_doubt_sent_message'] = '<p><b>Avís del sistema de gestió de drets de propietat intel·lectual (GDPI) de l\'Aula Virtual:</b></p> <p>Heu realitzat una consulta de gestió de drets de propietat intel·lectual sobre el fitxer {$a->filename} del mòdul {$a->modulename} amb el text següent:</p> {$a->doubttext} <p>Rebreu la resposta per correu electrònic.</p>';
$string['your_doubt_sent_subject'] = '[GDPI@Aula Virtual] Consulta sobre el fitxer {$a->filename}';

$string['review_table'] = 'Taula de tramitació';
$string['review_table_filter_options'] = 'Opcions de filtrat';

$string['manage'] = 'Gestiona';
$string['send'] = 'Envia';

$string['datecreated'] = 'Data de creació';
$string['datecreated_help'] = 'Data de creació del fitxer.';

$string['visible'] = 'Visible';
$string['perpetuallicense'] = 'Perpètua';
$string['doubt_form'] = 'No sé classificar el document. Vull consultar a un especialista';
$string['massive_actions'] = 'Accions en massa';
$string['review_as'] = 'Tramita fitxers sel·leccionats com';

$string['action_notprintable'] = 'No és una obra textual protegida ni conté fragments d’obres textuals protegides';
$string['action_administrative'] = 'Conté informació administrativa o disposicions legals';
$string['action_teachers'] = 'Obra titularitat del professorat d\'aquest curs sense cessió de drets d\'explotació a tercers';
$string['action_university'] = 'Obra editada pel servei de publicacions d\'aquesta universitat';
$string['action_public_domain'] = 'Obra de domini públic';

$string['massive_confirm_dlg_title'] = 'Confirma acció massiva';
$string['massive_confirm_dlg_body'] = '<p>Aneu a tramitar {$a->numfiles} fitxers com {$a->documentcontent}</p><p>Esteu segurs?</p>';

$string['perpage'] = 'Fitxers per pàgina';

$string['massive_actions_one_needed'] = 'Heu de sel·leccionar al com a mínim un fitxer per a poder tramitar';
$string['filename_help'] = '<p>Introduïu el nom del fitxer. Podeu utilitzar els caràcters especials * i ?. Exemples:</p>'
        . '<ul>'
        . '<li>Tots els fitxers amb extensió .zip: *.zip</li>'
        . '<li>Fitxers que contenten la cadena juan en el seu nom: *juan*</li>'
        . '<li>Fitxers que comencen per juan seguits de qualsevol caràcter adicional: juan?'
        . '</ul>';

$string['uploaddate'] = 'Data de pujada';


$string['document_content_administrative'] = 'Conté informació administrativa o disposicions legals';
$string['document_content_editedbyuniversity'] = 'És una obra editada pel servei de publicacions d\'aquesta Universitat';
$string['document_content_havelicense'] = 'És una obra per a la qual dispose d’autorització escrita o llicència en vigor';
$string['document_content_lt10fragment'] = 'És un fragment o conté fragments d’una obra (imatges soltes, un capítol, un article) per a la qual no tinc autorització o llicència';
$string['document_content_other'] = 'És una obra completa (o fragment superior al 10%) per a la qual no tinc autorització o llicència (tramitació de llicència amb cost econòmic)';
$string['document_content_ownedbyteachers'] = 'És una obra els autors de la qual són professorat d’aquest curs';
$string['document_content_publicdomain'] = 'És una obra de domini públic';
$string['document_content_notprintable'] = 'No és una obra textual protegida ni conté fragments d’obres textuals protegides';
$string['document_content_notprintable_help'] = 'El fitxer és o conté exclusivament, per exemple, una fotografia, una base de dades, un programa d’ordinador, etc. Són obres textuals protegides (i per tant classificables en les altres opcions del formulari) els llibres, revistes, periòdics i partitures musicals.';
$string['document_content_administrative_help'] = 'El fitxer conté normes pròpies de l’assignatura, llistats de notes, etc. O conté exclusivament el text o part del text de disposicions legals o reglamentàries, o dels seus corresponents projectes, o de resolucions d’òrgans jurisdiccionals (article 13 de l’LPI).';
$string['document_content_ownedbyteachers_help'] = 'El fitxer és una obra (apunts, presentacions, butlletins de pràctiques, etc.) els autors de la qual són professorat d’aquest curs i no n’han cedit els drets d’explotació a tercers, p. ex. a una editorial.';
$string['document_content_editedbyuniversity_help'] = 'El fitxer és una obra editada pel servei de publicacions d\'aquesta Universitat.';
$string['document_content_publicdomain_help'] = 'En la legislació espanyola, una obra passa al domini públic transcorreguts setanta anys després de la mort de l’últim autor viu (vuitanta anys si va morir abans del 7 de desembre de 1987).';
$string['document_content_lt10fragment_help'] = 'El fitxer és un fragment d’una obra o conté fragments d’una obra (imatges soltes, un capítol d’un llibre, un article d’una revista o extensió no superior al 10%) per a la qual no tinc autorització escrita o llicència en vigor.';
$string['document_content_havelicense_help'] = 'El fitxer és una obra per a la qual dispose d’una autorització escrita  (p. ex. un correu electrònic de l’autor o dels titulars dels drets) o d’una llicència en vigor (p. ex. una llicència Creative Commons o una llicència comercial).';
$string['document_content_other_help'] = 'El fitxer és una obra completa (o un fragment superior al 10%) per a la qual no tinc autorització escrita o llicència en vigor. Sol·licite tramitar la llicència, amb el cost econòmic corresponent per al qual dispose de crèdit pressupostari.';

$string['lpi:downloadfile'] = 'Decarregar fitxer LPI';
$string['downloadtoopen'] = 'Haureu de descarregar el fitxer per obrir-lo';

$string['cannotchangegt10locally'] = 'No podeu canviar l\'estat de tramitació del fitxer mentres no es resolga la tramitació de la llicència';

$string['header_hide_activity'] = 'Ocultació d\'activitats';
$string['header_hide_activity_desc'] = 'Configureu els paràmetres que afecten a la tasca d\'ocultació d\'activitats';
$string['header_others'] = 'Altres';
$string['header_others_desc'] = 'Altres paràmetres de configuració';
$string['enableactivityhiding'] = 'Activa la ocultació d\'activitats';
$string['enableactivityhiding_desc'] = 'Activa el procés d\'ocultació d\'activitats. Tingueu present que pera a que la ocultació funcione, la tasca \local_lpi\task\hide_expired_modules te que estar activada.';


