<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Spanish language pack.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

$string['action'] = 'Acción';
$string['author'] = 'Autoría';
$string['author_placeholder'] = 'Autor, autora, autores y/o autoras de la obra';
$string['cachedef_pendingfilesnot'] = 'Utilizada para recordar si un usuario ha sido notificado ya de que tiene ficheros pendientes de revisión a nivel de curso';
$string['cannot_doubt_in_doubt'] = 'No puede realizar una nueva consulta hasta que no se resuelva la anterior.';
$string['cannot_edit_state_doubt'] = 'No puede editar los metadatos de un fichero mientras esté en estado de duda. Espere hasta que las dudas planteadas sean resueltas.';
$string['cannot_review_in_doubt'] = 'No puede editar los metadatos hasta que se resuelva su estado de revisión.';
$string['comments'] = 'Observaciones';
$string['comments_placeholder'] = 'Puede escribir algunos comentarios';
$string['comments_placeholder_other'] = 'Indique si dispone de estructura presupuestaria para tramitar la licencia';
$string['deadlineduration'] = 'Plazo de ocultación';
$string['deadlinedurationdesc'] = 'Período de tiempo que el usuario tiene para rellenar los metadatos del fichero antes de que su actividad o recurso asociado se oculten automáticamente.';
$string['doubt'] = 'Consultar';
$string['doubt_asked'] = 'Consulta enviada';


$string['doubt_email_subject'] = '[GDPI@Aula Virtual] Consulta del usuario {$a->username} sobre el fichero {$a->filename} realizada';
$string['doubt_email_body'] = '<p><b>Aviso del sistema de gestión de derechos de propiedad intelectual (GDPI) del Aula Virtual:</b></p>
<p>El usuario {$a->username} ha realizado una consulta sobre el fichero {$a->filename}:</p>
<ul>
    <li>Filename: {$a->filename}</li>
    <li>Nom complet del curs: {$a->coursename}</li>
    <li>Acceso al fichero: {$a->reviewweburl}</li>
</ul>

<p>El texto de la consulta realizada es:</p>';


$string['doubt_enqueued_error'] = 'El mensaje no ha sido encolado para envío. Inténtelo de nuevo más tarde y, si el problema persiste, comuníqueselo a su administrador Moodle.';
$string['doubt_enqueued_successfully'] = 'El mensaje ha sido encolado correctamente. Recibirá un email con una copia del mensaje en breve.';
$string['doubt_help'] = 'Enviar un correo electrónico para solicitar asesoramiento';
$string['doubt_modal_cancel'] = 'Cancelar';
$string['doubt_modal_desc'] = 'Su consulta será enviada por correo a un especialista en gestión de derechos de propiedad intelectual. Obtendrá la respuesta en el buzón de {$a->useremail}.';
$string['doubt_modal_send'] = 'Enviar consulta';
$string['doubt_modal_title'] = 'Consulta a especialista en propiedad intelectual';
$string['doubtsemail'] = 'Correo electrónico para las dudas';
$string['doubtsemaildesc'] = 'La dirección de correo electrónico utilizada para enviar las dudas planteadas por el profesorado';
$string['downloadurl'] = 'URL de descarga';
$string['publisher'] = 'Editorial';
$string['error_file_not_found'] = 'El fichero con id {$a} no se ha encontrado';
$string['error_invalidstate'] = 'No se puede descarga un fichero en estado {$a}';
$string['error_ivalid_authorization_exception'] = 'Código de autorización inválido: {$a}';
$string['error_metadatanotfound'] = 'No se pudo obtener los metadatos con id {$a}';
$string['error_notsupportedstate'] = 'No se puede establecer el estado {$a->state} a los metadatos con id {$a->metadataid}';
$string['error_timeendlicense_in_the_past'] = 'No se puede establecer la Fecha de fin de licencia en el pasado';
$string['error_timeendlicense_needed'] = 'Fecha de fin de licencia es obligatorio';
$string['error_course_doesnt_exists'] = 'El curso no existe';
$string['error_max_number_of_results'] = 'El número máximo de resultados solicitados ha excedido el límite máximo ({$a})';
$string['error_courseid_salt_already_exists'] = 'El salt del courseid ya fue establecido y no puede ser actualizado';
$string['external_review_subject'] = 'El estado de un fichero ha sido resuelto';
$string['external_review_message'] = '<p>El siguiente fichero ha sido revisado externamente con los siguientes datos:</p>'
        . '<ul>'
        . '    <li>Nombre de fichero: {$a->filename}</li>'
        . '    <li>Curso: {$a->coursefullname}</li>'
        . '    <li>Estado:: {$a->reviewresolution}</li>'
        . '    <li>Fecha fin de licencia (si aplica): {$a->timeendlicense}</li>'
        . '</ul>';
$string['external_review_message_deny_reason'] = '<p>Motivo de denegación:</p><div class="denyreason">{$a}</div>';
$string['filemetadata'] = 'Metadatos de fichero';
$string['filename'] = 'Fichero';
$string['filepreview'] = 'Previsualización del fichero';
$string['filestoreview'] = 'Ficheros a revisar';
$string['forbidden'] = 'Prohibido';
$string['gt10'] = 'Necesito tramitar una licencia';
$string['gt10perm'] = 'Dispongo de licencia en vigor';
$string['hidden'] = 'Oculto';
$string['hide_expired_modules'] = 'Oculta módulos expirados';
$string['identification'] = 'Identificador de la obra';
$string['identification_help'] = '<p>Identificador de la obra. Puede ser:</p>
<ul>
<li>El ISBN para textos</li>
<li>El ISSN para publicaciones periódicas</li>
<li>etc.</li>
</ul>';
$string['identification_placeholder'] = 'OBLIGATORIO: ISBN, ISSN, DOI, URL,... (formato libre)';
$string['identificationhelp'] = 'Identificador de fichero único (ISBN, ISSN, etc.)';
$string['isbn'] = 'ISBN';
$string['issn'] = 'ISSN';
$string['license_expired_task'] = 'Comprueba licencias expiradas';
$string['loe10'] = 'No necesita por extensión';
$string['lpi:addfilemetadata'] = 'Gestionar los metadatos de los ficheros';
$string['lpi:countasstudent'] = 'El usuario se considera un estudiante del curso';
$string['lpi:notifymodulehidden'] = 'Notifica que un módulo ha sido ocultado';
$string['lpi:notifydoubtsentconfirmation'] = 'Confirmación de que una duda ha sido enviada';
$string['lpi:fileexternallyreviewed'] = 'Un fichero ha sido revisado externamente';
$string['lpi:notifyexpiredlicenses'] = 'Aviso de licencia expirada para un fichero';
$string['lpi:audit'] = 'Auditar los metadatos introducidos por el profesorado';
$string['lpisettings'] = 'Ajustes LPI';
$string['message_modulehidden_message'] = '<p><b>Aviso del sistema de gestión de derechos de propiedad intelectual (GDPI) del Aula Virtual:</b></p>

<p>El módulo {$a->modulename} del curso {$a->courseshortname} ha sido ocultado automáticamente por no haber tramitado la gestión de derechos de propiedad intelectual en el plazo establecido.</p>

<p>Por favor, tramite el formulario asociado <a href="{$a->reviewfileurl}">pinchando aquí</a>.</p>';

$string['message_modulehidden_subject'] = '[GDPI@Aula Virtual] El módulo {$a->modulename} del curso {$a->courseshortname} ha pasado a no estar accesible para los estudiantes';



$string['messageprovider:modulehidden'] = 'Notificación de actividades y recursos ocultados automáticamente';
$string['navigation_category'] = 'Propiedad Intelectual';
$string['navigation_title'] = 'Gestión de derechos de propiedad intelectual';


$string['new_file_to_review_email_body'] = '<p><b>Aviso del sistema de gestión de derechos de propiedad intelectual (GDPI) del Aula Virtual:</b></p>

<p>El usuario {$a->username} ha iniciado la tramitación del fichero {$a->filename}.</p>

<p>Acceda a la aplicación de gestión para iniciar la revisión del fichero <a href="{$a->reviewweburl}">pinchando aquí</a>.</p>';

$string['new_file_to_review_email_subject'] = '[GDPI@Aula Virtual] Nueva tramitación de fichero realizada';




$string['nofilepreview'] = 'No hay previsualización disponible';
$string['notificationtext'] = 'Texto a notificar';
$string['notificationtextdesc'] = 'El texto que leerá el profesorado en la notificación de ficheros pendientes de revisar a nivel de curso. Is no se especifica se usará la cadena pendingfilesnotification del plugin local_lpi.';
$string['ok'] = 'El fichero es mío o de dominio público';
$string['pages'] = 'Páginas utilizadas';
$string['pages_help'] = 'Páginas utilizadas. Puede usar el formato que considere, aunque le recomendamos algo como: 1-10, 44, 50-55';
$string['pages_placeholder'] = '1-4,xxx u otra descripción del rango';
$string['pending_changes_dlg_body'] = 'Los metadatos del fichero han cambiado. Si continúa, los cambios se perderán. ¿Quiere continuar?';
$string['pending_changes_dlg_title'] = 'Los cambios pendientes se perderán';
$string['pending_review_email'] = 'Correo electrónico de tramitación pendiente.';
$string['pending_review_email_desc'] = 'Una dirección de correo usada para enviar un email cuando es necesario tramitar una nueva licencia para un fichero.';
$string['pendingalerttitle'] = 'Aviso sobre gestión de derechos de propiedad intelectual';
$string['pendingchangestext'] = 'Hay cambios pendientes de guardar. Si pulsa <em>Sí</em> los cambios se perderán. ¿Desea abandonar la página?';
$string['pendingchangestitle'] = 'Cambios pendientes de guardar';
$string['pendingfilesnotification'] = 'El curso <em>{$a->coursefullname}</em> tiene ficheros pendientes de revisión para cumplir con la Ley de Propiedad Intelectual. Esta revisión es obligatoria. Puede encontrar más información <a href="{$a->moreinfourl}" target="_blank">pinchando aquí</a>. ';
$string['pendingfilestoreview'] = 'Ficheros pendientes de revisar';
$string['pluginname'] = 'Gestión de derechos de propiedad intelectual';
$string['property_of_help'] = '<p>Indica la titularidad del derecho de comunicación pública. Valores posibles:</p>'
        . '<ul>'
        . '<li><em>Mía o de la universidad</em>: soy el autor, los autores son profesores de la universidad, la obra es editada por el Servicio de Publicaciones de la universidad o hay acuerdo específico que autoriza el uso sin remuneración equitativa</li>'
        . '<li><em>Dominio público</em></li>'
        . '<li><em>Obra plástica aislada</em>: obra plástica aislada o fotografía figurativa</li>'
        . '<li><em>Obra impresa o susceptible de serlo</em></li>'
        . '</ul>';
$string['property_mine'] = 'Mía o de la universidad';
$string['property_mine_help'] = '';
$string['property_of'] = 'Titularidad de derechos';
$string['document_content'] = 'Este fichero';

$string['property_printedprintable'] = 'Obra impresa o susceptible de serlo';
$string['property_publicdomain'] = 'Dominio público';
$string['property_plastic'] = 'Obra plástica aislada';
$string['publisher_placeholder'] = 'Editorial';
$string['returntoreview'] = 'Volver';
$string['review'] = 'Tramitar';
$string['review_state_forbidden'] = 'Prohibido';
$string['review_state_ok'] = 'Ok';
$string['review_state_pending'] = 'Pendiente';
$string['review_web_url'] = 'URL de la herramienta de revisión';
$string['review_web_url_desc'] = 'La URL de la herramienta externa usada para tramitar ficheros';
$string['reviewedstate'] = 'Estado de revisión';
$string['reviewpendingfiles'] = 'Ficheros pendiente de revisar';
$string['send_doubts_email_task'] = 'Envía mensajes de duda';
$string['state'] = 'Estado';
$string['status'] = 'Estado del fichero';
$string['statushelp'] = 'El estado del fichero dado como un porcentaje o un estado final';
$string['timeendlicense'] = 'Fecha de fin de licencia';
$string['title'] = 'Título';
$string['title_placeholder'] = 'Título';
$string['totalpages'] = 'Páginas totales de la obra';
$string['totalpages_help'] = 'El número de páginas de la obra';
$string['totalpages_placeholder'] = '300 u otra descripción de la extensión';
$string['typeplastic'] = 'Obra plástica aislada';
$string['typeofcontent'] = 'Tipo de contenido';
$string['typetext'] = 'Texto';
$string['unknown'] = 'A revisar';
$string['updaterightscentre'] = 'Actualiza la entidad de gestión de derechos para cada fichero';
$string['updatesuccess'] = 'Fichero {$a} actualizado correctamente';

$string['your_doubt_sent_subject'] = '[GDPI@Aula Virtual] Consulta sobre el fichero {$a->filename} realizada';
$string['your_doubt_sent_message'] = '<p><b>Aviso del sistema de gestión de derechos de propiedad intelectual (GDPI) del Aula Virtual:</b></p>

<p>Usted ha realizado una consulta de gestión de derechos de propiedad intelectual sobre el fichero {$a->filename} del módulo {$a->modulename} con el siguiente texto:</p>

{$a->doubttext}

<p>En breve recibirá respuesta por correo electrónico.</p>';

$string['review_page_title'] = 'Gestión de derechos de propiedad intelectual';
$string['review_page_description'] = 'Aquí se relacionan todos los ficheros subidos por el profesorado al '
        . 'curso para la gestión de los derechos de propiedad intelectual. '
        . 'Si tiene dudas consulte la guía <a href="{$a}" target="_new">pinchando aquí</a>.';
$string['guide_url'] = 'URL guía de ayuda';
$string['guide_url_desc'] = 'URL que apunta a la guía de ayuda.';

$string['authorization'] = 'Autorización';
$string['authorization_notprocessedyet'] = 'Sin tramitar';

$string['authorization_notprocessedyet_desc'] = 'Todavía no ha realizado ninguna acción sobre el fichero.';
$string['authorization_notprocessedyet_desc_help'] = 'Todavía no ha realizado ninguna acción sobre el fichero.';

$string['authorization_notnecessary'] = 'Autorizado';

$string['authorization_notnecessary_lt10_desc'] = 'Por extensión de la obra (capítulo, artículo o extensión de hasta el 10%) no precisa autorización. Remuneración equitativa vía convenio CEDRO-VEGAP.';
$string['authorization_notnecessary_lt10_desc_help'] = 'Por extensión de la obra (capítulo, artículo o extensión de hasta el 10%) no precisa autorización. Remuneración equitativa vía convenio CEDRO-VEGAP.';

$string['authorization_notnecessary_pd'] = 'La obra está en el dominio público.';
$string['authorization_notnecessary_pd_help'] = 'La obra está en el dominio público.';

$string['authorization_notnecessary_haveauthz'] = 'Dispongo de autorización de los autores o la universidad es propietaria de los derechos.';
$string['authorization_notnecessary_notprintable'] = 'No es una obra imprimible o susceptible de serlo';
$string['authorization_license'] = 'Autorizado';

$string['authorization_license_desc'] = 'Se ha adquirido una licencia para la explotación.';
$string['authorization_license_desc_help'] = 'Se ha adquirido una licencia para la explotación.';

$string['authorization_notauthz'] = 'No autorizado';

$string['authorization_notauthz_desc'] = 'El resultado de la tramitación es negativo y el fichero permanecerá oculto. Puede iniciar una nueva tramitación. Motivo: {$a}';
$string['authorization_notauthz_desc_help'] = '<p>El resultado de la tramitación es negativo y el fichero permanecerá oculto. Puede iniciar una nueva tramitación.</p><p>---</p><p>Motivo: {$a}</p>';

$string['authorization_waiting'] = 'En tramitación';

$string['authorization_waiting_desc'] = 'Usted ha iniciado una tramitación que está siendo procesada por personal especializado en derechos de propiedad intelectual.';
$string['authorization_waiting_desc_help'] = 'Usted ha iniciado una tramitación que está siendo procesada por personal especializado en derechos de propiedad intelectual.';

$string['authorization_notprintable'] = 'No es una obra textual protegida ni contiene fragmentos de obras textuales protegidas';

$string['authorization_notnecessary_teachers'] = 'Obra cuya autoría es de profesorado de la universidad y no han cedido los derechos de explotación a un tercero';
$string['authorization_notnecessary_teachers_help'] = 'Obra cuya autoría es de profesorado de la universidad y no han cedido los derechos de explotación a un tercero';

$string['authorization_notnecessary_university'] = 'Obra editada por el servicio de publicaciones de esta universidad';
$string['authorization_notnecessary_university_help'] = 'Obra editada por el servicio de publicaciones de esta universidad';

$string['authorization_notnecessary_administrative'] = 'Contiene información administrativa o disposiciones legales';
$string['authorization_notnecessary_administrative_help'] = 'El fichero contiene normas propias de la asignatura, listados de notas, etc. O contiene exclusivamente el texto o parte del texto de disposiciones legales o reglamentarias o de sus correspondientes proyectos o de resoluciones de órganos jurisdiccionales (artículo 13 de la LPI)';

$string['authorization_authorized'] = 'Autorizado';

$string['realstate'] = 'Estado actual';
$string['previous'] = 'Anterior';
$string['next'] = 'Siguiente';
$string['processform'] = 'Formulario de tramitación';
$string['authorization_help'] = '<ul>'
        . '<li><em>No necesita por extensión</em>: capítulo de libro, artículo de revista o extensión de hasta un 10% de la obra</li>'
        . '<li><em>Dispongo de licencia en vigor</em></li>'
        . '<li><em>Necesito tramitar una licencia</em></li>'
        . '</ul>';
$string['nenrolments'] = 'Número de matriculados';
$string['nenrolments_help'] = 'Dado que el nº actual de participantes puede variar, puede usted sustituir este valor por una estimación del total de estudiantes del curso.';
$string['nenrolments_placeholder'] = 'Estimación del número de matriculados (opcional)';
$string['nenrolments_help'] = 'Estimación del número de matriculados (opcional)';

$string['license_expired_subject'] = '[GDPI@Aula Virtual] Ha expirado la licencia de un fichero del módulo {$a->modulename}';
$string['license_expired_message'] = '<p><b>Aviso del sistema de gestión de derechos de propiedad intelectual (GDPI) del Aula Virtual:</b></p>

<p>El módulo {$a->modulename} ha dejado de estar accesible para los estudiantes porque la licencia de derechos de propiedad intelectual de uno de los ficheros ha expirado.</p>

<p>Puede tramitar la renovación de la licencia en el formulario asociado <a href="{$a->reviewfileurl}">pinchando aquí</a>.</p>';

$string['filteroptions'] = 'Opciones de filtrado';
$string['applyfilter'] = 'Aplicar filtros';

$string['filter_visibility'] = 'Visibilidad';
$string['filter_visibility_all'] = 'Mostrar todos';
$string['filter_visibility_hidden'] = 'Ocultos';
$string['filter_visibility_visibles'] = 'Visibles';

$string['filter_authz'] = 'Autorización';

$string['detailedreviewpage'] = 'Página de tramitación';
$string['error_doubt'] = 'Se produjo un error y no ha sido posible enviar la consulta. Detalle del error:';

$string['beginreview'] = 'Iniciar revisión';

$string['document_content_administrative'] = 'Disposiciones legales e información administrativa (leyes, resoluciones, normativa interna de la universidad, listados de notas...)';
$string['document_content_ownedbyteachers'] = 'Obra cuya autoría es de profesorado de la universidad y no han cedido los derechos de explotación a un tercero';
$string['document_content_editedbyuniversity'] = 'Una obra editada por el servicio de publicaciones de la universidad';
$string['document_content_publicdomain'] = 'Una obra de dominio público';
$string['document_content_lt10fragment'] = 'Un fragmento de una obra (capítulo de libro, artículo o extensión no superior al 10%, una imagen)';
$string['document_content_havelicense'] = 'Una obra de la que dispongo de licencia en vigor';
$string['document_content_other'] = 'No tengo licencia en vigor (tramitación de licencia con coste económico)';
$string['document_content_notprintable'] = 'No es una obra impresa o susceptible de serlo (fotografías, código de ordenador, base de datos, ...)';

$string['license_placeholder'] = 'Texto descriptivo de la licencia de la obra, por ejemplo, Creative Commons';
$string['error_cannot_send_empty_doubt'] = 'Debe escribir texto en el campo Consulta para que esta sea finalmente enviada.';
$string['error_cannot_send_empty_doubt_title'] = 'Atención';

$string['close'] = 'Cerrar';
$string['insert_orphaned_files_task'] = 'Insertar ficheros huérfanos';

$string['messageprovider:fileexternallyreviewed'] = 'Fichero revisado externamente';
$string['messageprovider:doubt_sent_confirmation'] = 'Consulta enviada correctamente';
$string['messageprovider:hidden_module_confirmation'] = 'Módulo ocultado automáticamente';
$string['messageprovider:license_expired_confirmation'] = 'Licencia expirada';

$string['review_table'] = 'Tabla de tramitación';
$string['review_table_filter_options'] = 'Opciones de filtrado';

$string['manage'] = 'Gestionar';
$string['send'] = 'Enviar';

$string['datecreated'] = 'Fecha de creación';
$string['datecreated_help'] = 'Fecha de subida del fichero';

$string['visible'] = 'Visible';
$string['perpetuallicense'] = 'Perpetua';
$string['doubt_form'] = 'No sé clasificar el documento. Quiero consultar a un especialista';

$string['massive_actions'] = 'Acciones masivas';
$string['review_as'] = 'Tramitar ficheros seleccionados como';

$string['action_notprintable'] = 'No es obra textual protegida ni contiene obras textuales protegidas';
$string['action_administrative'] = 'Contiene información administrativa o disposiciones legales';
$string['action_teachers'] = 'Obra titularidad del profesorado de este curso sin cesión de derechos de explotación a terceros';
$string['action_university'] = 'Obra editada por el servicio de publicaciones de esta universidad';
$string['action_public_domain'] = 'Obra de dominio público';

$string['massive_confirm_dlg_title'] = 'Confirme acción masiva';
$string['massive_confirm_dlg_body'] = '<p>Va a tramitar {$a->numfiles} ficheros como {$a->documentcontent}</p><p>¿Está seguro/a?</p>';
$string['perpage'] = 'Ficheros por página';

$string['massive_actions_one_needed'] = 'Debe seleccionar al menos un fichero para poder tramitar';

$string['filename_help'] = '<p>Introduzca el nombre del fichero. Puede usar los caracteres especiales * y ? para indicar cualquier cadena y ? para cualquier carácter. Ejemplos:</p>'
        . '<ul>'
        . '<li>Todos los ficheros con extensión zip: *.zip</li>'
        . '<li>Ficheros cuyo nombre contiene la cadena juan: *juan*</li>'
        . '<li>Ficheros que empiezan por juan seguidos de cualquier carácter adicional: juan?'
        . '</ul>';

$string['uploaddate'] = 'Fecha de subida';

$string['document_content_administrative'] = 'Contiene información administrativa o disposiciones legales';
$string['document_content_editedbyuniversity'] = 'Es una obra editada por el servicio de publicaciones de esta universidad';
$string['document_content_havelicense'] = 'Es una obra de la que dispongo de autorización escrita o licencia en vigor';
$string['document_content_lt10fragment'] = 'Es un fragmento o contiene fragmentos de una obra (imágenes sueltas, un capítulo, un artículo) para la que no tengo autorización o licencia';
$string['document_content_other'] = 'Es una obra completa (o fragmento superior al 10%) para la que no tengo autorización o licencia (tramitación de licencia con coste económico)';
$string['document_content_ownedbyteachers'] = 'Es una obra cuya autoría es de profesorado de este curso';
$string['document_content_publicdomain'] = 'Es una obra de dominio público';
$string['document_content_notprintable'] = 'No es una obra textual protegida ni contiene fragmentos de obras textuales protegidas';

$string['document_content_notprintable_help'] = 'El fichero es o contiene exclusivamente, por ejemplo, una fotografía, una base de datos, un programa de ordenador, etc. Son obras textuales protegidas (y por lo tanto clasificables en las otras opciones del formulario) libros, revistas, periódicos y partituras musicales.';
$string['document_content_administrative_help'] = 'El fichero contiene normas propias de la asignatura, listados de notas, etc. O contiene exclusivamente el texto o parte del texto de disposiciones legales o reglamentarias o de sus correspondientes proyectos o de resoluciones de órganos jurisdiccionales (artículo 13 de la LPI)';
$string['document_content_ownedbyteachers_help'] = 'El fichero es una obra (apuntes, presentaciones, boletines de prácticas, etc.) los autores de la cual son profesorado de este curso y no han cedido los derechos de explotación a terceros, por ejemplo a una editorial.';
$string['document_content_editedbyuniversity_help'] = 'El fichero es una obra editada por el servicio de publicaciones de esta universidad.';
$string['document_content_publicdomain_help'] = 'En la legislación española, una obra pasa al dominio público transcurridos setenta años tras la muerte del último autor vivo (ochenta años si murió antes del 7 de diciembre de 1987).';
$string['document_content_lt10fragment_help'] = 'El fichero es un fragmento de una obra o contiene fragmentos de una obra (imágenes sueltas, un capítulo de un libro, un artículo de una revista o extensión no superior al 10%) para la cual no tengo autorización escrita o licencia en vigor.';
$string['document_content_havelicense_help'] = 'El fichero es una obra para la cual dispongo de una autorización escrita (por ejemplo, un correo electrónico del autor o de los titulares de los derechos) o de una licencia en vigor (por ejemplo, una licencia Creative Commons o una licencia comercial).';
$string['document_content_other_help'] = 'El fichero es una obra completa (o fragmento superior al 10%) para la que no tengo autorización escrita o licencia en vigor. Solicito tramitar la licencia, con el coste económico correspondiente para el que dispongo de crédito presupuestario.';

$string['lpi:downloadfile'] = 'Descargar fichero LPI';
$string['downloadtoopen'] = 'Deberá descargar el fichero para abrirlo';

$string['cannotchangegt10locally'] = 'No puede cambiar el estado de tramitación de un fichero mientras espere tramitar una licencia';

$string['header_hide_activity'] = 'Ocultación de actividades';
$string['header_others'] = 'Otros';
$string['header_hide_activity_desc'] = 'Configure los parámetros de ocultación automàtica de las actividades';
$string['header_others_desc'] = 'Otros parámetros de configuración';
$string['enableactivityhiding'] = 'Activar la ocultación de actividades';
$string['enableactivityhiding_desc'] = 'Activa el proceso de ocultación de actividades. Tenga en cuenta que para que la ocultación tenga efecto, la tarea \local_lpi\task\hide_expired_modules tiene que estar activada.';
