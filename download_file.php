<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An interface to download a file that is in two of these states:
 *
 * \local_lpi\orm\file_metadata::STATE_GT10
 * \local_lpi\orm\file_metadata::STATE_DOUBT
 *
 * We don't use pluginfile.php or webservice/pluginfile.php because there no
 * way to specify that the above conditions are met. Moreover,
 * we don't want to give the webservice user more privileges that the necessary.
 *
 * This script is mainly inspired by webservice/pluginfile.php.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

if (array_key_exists('token', $_REQUEST)) {
    define('AJAX_SCRIPT', true);
    define('NO_MOODLE_COOKIES', true);
} else {
    define('AJAX_SCRIPT', false);
}

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/webservice/lib.php');

use local_lpi\orm\file_metadata;

// The id in the {local_lpi_file_metadata} table.
$metadataid = optional_param('id', 0, PARAM_INT);
// Token based authentication.
$token = optional_param('token', '', PARAM_RAW);
// Contenthash or metadataid are valid.
$contenthash = optional_param('contenthash', '', PARAM_ALPHANUM);
// Courseid mandatory if contenthash.
$courseid = optional_param('courseid', 0, PARAM_INT);

// TODO: Add the possibility of restrincting the allowed IPs.
header('Access-Control-Allow-Origin: *');

// Authentication.
if ($token) {
    $webservicelib = new webservice();
    $authenticationinfo = $webservicelib->authenticate_user($token);

    $enabledfiledownload = (int) ($authenticationinfo['service']->downloadfiles);
    if (empty($enabledfiledownload)) {
        throw new webservice_access_exception('Web service file downloading must be enabled in external service settings');
    }
}
// Authorization.
$lpi = \local_lpi\manager::get_instance();
if ($metadataid) {
    $filemetadata = $lpi->get_file_metadata_by_id($metadataid);
    if (!$filemetadata) {
        send_file_not_found();
    }
} else if ($contenthash) {
    if (!$courseid) {
        throw new moodle_exception('missingparam', 'moodle', '', 'courseid');
    }
    $filemetadata = $lpi->get_file_metadata_by_contenthash($courseid, $contenthash);
    if (!$filemetadata) {
        send_file_not_found();
    }
    require_login(SITEID, false);
} else {
    throw new moodle_exception('missingparam', 'moodle', '', 'metadataid or contenthash');
}

// Do not call require_login. We don't require to be enrolled into the course.
$ctx = \context_course::instance($filemetadata->get_courseid());
require_capability('local/lpi:downloadfile', $ctx);

/*
 * Verify that the file is in state \local_lpi\orm\file_metadata::STATE_GT10
 * or that the last doubt was states 30 days before.
 */
$state = $filemetadata->get_state();
if ($state !== file_metadata::STATE_GT10 && ((time() - $filemetadata->get_lastdoubttime()) > 30 * 24 * 3600)) {
    throw new \local_lpi\exception\invalid_state_exception($filemetadata->get_id(), $state);
}

// Finally, send the file (one of them if multiple area set).
send_stored_file($filemetadata->get_files()[0], null, 0, true);
