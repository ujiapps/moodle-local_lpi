<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Review pending files page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

require_once(dirname(__FILE__) . '/../../config.php');

use local_lpi\orm\file_metadata;
use local_lpi\objects\authorization_state;

$courseid = required_param('courseid', PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);

require_login($courseid);

$coursecontext = context_course::instance($courseid);
require_capability('local/lpi:addfilemetadata', $coursecontext);

$course = $DB->get_record('course', array('id' => $courseid));

$PAGE = new moodle_page();
$PAGE->set_context($coursecontext);
$PAGE->set_course($course);
$PAGE->set_url($CFG->wwwroot . '/local/lpi/review.php', array('courseid' => $courseid));
$PAGE->set_pagelayout('incourse');
$PAGE->set_heading(get_string('review_page_title', 'local_lpi'));
$PAGE->set_cacheable(false);

// If we're getting filter options the store them.
$filterform = new local_lpi\output\review_data_filter_form(
        'filterform',
        $courseid,
        $page
);
$filterform->set_filter_data();

// Apply filters.
if ($formdata = $filterform->get_data()) {

    local_lpi\review_filters::set_visibility_filter($formdata->visibilitygroup['visibility']);

    $values = array(
        \local_lpi\review_filters::FILTER_AUTHZ_NOT_PROCESSED_YET,
        \local_lpi\review_filters::FILTER_AUTHZ_NOT_AUTHORIZED,
        \local_lpi\review_filters::FILTER_AUTHZ_WAITING_FOR_LICENSE,
        \local_lpi\review_filters::FILTER_AUTHZ_AUTHORIZED,
        \local_lpi\review_filters::FILTER_AUTHZ_NOT_PRINTABLE
    );
    foreach ($values as $name) {
        if (isset($formdata->authzgroup) && array_key_exists('authz_' . $name, $formdata->authzgroup)) {
            local_lpi\review_filters::set_authz_filter_values($name, $formdata->authzgroup['authz_' . $name]);
        } else {
            local_lpi\review_filters::set_authz_filter_values($name, 0);
        }
    }

    if (isset($formdata->topicsfilter) && $formdata->topicsfilter !== null) {
        \local_lpi\review_filters::set_topic_filter($formdata->topicsfilter);
    }

    if (isset($formdata->perpage)) {
        local_lpi\review_filters::set_perpage_filter($formdata->perpage);
    }

    if (isset($formdata->filename)) {
        local_lpi\review_filters::set_filename_filter($formdata->filename);
    }

    redirect(new \moodle_url('/local/lpi/review.php', array(
        'courseid' => $courseid,
        'page' => 0
    )));
}

$lpi = local_lpi\manager::get_instance();

// Massive actions form.
$massiveactions = new local_lpi\output\massive_actions_form($courseid);
if ($data = $massiveactions->get_data()) {

    $contenthashes = explode(",", $data->files);
    $courseid = (int) $data->courseid;
    $documentcontent = new local_lpi\objects\document_content($data->actions);

    $lpi->apply_massive_document_content($documentcontent, $courseid, $contenthashes);

    redirect(new \moodle_url('/local/lpi/review.php', array(
        'courseid' => $courseid
    )), get_string('success'));

}

// Empezamos a renderizar.

$output = $PAGE->get_renderer('local_lpi');
echo $output->header();

/* Add file_metadata of those files without file metadata. Deactivate this
   temporary and let the scheduled task to do it.
   $lpi->insert_unknown_files_into_file_metadata($courseid).
*/

// Render the review_page template.
$visibility = local_lpi\review_filters::get_visibility_filter();
switch ($visibility) {
    case \local_lpi\review_filters::FILTER_VISIBILITY_ALL:
        $visibility = null;
        break;
    case \local_lpi\review_filters::FILTER_VISIBILITY_HIDDEN:
        $visibility = 0;
        break;
    case \local_lpi\review_filters::FILTER_VISIBILITY_VISIBLES:
        $visibility = 1;
        break;
}

// Now the filter.

$authzstatefilters = local_lpi\review_filters::get_authz_filter_values();
$authzstates = authorization_state::get_authorization_states_from_authz_filters($authzstatefilters);

if (!isset($USER->local_lpi_return_page)) {
    $USER->local_lpi_return_page = array();
}
$USER->local_lpi_return_page[$courseid] = $page;

$PAGE->requires->js_call_amd('local_lpi/page-review', 'init', array('pending-table', 'filterform'));

$pendingtable = new \local_lpi\output\pending_table(
        "pending-table",
        $courseid,
        local_lpi\review_filters::get_perpage_filter(),
        $authzstates,
        $visibility,
        $output
);

$config = local_lpi\config::get_instance();
$reviewpage = new local_lpi\output\review_page(
        $config->get_guideurl(),
        $pendingtable,
        $filterform,
        $courseid
);

echo $output->render($reviewpage);

echo $output->footer();
