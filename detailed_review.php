<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Detailed review page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

require_once(dirname(__FILE__) . '/../../config.php');

use local_lpi\objects\authorization_state;

$courseid = required_param('courseid', PARAM_INT);
$contenthash = required_param('contenthash', PARAM_ALPHANUM);

require_login($courseid);

$coursecontext = context_course::instance($courseid);
require_capability('local/lpi:addfilemetadata', $coursecontext);

$course = $DB->get_record('course', array('id' => $courseid));

$PAGE = new moodle_page();
$PAGE->set_context($coursecontext);
$PAGE->set_course($course);
$PAGE->set_url($CFG->wwwroot . '/local/lpi/detailed_review.php', array('courseid' => $courseid));
$PAGE->set_pagelayout('embedded');

$PAGE->set_title(get_string('detailedreviewpage', 'local_lpi'));
$PAGE->set_heading(get_string('detailedreviewpage', 'local_lpi'));
$PAGE->set_cacheable(false);

// The page to return when we want to go back to the review_page.
if (isset($USER->local_lpi_return_page) && array_key_exists($courseid, $USER->local_lpi_return_page)) {
    $returnpage = $USER->local_lpi_return_page[$courseid];
} else {
    $returnpage = 0;
}

$manager = local_lpi\manager::get_instance();
$filemetadata = $manager->get_file_metadata_by_contenthash($courseid, $contenthash);

// The state GT10 can only change externally.
if ($filemetadata->get_state() === \local_lpi\orm\file_metadata::STATE_GT10) {
    redirect(
            new \moodle_url('/local/lpi/review.php', array('courseid' => $courseid, 'page' => $returnpage)),
            get_string('cannotchangegt10locally', 'local_lpi'),
            5,
            \core\output\notification::NOTIFY_ERROR
    );
}

$filemetadataform = new \local_lpi\output\file_metadata_form(
        'detailed-review-form',
        $courseid,
        $contenthash,
        $filemetadata->get_files()[0]->get_filename()
);

if ($formdata = $filemetadataform->get_data()) {

    $filemetadataform->update_file_metadata_with_data($filemetadata);

    // We need to setup the userid here.
    $filemetadata->set_userid($USER->id);

    // Reset the rights management entity to run update_rights_entity task.
    $filemetadata->set_rightsentity(null);

    $manager->add_file_metadata($filemetadata);

    // Trigger the file_reviewd event.
    $event = \local_lpi\event\file_reviewed::create_from_file_metadata($filemetadata);
    $event->trigger();

    // Redirect to the right place.
    if (isset($formdata->nextfileid) && $formdata->nextfileid) {

        redirect(new \moodle_url(
            '/local/lpi/detailed_review.php',
            ['courseid' => $courseid, 'contenthash' => $formdata->nextfileid]
        ));

    } else {
        redirect(
            new \moodle_url('/local/lpi/review.php', array('courseid' => $courseid, 'page' => $returnpage)),
            get_string('updatesuccess', 'local_lpi', $filemetadata->get_files()[0]->get_filename()),
            5,
            \core\output\notification::NOTIFY_SUCCESS
        );
    }

} else if (!$filemetadataform->is_submitted()) {
    // Not validated but submitted.
    $filemetadataform->set_file_metadata($filemetadata);
}

// Require PDF.js. This is tricky... we need a amd module to access it.
$PAGE->requires->js(
        new \moodle_url("/local/lpi/js/pdfjs/build/pdf.js"),
        true
);
$PAGE->requires->js_call_amd(
        'local_lpi/page-detailed-review',
        'init',
        array(
            $filemetadata->get_contenthash(),
            (int) $courseid
        )
);
$PAGE->requires->css('/lib/jquery/ui-1.12.1/jquery-ui.min.css');

$output = $PAGE->get_renderer('local_lpi');
echo $output->header();

list($authzstate, $authzstatedesc) = authorization_state::get_authorization_state_string($filemetadata);
$detailedreviewnavigation = new local_lpi\output\detailed_review_navigation(
        $filemetadata,
        new \moodle_url(
                '/local/lpi/review.php',
                array(
                    'courseid' => $courseid,
                    'page' => $returnpage
                )
        ),
        array(),
        0,
        $authzstate,
        $authzstatedesc
);

$detailedreviewpage = new \local_lpi\output\detailed_review_page(
        $detailedreviewnavigation,
        $filemetadataform,
        new \local_lpi\output\file_preview($filemetadata)
);
echo $output->render($detailedreviewpage);

echo $output->footer();
