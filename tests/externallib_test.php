<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Externallib tests.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot . '/webservice/tests/helpers.php');

/**
 * @group local_lpi
 */
class local_lpi_externallib_testcase extends externallib_advanced_testcase {

    const NUM_ACTIVITIES = 10;

    private $course;
    private $teachers;
    private $students;

    private $activities;

    private $manager;

    public function setUp(): void {
        parent::setUp();

        $this->manager = local_lpi\manager::get_instance();

        $this->course = $this->getDataGenerator()->create_course();
        $this->teachers[] = $this->getDataGenerator()->create_user(array(
            'idnumber' => 0
        ));
        $this->teachers[] = $this->getDataGenerator()->create_user(array(
            'idnumber' => 1
        ));
        $this->students[] = $this->getDataGenerator()->create_user();

        foreach ($this->teachers as $teacher) {
            $this->getDataGenerator()->enrol_user($teacher->id, $this->course->id, 'editingteacher');
        }
        foreach ($this->students as $student) {
            $this->getDataGenerator()->enrol_user($student->id, $this->course->id, 'student');
        }
        for ($t = 0; $t < count($this->teachers); $t++) {
            $this->setUser($this->teachers[$t]);
            for ($i = 0; $i < self::NUM_ACTIVITIES; $i++) {
                $this->activities[] = $this->getDataGenerator()->create_module(
                        'resource',
                        array(
                            'course' => $this->course
                        )
                );
            }
        }
    }

    /**
     * @runInSeparateProcess
     */
    public function test_that_users_without_addfilemetadata_capability_cannot_ask_questions() {
        $this->resetAfterTest();
        global $CFG,$SCRIPT;
        require_once($CFG->dirroot . '/local/lpi/externallib.php');

        $this->setUser($this->teachers[0]);

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_UNKNOWN);
        $this->manager->add_file_metadata($filemetadata);

        \local_lpi_external::add_doubt_message($this->course->id, $filemetadata->get_contenthash(), 'test', FORMAT_PLAIN);

        // People without addfilemetadata cannot ask questions. Test it with a student.
        $this->setUser($this->students[0]);
        $this->expectException(\required_capability_exception::class);
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_UNKNOWN);
        $this->manager->add_file_metadata($filemetadata);
        \local_lpi_external::add_doubt_message($this->course->id, $filemetadata->get_contenthash(), 'test', FORMAT_PLAIN);

    }

    /**
     * @runInSeparateProcess
     */
    public function test_users_with_addfilemetadata_can_get_files_pending_to_classify()
    {
        $this->resetAfterTest();
        global $CFG,$SCRIPT;
        require_once($CFG->dirroot . '/local/lpi/externallib.php');

        // Teachers can get pending files.
        $this->setUser($this->teachers[0]);
        $pending = \local_lpi_external::get_pending_fileids($this->course->id);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_users_without_addfilementadata_cannot_get_files_pending_to_classify()
    {
        $this->resetAfterTest();
        global $CFG,$SCRIPT;
        require_once($CFG->dirroot . '/local/lpi/externallib.php');

        $this->expectException(\required_capability_exception::class);

        $this->setUser($this->students[0]);
        $pending = \local_lpi_external::get_pending_fileids($this->course->id);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_that_questions_without_a_message_cannot_be_sent() {
        $this->resetAfterTest();
        global $CFG,$SCRIPT;
        require_once($CFG->dirroot . '/local/lpi/externallib.php');

        $this->expectException(\local_lpi\exception\empty_doubt_not_allowed_exception::class);

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_UNKNOWN);
        $this->manager->add_file_metadata($filemetadata);

        local_lpi_external::add_doubt_message($this->course->id, $filemetadata->get_contenthash(), '        ', FORMAT_PLAIN);
    }
}
