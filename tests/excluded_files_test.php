<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Test excluded files are really excluded.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/commons.php');

use local_lpi\manager;
use local_lpi\ignored_files_manager;

/**
 * @group local_lpi
 */
class excluded_files_test extends advanced_testcase {

    use files_methods;

    const COURSES = 2;
    const TEACHERS = 2;
    const STUDENTS = 2;

    protected $courses;
    protected $teachers;
    protected $students;

    public function setUp(): void {
        parent::setUpBeforeClass();

        $this->courses = [];
        $this->teachers = [];
        $this->students = [];

        for ($i = 0; $i < self::TEACHERS; $i++) {
            $this->teachers[$i] = $this->getDataGenerator()->create_user();
        }
        for ($i = 0; $i < self::STUDENTS; $i++) {
            $this->students[$i] = $this->getDataGenerator()->create_user();
        }
        for ($i = 0; $i < self::COURSES; $i++) {
            $this->courses[$i] = $this->getDataGenerator()->create_course();
            for ($j = 0; $j < self::TEACHERS; $j++) {
                $this->getDataGenerator()->enrol_user($this->teachers[$j]->id, $this->courses[$i]->id, 'editingteacher');
            }
            for ($j = 0; $j < self::STUDENTS; $j++) {
                $this->getDataGenerator()->enrol_user($this->students[$j]->id, $this->courses[$i]->id, 'student');
            }
        }
    }

    public function test_image_audio_and_video_files_are_excluded_are_not_notified_as_pending() {
        $this->resetAfterTest();

        $manager = manager::get_instance();

        $this->setUser($this->teachers[0]);

        $mimetypes = [
            'image/jpeg' => [
                'uno.jpg',
                'dos.jpg',
                'tres.jpg'
            ],
            'video/mp4' => [
                'uno.mp4',
                'dos.mp4',
                'tres.mp4'
            ],
            'audio/mp3' => [
                'uno.mp3',
                'dos.mp3',
                'tres.mp3'
            ]
        ];

        // Test Sections.
        foreach ($mimetypes as $mimetype => $filenames) {
            // One image should be excluded.
            $file = $this->create_file($filenames[0], $mimetype, 'one');
            $this->add_file_to_section($file, $this->courses[0], 0);

            $ret = $manager->should_notify_teachers($this->courses[0]->id);
            $this->assertFalse($ret);

            // An image in another course should be exluded too.
            $file = $this->create_file($filenames[1], $mimetype, 'dos');
            $this->add_file_to_section($file, $this->courses[1], 0);

            $ret = $manager->should_notify_teachers($this->courses[0]->id);
            $this->assertFalse($ret);
            $ret = $manager->should_notify_teachers($this->courses[1]->id);
            $this->assertFalse($ret);

            // Test modules.

            // One image.
            $file = $this->create_file($filenames[2], $mimetype, 'a gif');
            $this->add_file_to_resource($file, $this->courses[0]);

            $ret = $manager->should_notify_teachers($this->courses[0]->id);
            $this->assertFalse($ret);
        }

        $this->setUser();
    }

    public function test_files_that_are_marked_as_ignored_must_not_be_classified() {
        $this->resetAfterTest();

        $this->setUser($this->teachers[0]);

        // Add a resource with a text file.
        $file = $this->create_file('juan.txt');
        $resource = $this->add_file_to_resource($file, $this->courses[0]);

        // We have something pending.
        $manager = manager::get_instance();
        $ret = $manager->should_notify_teachers($this->courses[0]->id);
        $this->assertTrue($ret);

        // Mark as ignored and no notification should be shown.
        $context = context_module::instance($resource->cmid);

        $fs = get_file_storage();
        $file = $fs->get_file($context->id, 'mod_resource', 'content', 0, '/', 'juan.txt');

        $if = new local_lpi\orm\ignored_file();
        $if->set_courseid($this->courses[0]->id);
        $if->set_fileid($file->get_id());
        $if->set_reason(local_lpi\orm\ignored_file::REASON_STUDENT_FILE);
        $if->set_timecreated(time());

        ignored_files_manager::get_instante()->add($if);

        // Now test that it's excluded.
        $ret = $manager->should_notify_teachers($this->courses[0]->id);
        $this->assertFalse($ret);
    }

    public function test_that_h5p_files_are_excluded_from_pending_metadata_classification () {
        $this->resetAfterTest();

        $manager = manager::get_instance();

        $this->setUser($this->teachers[0]);
        $file = $this->create_file('contenttype1.h5p', 'application/zip.h5p');
        $this->add_file_to_section($file, $this->courses[0], 0);
        $this->setUser();

        $pendingfiles = $manager->get_file_metadata($this->courses[0]->id);

        $this->assertEmpty($pendingfiles);
    }

    public function test_that_h5p_files_are_not_notified_as_to_be_classified() {
        $this->resetAfterTest();

        $manager = manager::get_instance();

        $this->setUser($this->teachers[0]);
        $file = $this->create_file('contenttype1.h5p', 'application/zip.h5p');
        $this->add_file_to_section($file, $this->courses[0], 0);
        $this->setUser();

        $this->assertFalse($manager->should_notify_teachers($this->courses[0]->id));
    }

}

