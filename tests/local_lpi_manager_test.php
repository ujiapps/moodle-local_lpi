<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit testing for manager.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

require_once(__DIR__ . '/commons.php');

/**
 * Unit test for local_lpi\manager
 *
 * @group local_lpi
 */
class local_lpi_manager_test extends \advanced_testcase {

    use files_methods;

    private $manager;

    private $teacher;
    private $neteacher;
    private $students;

    private $course;

    private $modules;

    public function setUp(): void {
        $this->teacher = $this->getDataGenerator()->create_user();

        $this->students = array();
        $this->students[] = $this->getDataGenerator()->create_user();
        $this->students[] = $this->getDataGenerator()->create_user();

        $this->neteacher = $this->getDataGenerator()->create_user();

        $this->course = $this->getDataGenerator()->create_course();

        foreach ($this->students as $student) {
            $this->getDataGenerator()->enrol_user($student->id, $this->course->id, 'student');
        }
        $this->getDataGenerator()->enrol_user($this->teacher->id, $this->course->id, 'editingteacher');
        $this->getDataGenerator()->enrol_user($this->neteacher->id, $this->course->id, 'teacher');

        // Editing teacher adds some resources with a file in it.
        $this->setUser($this->teacher);

        $record = new \stdClass();
        $record->course = $this->course;
        $record->files = $this->addFiles($this->course->id);

        $resourcegenerator = $this->getDataGenerator()->get_plugin_generator('mod_resource');

        // Three files with exactly the same content.
        $this->modules = array();
        for ($i = 0; $i < 3; $i++) {
            $record->files = $this->addFiles($this->course->id);
            $this->modules[] = $resourcegenerator->create_instance($record);
        }

        $this->setUser();

        $this->manager = \local_lpi\manager::get_instance();

    }

    public function test_that_changes_to_file_metadata_information_are_logged_to_the_file_metadata_log() {
        global $DB;

        $this->resetAfterTest();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = array_pop($filemetadatas);

        $filemetadata->set_state(file_metadata::STATE_LOE10);
        $this->manager->add_file_metadata($filemetadata);

        $fmlog = $DB->get_record('local_lpi_file_metadata_log', ['metadataid' => $filemetadata->get_id()]);
        $fm = $filemetadata->to_db();

        foreach ($fm as $key => $value) {
            if ($key === 'id') {
                $this->assertEquals($fm->id, $fmlog->metadataid);
            } else {
                $this->assertEquals($fm->$key, $fmlog->$key);
            }
        }
    }

    public function test_that_course_modules_in_hidden_courses_are_not_going_to_be_hidden() {
        global $DB;

        $this->resetAfterTest();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $this->manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - 3600 * 24 * 400;
        $DB->update_record('local_lpi_file_metadata', $r);

        // The course now invisible.
        course_change_visibility($this->course->id, false);

        $cms = $this->manager->get_cms_to_hide($r->timecreated + 3600);
        $this->assertEmpty($cms);
    }

    public function test_that_modules_are_not_hidden_if_they_contain_files_created_after_a_duedate()
    {
        global $DB;

        $this->resetAfterTest();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $this->manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - 3600 * 24 * 400;
        $DB->update_record('local_lpi_file_metadata', $r);

        // No modules to hide if duetime is greater than timecreated.
        $cms = $this->manager->get_cms_to_hide($r->timecreated - 3600);
        $this->assertEmpty($cms);
    }

    public function test_that_modules_are_hidden_if_they_contain_files_created_before_a_duedate() {
        global $DB;

        $this->resetAfterTest();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $this->manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - 3600 * 24 * 400;
        $DB->update_record('local_lpi_file_metadata', $r);

        // All modules should be returned if duetime is less that the one set.
        $cms = $this->manager->get_cms_to_hide($r->timecreated + 3600);
        $this->assertNotEmpty($cms);
        $this->assertCount(count($filemetadata->get_cmids()), $cms);

        $cmids = array_map(function($cm) {
            return $cm->cmid;
        }, $cms);

        foreach ($cmids as $cmid) {
            $this->assertContains($cmid, $filemetadata->get_cmids());
        }
        foreach ($filemetadata->get_cmids() as $cmid) {
            $this->assertContains($cmid, $cmids);
        }
    }

    public function test_that_timemodified_field_in_file_metadata_is_updated_when_file_metadata_is_updated() {
        $this->resetAfterTest();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = $filemetadatas[0];

        // On add timecreated and timemodified are equal.
        $this->manager->add_file_metadata($filemetadata);

        $inserted = $this->manager->get_file_metadata_by_id($filemetadata->get_id());

        $this->assertNotNull($inserted->get_timemodified());
        $this->assertEquals($inserted->get_timecreated(), $inserted->get_timemodified());

        sleep(1);

        $filemetadata->set_state(file_metadata::STATE_LOE10);
        $this->manager->add_file_metadata($filemetadata);

        $this->assertGreaterThan($filemetadata->get_timecreated(), $filemetadata->get_timemodified());

    }

    public function test_that_files_that_have_more_than_10_percent_fragment_must_be_reviewed_externally() {

        $this->resetAfterTest();

        $course2 = $this->getDataGenerator()->create_course();
        $this->getDataGenerator()->enrol_user(
                $this->teacher->id,
                $course2->id,
                'editingteacher'
        );
        $this->setUser($this->teacher);
        $this->addResource($course2);
        $this->addResource($course2, "Otro fichero");
        $this->setUser();

        $filemetadatas = $this->manager->get_file_metadata($course2->id);
        $filemetadata = current($filemetadatas);
        $filemetadata->set_userid($this->teacher->id);
        $filemetadata->set_state(file_metadata::STATE_GT10);

        $this->manager->add_file_metadata($filemetadata);

        /*
         * We have three activities at one course with the same content and
         * so the same metadata. And we have added one more course with two
         * resources with different content.
         *
         * Just one resource at course2 is in STATE_GT10 so we should have one
         * as a result.
         */

        $count = $this->manager->count_pending_files_to_review_externally();
        $this->assertEquals(1, $count);

        // Now put metadata in course 1 in GT10 state, so we should have two.

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = current($filemetadatas);
        $filemetadata->set_state(file_metadata::STATE_GT10);
        $filemetadata->set_title("Esto es otro fichero");
        $filemetadata->set_author("JSM");
        $filemetadata->set_userid($this->teacher->id);
        $this->manager->add_file_metadata($filemetadata);

        $count = $this->manager->count_pending_files_to_review_externally();
        $this->assertEquals(2, $count);

        // Now test search filter.
        $count = $this->manager->count_pending_files_to_review_externally("Otro fichero");
        $this->assertEquals(1, $count);

        $count = $this->manager->count_pending_files_to_review_externally("jsm");
        $this->assertEquals(1, $count);
    }

    public function test_that_getting_file_metadata_by_contenthash_returns_file_metadata_requested() {
        $this->resetAfterTest();

        $this->setUser($this->teacher);
        $this->addResource($this->course, "ama, ama y ensancha el alma");
        $this->setUser();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $contenthashes = array_map(function($filemetadata) {
            return $filemetadata->get_contenthash();
        }, $filemetadatas);

        $ret = $this->manager->get_file_metadata_by_contenthashes(
            $this->course->id,
            $contenthashes
        );

        // Test that the arrays have the same keys.
        $this->assertEmpty(array_diff_key(array_keys($ret), $contenthashes));
    }


    public function test_that_filtering_files_to_review_by_name_is_case_insensitive() {
        $this->resetAfterTest();

        // Add another file in other to have at last two metadatas inserted.
        $this->setUser($this->teacher);
        $this->addResource($this->course, "UN FICHERO");
        $this->setUser();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadatas[0]->set_author("Nicanor Parra");
        $filemetadatas[0]->set_userid($this->teacher->id);
        $filemetadatas[0]->set_state(file_metadata::STATE_GT10);
        $filemetadatas[1]->set_title("Poemas y antipoemas");
        $filemetadatas[1]->set_userid($this->teacher->id);
        $filemetadatas[1]->set_state(file_metadata::STATE_GT10);

        $this->manager->add_file_metadata($filemetadatas[0]);
        $this->manager->add_file_metadata($filemetadatas[1]);

        // Test author is searched.
        $files = $this->manager->get_pending_files_to_review_externally('paRra');
        $this->assertCount(1, $files);
        $this->assertEquals('Nicanor Parra', current($files)->author);

        // Test title is searched.
        $files = $this->manager->get_pending_files_to_review_externally("MAS y anTI");
        $this->assertCount(1, $files);
        $this->assertEquals("Poemas y antipoemas", current($files)->title);
    }

    public function test_that_files_associated_with_activities_deleted_are_not_returned_as_pending_to_review_externally() {
        $this->resetAfterTest();

        /*
         * When an activity is fully deleted it should not be retourned back.
         *
         * The test is this:
         * . Create a course.
         * . Create an activity with a file
         * . Put that file in GT10 state.
         * . Test that the file is returned.
         * . Delete the resource
         * . Test that the file is not returned.
         */
        $this->setUser($this->teacher);
        $course2 = $this->getDataGenerator()->create_course();
        $this->getDataGenerator()->enrol_user($this->teacher->id, $course2->id, 'editingteacher');
        $resource = $this->addResource($course2, "Mi contenido");
        $this->setUser();

        $filemetadatas = $this->manager->get_file_metadata($course2->id);
        $filemetadata = current($filemetadatas);
        $filemetadata->set_state(file_metadata::STATE_GT10);
        $filemetadata->set_userid($this->teacher->id);
        $this->manager->add_file_metadata($filemetadata);

        $ret = $this->manager->get_pending_files_to_review_externally();
        $this->assertCount(1, $ret);

        // Delete the resource.
        course_delete_module($resource->cmid);

        $ret = $this->manager->get_pending_files_to_review_externally();
        $this->assertEmpty($ret);

        // Now upload again the same file and assert that it gets returned again.
        $this->setUser($this->teacher);
        $resource = $this->addResource($course2, "Mi contenido");
        $this->setUser();

        $ret = $this->manager->get_pending_files_to_review_externally();
        $this->assertCount(1, $ret);
    }

    public function test_that_files_files_to_review_externally_returns_only_files_in_gt10_state() {
        $this->resetAfterTest();

        // Add 10 different files.
        $this->setUser($this->teacher);
        for ($i = 0; $i < 10; $i++) {
            $this->addResource($this->course, "Universitat Jaume I $i");
        }
        $this->setUser();

        // No file should be returned: they're all in UNKNOWN state.
        $files = $this->manager->get_pending_files_to_review_externally();
        $this->assertEmpty($files);

        // Put all metadata in all possible states but STATE_GT10 and verify
        // that no file is returned yet.
        $posgt10 = 0;
        $states = array();
        $allowedstates = file_metadata::get_allowedstates();
        foreach ($allowedstates as $st) {
            if ($st === file_metadata::STATE_GT10) {
                continue;
            }
            $states[] = $st;
        }
        unset($allowedstates);

        $filemetadatas = $this->manager->get_file_metadata(
                $this->course->id,
                null,
                null,
                null,
                null,
                0,
                1000
        );
        for ($i = 0; $i < count($filemetadatas); $i++) {
            $filemetadatas[$i]->set_state($states[$i % count($states)]);
            $filemetadatas[$i]->set_userid($this->teacher->id);
            $this->manager->add_file_metadata($filemetadatas[$i]);
        }

        $files = $this->manager->get_pending_files_to_review_externally();
        $this->assertEmpty($files);

        // Now test STATE_GT10 states are returned correctly. Put two files in
        // STATE_GT10 and these files should be returned by the method.
        $filemetadatas[8]->set_state(file_metadata::STATE_GT10);
        $filemetadatas[1]->set_state(file_metadata::STATE_GT10);
        $this->manager->add_file_metadata($filemetadatas[8]);
        $this->manager->add_file_metadata($filemetadatas[1]);

        $files = $this->manager->get_pending_files_to_review_externally();
        $this->assertCount(2, $files);

        // Now assert the the class contains the minimum set of attributes.
        $file = current($files);
        $this->assertTrue(property_exists($file, 'id'));
        $this->assertTrue(property_exists($file, 'title'));
        $this->assertTrue(property_exists($file, 'author'));
        $this->assertTrue(property_exists($file, 'state'));
        $this->assertTrue(property_exists($file, 'metadatauserid'));
        $this->assertTrue(property_exists($file, 'metadatauseridnumber'));
        $this->assertTrue(property_exists($file, 'metadatauseremail'));
        $this->assertTrue(property_exists($file, 'metadatatimecreated'));
    }

    public function test_get_file_metadata_by_contenthash_returns_all_the_metadata_available() {
        $this->resetAfterTest();

        // Not existing metadata should return null.
        $filemetadata = $this->manager->get_file_metadata_by_contenthash($this->course->id, 'x');
        $this->assertNull($filemetadata);

        // Get an existing metadata.
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = current($filemetadatas);

        $filemetadata->set_userid($this->teacher->id);
        $filemetadata->set_property(file_metadata::PROPERTY_PUBLICDOMAIN);
        $filemetadata->set_author("Miguel de Cervantes");

        $this->manager->add_file_metadata($filemetadata);

        $aux = $this->manager->get_file_metadata_by_contenthash($this->course->id, $filemetadata->get_contenthash());

        $this->assertEquals($aux->get_id(), $filemetadata->get_id());
        $this->assertEquals(file_metadata::PROPERTY_PUBLICDOMAIN, $aux->get_property());
        $this->assertEquals("Miguel de Cervantes", $aux->get_author());

        // It should have three modules (they have the same contenthash file).
        $this->assertCount(count($this->modules), $aux->get_cmids());
        $cmids = array_map(function($m) {
            return $m->cmid;
        }, $this->modules);

        $this->assertEmpty(array_diff($cmids, $aux->get_cmids()));
    }

    public function test_get_file_metadata_by_id_returns_all_metadata_available() {
        $this->resetAfterTest();

        // Not existing metadata should return null.
        $filemetadata = $this->manager->get_file_metadata_by_id(999999);
        $this->assertNull($filemetadata);

        // Get an existing metadata.
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = current($filemetadatas);
        $filemetadata->set_property(file_metadata::PROPERTY_PUBLICDOMAIN);
        $filemetadata->set_author("Miguel de Cervantes");

        $this->manager->add_file_metadata($filemetadata);
        $aux = $this->manager->get_file_metadata_by_id($filemetadata->get_id());
        $this->assertEquals($aux->get_id(), $filemetadata->get_id());
        $this->assertEquals(file_metadata::PROPERTY_PUBLICDOMAIN, $aux->get_property());
        $this->assertEquals("Miguel de Cervantes", $aux->get_author());

        // It should have three modules (they have the same contenthash file).
        $this->assertCount(count($this->modules), $aux->get_cmids());
        $cmids = array_map(function($m) {
            return $m->cmid;
        }, $this->modules);

        $this->assertEmpty(array_diff($cmids, $aux->get_cmids()));
    }

    public function test_that_editing_teachers_have_addfilemetadata_capability_by_default() {
        $this->resetAfterTest();
        $ctx = \context_course::instance($this->course->id);
        $this->assertTrue(has_capability('local/lpi:addfilemetadata', $ctx, $this->teacher));
    }

    public function test_that_non_editing_teachers_have_addfilemetadata_capability_by_default() {
        $this->resetAfterTest();
        $ctx = \context_course::instance($this->course->id);
        $this->assertFalse(has_capability('local/lpi:addfilemetadata', $ctx, $this->neteacher));
    }

    public function test_that_students_do_not_have_addfilemetadata_capability_by_default() {
        $this->resetAfterTest();
        $ctx = \context_course::instance($this->course->id);
        foreach ($this->students as $student) {
            $this->assertFalse(has_capability('local/lpi:addfilemetadata', $ctx, $student));
        }
    }

    public function test_that_teacher_must_be_notified_if_files_are_in_unknown_state() {
        $this->resetAfterTest();
        $lpi = local_lpi\manager::get_instance();
        $this->assertTrue($lpi->should_notify_teachers($this->course->id));
    }

    public function test_that_teachers_must_no_be_notified_of_files_in_hidden_course_modules() {
        $this->resetAfterTest();

        $lpi = local_lpi\manager::get_instance();

        // Hide course modules and it should not have unchecked files.
        foreach ($this->modules as $module) {
            set_coursemodule_visible($module->cmid, false);
        }
        $this->assertFalse($lpi->should_notify_teachers($this->course->id));
    }

    public function test_that_teachers_must_not_be_notified_of_files_in_a_known_state() {
        $this->resetAfterTest();

        $lpi = local_lpi\manager::get_instance();

        $pending = $lpi->get_file_metadata($this->course->id, null, null, null, null, 0, 1000);
        foreach ($pending as $filemetadata) {
            $filemetadata->set_state(local_lpi\orm\file_metadata::STATE_GT10_AND_PERM);
            $lpi->add_file_metadata($filemetadata);
        }

        $this->assertFalse($lpi->should_notify_teachers($this->course->id));
    }

    public function test_get_real_number_of_enroled_students() {
        $this->resetAfterTest();
        $n = $this->manager->get_real_number_of_enroled_students($this->course->id);
        $this->assertEquals(count($this->students), $n);
    }

    public function test_recyclebin_files_are_not_taken_into_account () {
        $this->resetAfterTest();

        set_config('coursebinenable', 1, 'tool_recyclebin');

        // Delete all three modules with files.
        $modulestodeletecount = count($this->modules);
        $this->assertGreaterThan(0, $modulestodeletecount);

        foreach ($this->modules as $module) {
            course_delete_module($module->cmid);
        }

        // Check the course module is now in the recycle bin.
        global $DB;
        $this->assertEquals($modulestodeletecount, $DB->count_records('tool_recyclebin_course'));

        $filesmetadatas = $this->manager->get_file_metadata($this->course->id);
        $this->assertCount(0, $filesmetadatas);
    }
}
