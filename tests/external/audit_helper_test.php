<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Test for audit.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../commons.php');

use local_lpi\audit\audit_helper;
use local_lpi\objects\document_content;

/**
 * @group local_lpi
 */
class audit_helper_test extends \advanced_testcase {

    use files_methods;
    use task_methods;

    private $courses;
    private $teachers;

    public function setUp(): void {

        $this->courses = [];
        $this->courses[] = $this->getDataGenerator()->create_course();
        $this->courses[] = $this->getDataGenerator()->create_course();
        $this->courses[] = $this->getDataGenerator()->create_course();

        $this->teachers = [];
        foreach ($this->courses as $c) {
            $t = $this->getDataGenerator()->create_user();
            $this->getDataGenerator()->enrol_user($t->id, $c->id, 'editingteacher');
            $this->teachers[$c->id] = $t;
        }
    }

    public function test_get_file_metadata_dont_return_files_in_unknonwn_state() {
        $this->resetAfterTest();

        $manager = \local_lpi\manager::get_instance();
        $audit = audit_helper::get_instance();

        // Add two files to a course.
        $course = array_shift($this->courses);

        $this->setUser($this->teachers[$course->id]);
        $this->addResource($course, "juan 1");
        $this->addResource($course, "juan 2");
        $this->setUser();

        $ret = $audit->get_file_metadata();
        $this->assertCount(0, $ret);

        $this->run_task_insert_orphaned_files();

        $ret = $audit->get_file_metadata();
        $this->assertCount(0, $ret);
    }

    public function test_get_file_metadata_dont_return_files_in_administrative_state() {
        $this->resetAfterTest();

        $manager = \local_lpi\manager::get_instance();
        $audit = audit_helper::get_instance();

        // Add two files to a course.
        $course = array_shift($this->courses);

        $this->setUser($this->teachers[$course->id]);
        $this->addResource($course, "juan 1");
        $this->addResource($course, "juan 2");
        $this->setUser();

        $this->run_task_insert_orphaned_files();

        // Set the state of one file to be administrative.
        $filemetadatas = $manager->get_file_metadata($course->id);

        $filemetadata = array_pop($filemetadatas);
        $documentcontent = new document_content(document_content::ADMINISTRATIVE);
        $documentcontent->update_file_metadata($filemetadata);
        $manager->add_file_metadata($filemetadata);

        $filemetadata = array_pop($filemetadatas);
        $documentcontent = new document_content(document_content::HAVELICENSE);
        $documentcontent->update_file_metadata($filemetadata);
        $manager->add_file_metadata($filemetadata);

        // Now we can just return one file and it should be type HAVELICENSE.
        $ret = $audit->get_file_metadata();
        $this->assertCount(1, $ret);

        $ret = array_pop($ret);

        $dc = document_content::get_document_content_by_file_metadata($ret);
        $this->assertEquals(document_content::HAVELICENSE, $dc->get_value());

    }

    public function test_that_requesting_more_files_than_the_maximum_limit_returns_error() {
        $this->resetAfterTest();
        $this->expectException(\local_lpi\exception\max_number_of_results_requested_exceeded::class);

        $audit = audit_helper::get_instance();
        $audit->get_file_metadata(0, audit_helper::MAX_LIMITNUM + 1);
    }

    public function test_get_file_metadata() {
        $this->resetAfterTest();

        $manager = \local_lpi\manager::get_instance();
        $audit = audit_helper::get_instance();

        // No files in any course so zero-count array should be returned.
        $ret = $audit->get_file_metadata();
        $this->assertCount(0, $ret);

        // Add same content resource files to three different courses.
        foreach ($this->courses as $c) {
            $this->setUser($this->teachers[$c->id]);
            $this->addResource($c);
            $this->setUser();
        }

        // Still no file_metadata: we need to execute the insert_orphaed_task.
        $ret = $audit->get_file_metadata();
        $this->assertCount(0, $ret);

        $this->run_task_insert_orphaned_files();

        // Still no file_metadata: they're in unknown state.
        $ret = $audit->get_file_metadata();
        $this->assertCount(0, $ret);

        // Now set the state of one other than administrative or unknown.
        $filemetadatas = $manager->get_file_metadata($this->courses[0]->id);
        $filemetadata = array_pop($filemetadatas);
        $dc = new document_content(document_content::OWNED_BY_TEACHERS);
        $dc->update_file_metadata($filemetadata);
        $manager->add_file_metadata($filemetadata);

        $ret = $audit->get_file_metadata();
        $this->assertCount(1, $ret);
    }

}
