<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for the audit service.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

use local_lpi\objects\document_content;
use local_lpi\exception\max_number_of_results_requested_exceeded;
use local_lpi\external\audit_service;

global $CFG;

require_once($CFG->dirroot . '/webservice/tests/helpers.php');
require_once(__DIR__ . '/../commons.php');

/**
 * @group local_lpi
 */
class audit_service_test extends externallib_advanced_testcase {

    use files_methods;
    use task_methods;

    private $courses;
    private $teachers;
    private $students;

    public function setUp(): void {
        $this->teachers = [];
        $this->courses = [];
        $this->students = [];
        $this->courses[] = $this->getDataGenerator()->create_course();
        $this->courses[] = $this->getDataGenerator()->create_course();
        $this->courses[] = $this->getDataGenerator()->create_course();
        foreach ($this->courses as $c) {
            $t = $this->getDataGenerator()->create_user();
            $s = $this->getDataGenerator()->create_user();
            $this->getDataGenerator()->enrol_user($t->id, $c->id, 'editingteacher');
            $this->getDataGenerator()->enrol_user($s->id, $c->id, 'student');
            $this->teachers[$c->id] = $t;
            $this->students[$c->id] = $s;
        }
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_auditable_file_metadata_authz() {
        $this->resetAfterTest();

        // Users without local/lpi:audit capacility at system context cannot
        // get auditables.
        $this->setUser(array_shift($this->teachers));
        $this->expectException(\required_capability_exception::class);
        $auditables = audit_service::get_auditable_file_metadata(0, local_lpi\external\audit_service::PAGE_SIZE + 1);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_courseid_pseudonimised() {
        $this->resetAfterTest();

        $manager = local_lpi\manager::get_instance();

        // Add a file to a course and set its state.
        $this->setUser($this->teachers[$this->courses[0]->id]);
        $this->addResource($this->courses[0], "My File");
        $this->setUser();

        $filemetadatas = $manager->get_file_metadata($this->courses[0]->id);
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_document_content(new document_content(document_content::PUBLIC_DOMAIN));
        $manager->add_file_metadata($filemetadata);

        // Now retrieve the auditable and test that the courseid is not the same.
        $this->setAdminUser();
        $auditables = local_lpi\external\audit_service::get_auditable_file_metadata(0, 100);
        $auditables = external_api::clean_returnvalue(
            local_lpi\external\audit_service::get_auditable_file_metadata_returns(),
            $auditables
        );

        // We have just one auditable.
        $auditable = (object) array_pop($auditables);
        $this->assertNotEquals($this->courses[0]->id, $auditable->courseid);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_auditable_file_metadata_error_too_much_data() {
        $this->resetAfterTest();
        $this->expectException(max_number_of_results_requested_exceeded::class);
        // No file_metadata returned.
        $this->setAdminUser();
        $auditables = local_lpi\external\audit_service::get_auditable_file_metadata(0, local_lpi\external\audit_service::PAGE_SIZE + 1);

    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_auditable_file_metadata() {
        $this->resetAfterTest();

        $manager = \local_lpi\manager::get_instance();

        // No file_metadata returned.
        $this->setAdminUser();
        $auditables = local_lpi\external\audit_service::get_auditable_file_metadata(0, 100);
        $auditables = external_api::clean_returnvalue(
            local_lpi\external\audit_service::get_auditable_file_metadata_returns(),
            $auditables
        );
        $this->setUser();

        $this->assertEmpty($auditables);

        // Add some files and metadata to each course.
        $i = 0;
        foreach ($this->courses as $c) {

            $this->setUser($this->teachers[$c->id]);
            $this->addResource($c, "Un fichero " . $i++);
            $this->setUser();

            $filemetadatas = $manager->get_file_metadata($c->id);
            $filemetadata = array_pop($filemetadatas);
            $filemetadata->set_document_content(new document_content(document_content::EDITED_BY_UNIVERSITY));

            $manager->add_file_metadata($filemetadata);
        }

        // Now we should get three file_metadatas.
        $this->setAdminUser();
        $auditables = local_lpi\external\audit_service::get_auditable_file_metadata(0, 100);
        $auditables = external_api::clean_returnvalue(
            local_lpi\external\audit_service::get_auditable_file_metadata_returns(),
            $auditables
        );

        $this->assertCount(3, $auditables);
    }
}