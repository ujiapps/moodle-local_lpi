<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for the integration service.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

use local_lpi\external\integration_service;

global $CFG;
require_once($CFG->dirroot . '/webservice/tests/helpers.php');
require_once($CFG->libdir . '/licenselib.php');

/**
 * @group local_lpi
 */
class integration_service_test extends \externallib_advanced_testcase {

    const NUM_ACTIVITIES = 10;

    private $course;
    private $teachers;
    private $students;

    private $activities;

    private $manager;

    public function setUp(): void {
        parent::setUp();

        $this->manager = local_lpi\manager::get_instance();

        $this->course = $this->getDataGenerator()->create_course();
        $this->teachers[] = $this->getDataGenerator()->create_user(array(
            'idnumber' => 0
        ));
        $this->teachers[] = $this->getDataGenerator()->create_user(array(
            'idnumber' => 1
        ));
        $this->students[] = $this->getDataGenerator()->create_user();

        foreach ($this->teachers as $teacher) {
            $this->getDataGenerator()->enrol_user($teacher->id, $this->course->id, 'editingteacher');
        }
        foreach ($this->students as $student) {
            $this->getDataGenerator()->enrol_user($student->id, $this->course->id, 'student');
        }
        for ($t = 0; $t < count($this->teachers); $t++) {
            $this->setUser($this->teachers[$t]);
            for ($i = 0; $i < self::NUM_ACTIVITIES; $i++) {
                $this->activities[] = $this->getDataGenerator()->create_module(
                    'resource',
                    array(
                        'course' => $this->course
                    )
                );
            }
        }
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_pending_files_authz() {
        $this->resetAfterTest();

        // No one without local/lpi:addfilemetadata at system context can
        // get pending_files.
        $this->expectException(required_capability_exception::class);
        $this->setUser($this->students[0]);
        $pending = integration_service::get_pending_files();

    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_file_metadata_authz() {
        $this->resetAfterTest();

        $pending = $this->manager->get_file_metadata($this->course->id);

        $fm = array_pop($pending);
        $fm->set_author("Juan Segarra Montesinos");
        $fm->set_title("Introducción a la programación con OpenSSL");
        $fm->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $fm->set_userid($this->teachers[0]->id);

        $this->manager->add_file_metadata($fm);

        $this->expectException(required_capability_exception::class);
        $this->setUser($this->students[0]);
        integration_service::get_file_metadata($fm->get_id());
    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization_authz() {
        $this->resetAfterTest();

        $pending = $this->manager->get_file_metadata($this->course->id);

        $fm = array_pop($pending);
        $fm->set_author("Juan Segarra Montesinos");
        $fm->set_title("Introducción a la programación con OpenSSL");
        $fm->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $fm->set_userid($this->teachers[0]->id);

        $this->manager->add_file_metadata($fm);

        $this->setUser($this->students[0]);
        $this->expectException(required_capability_exception::class);
        integration_service::set_file_authorization(
            $fm->get_id(),
            integration_service::AUTHZ_OK,
            \local_lpi\objects\document_content::ADMINISTRATIVE
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_externally_reviewed_logs_authz() {
        $this->resetAfterTest();

        $this->expectException(\required_capability_exception::class);
        $this->setUser($this->teachers[0]);
        integration_service::get_externally_reviewed_logs(
            0,
            integration_service::PAGE_SIZE
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_pending_files_nofile() {
        $this->resetAfterTest();

        // Test return structure.
        $this->setAdminUser();
        $pending = integration_service::get_pending_files();
        $pending = external_api::clean_returnvalue(integration_service::get_pending_files_returns(), $pending);
        $this->setUser();

        $this->assertArrayHasKey('count', $pending);
        $this->assertArrayHasKey('data', $pending);
        $this->assertIsInt($pending['count']);
        $this->assertIsArray($pending['data']);

        // No file is returned because they're not in DOUBT or GT10 states.
        $this->assertEquals(0, $pending['count']);
        $this->assertEmpty($pending['data']);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_pending_files() {
        $this->resetAfterTest();

        $metadatas = [];

        $pending = $this->manager->get_file_metadata($this->course->id);
        for ($i = 0; $i < 10; $i++) {
            $filemetadata = array_pop($pending);
            $filemetadata->set_userid($this->teachers[0]->id);
            $filemetadata->set_title("TITLE $i");
            $filemetadata->set_author("AUTHOR $i");
            $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_GT10);

            $this->manager->add_file_metadata($filemetadata);

            $metadatas[$filemetadata->get_id()] = $filemetadata;
        }

        // Now we should get ten files.
        $this->setAdminUser();
        $pending = integration_service::get_pending_files();
        $pending = external_api::clean_returnvalue(integration_service::get_pending_files_returns(), $pending);
        $this->setUser();

        $this->assertEquals(10, $pending['count']);
        $this->assertCount(10, $pending['data']);

        for ($i = 0; $i < 10; $i++) {
            $data = $pending['data'][$i];

            $this->assertEquals($metadatas[$data['id']]->get_title(), $data['title']);
            $this->assertEquals($metadatas[$data['id']]->get_author(), $data['author']);
            $this->assertEquals($metadatas[$data['id']]->get_state(), $data['state']);
            $this->assertEquals($metadatas[$data['id']]->get_userid(), $data['metadatauserid']);
            $this->assertEquals($this->teachers[0]->idnumber, $data['metadatauseridnumber']);
            $this->assertEquals($this->teachers[0]->email, $data['metadatauseremail']);
            $this->assertEquals($metadatas[$data['id']]->get_timecreated(), $data['metadatatimecreated']);
        }

        // Now test just count param.
        $this->setAdminUser();
        $pending = integration_service::get_pending_files(0, 2);
        $pending = external_api::clean_returnvalue(integration_service::get_pending_files_returns(), $pending);
        $this->setUser();

        $this->assertEquals(10, $pending['count']);
        $this->assertCount(2, $pending['data']);

        // Now test page param.
        $this->setAdminUser();
        $page = 0;
        do {
            $pending = integration_service::get_pending_files($page, 2);
            $pending = external_api::clean_returnvalue(integration_service::get_pending_files_returns(), $pending);
            $page++;
        } while (count($pending['data']) > 0);
        $this->setUser();

        $this->assertEquals(6, $page);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_pending_files_search() {
        $this->resetAfterTest();

        $pending = $this->manager->get_file_metadata($this->course->id);

        $fm = array_pop($pending);
        $fm->set_author("Juan Segarra Montesinos");
        $fm->set_title("Introducción a la programación con OpenSSL");
        $fm->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $fm->set_userid($this->teachers[0]->id);

        $this->manager->add_file_metadata($fm);

        $fm = array_pop($pending);
        $fm->set_author("Patrick Rothfuss");
        $fm->set_title("El nombre del viento");
        $fm->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $fm->set_userid($this->teachers[1]->id);

        $this->manager->add_file_metadata($fm);

        // Assert author is searched.
        $this->setAdminUser();
        $pending = integration_service::get_pending_files(0, 10, 'rothFUss');
        $pending = external_api::clean_returnvalue(integration_service::get_pending_files_returns(), $pending);
        $this->setUser();

        $this->assertEquals(1, $pending['count']);
        $this->assertEquals("El nombre del viento", $pending['data'][0]['title']);

        // Assert title is searched.
        $this->setAdminUser();
        $pending = integration_service::get_pending_files(0, 10, 'pROgramaciÓN');
        $pending = external_api::clean_returnvalue(integration_service::get_pending_files_returns(), $pending);
        $this->setUser();

        $this->assertEquals(1, $pending['count']);
        $this->assertEquals("Juan Segarra Montesinos", $pending['data'][0]['author']);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_file_metadata_notfound_metadata() {
        $this->resetAfterTest();
        $this->expectException(\local_lpi\exception\metadata_not_found_exception::class);

        // First, non existent metadata.
        integration_service::get_file_metadata(80);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_file_metadata_bad_state() {
        $this->resetAfterTest();

        // Second, state is not DOUBT or GT10.
        $pending = $this->manager->get_file_metadata($this->course->id);

        $states = array(
            local_lpi\orm\file_metadata::STATE_FORBIDDEN,
            local_lpi\orm\file_metadata::STATE_GT10_AND_PERM,
            local_lpi\orm\file_metadata::STATE_LOE10,
            local_lpi\orm\file_metadata::STATE_OK,
            local_lpi\orm\file_metadata::STATE_UNKNOWN
        );
        $fms = array();
        foreach ($states as $state) {
            $fm = array_pop($pending);
            $fm->set_state($state);
            $this->manager->add_file_metadata($fm);
            try {
                integration_service::get_file_metadata($fm->get_id());
                $this->assertTrue(false);
            } catch (\local_lpi\exception\metadata_not_found_exception $e) {
                continue;
            }
        }
    }

    /**
     * @runInSeparateProcess
     */
    public function test_get_file_metadata() {
        $this->resetAfterTest();

        $pending = $this->manager->get_file_metadata($this->course->id);

        $fm = array_pop($pending);

        $fm->set_author("Juan Segarra Montesinos");
        $fm->set_comments("Esto es un comentario");
        $fm->set_identification("ISBN.1");
        $licenses = license_manager::get_licenses();
        $fm->set_license(array_pop($licenses)->shortname);
        $fm->set_pages("1,2,60-100");
        $fm->set_publisher("Editorial UJI");
        $fm->set_title("Aprende a programar en C");
        $fm->set_totalpages(253);
        $fm->set_type(local_lpi\orm\file_metadata::TYPE_PRINTED_OR_PRINTABLE);
        $fm->set_state(local_lpi\orm\file_metadata::STATE_GT10);
        $fm->set_userid($this->teachers[0]->id);

        $this->manager->add_file_metadata($fm);

        $metadata = integration_service::get_file_metadata($fm->get_id());
        $metadata = external_api::clean_returnvalue(integration_service::get_file_metadata_returns(), $metadata);

        $this->assertEquals($fm->get_author(), $metadata['author']);
        $this->assertEquals($fm->get_comments(), $metadata['comments']);
        $this->assertEquals($fm->get_identification(), $metadata['identification']);
        $this->assertEquals($fm->get_pages(), $metadata['pages']);
        $this->assertEquals($fm->get_publisher(), $metadata['publisher']);
        $this->assertEquals($fm->get_title(), $metadata['title']);
        $this->assertEquals($fm->get_totalpages(), $metadata['totalpages']);
        $this->assertEquals($fm->get_state(), $metadata['state']);
    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization_not_found() {
        $this->resetAfterTest();
        $this->expectException(\local_lpi\exception\metadata_not_found_exception::class);

        // Unknown metadataid.
        integration_service::set_file_authorization(
            99,
            integration_service::AUTHZ_FORBIDDEN,
            \local_lpi\objects\document_content::ADMINISTRATIVE,
            null,
            null
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization_not_supported_state() {
        $this->resetAfterTest();

        // Bad state
        $ids = array();

        $pending = $this->manager->get_file_metadata($this->course->id);
        foreach (local_lpi\orm\file_metadata::get_allowedstates() as $state) {
            if (in_array($state, [local_lpi\orm\file_metadata::STATE_GT10])) {
                continue;
            }

            $fm = array_pop($pending);
            $fm->set_state($state);
            $this->manager->add_file_metadata($fm);

            try {
                integration_service::set_file_authorization(
                        $fm->get_id(),
                        integration_service::AUTHZ_FORBIDDEN,
                        \local_lpi\objects\document_content::UNKNOWN,
                        time() + 3600,
                        "No puedes porque no"
                );
                $this->assertTrue(false);
            } catch (local_lpi\exception\not_supported_state_exception $e) {
                continue;
            }
        }
    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization_invalid_authorization() {
        $this->resetAfterTest();
        $this->expectException(local_lpi\exception\invalid_authorization_exception::class);

        $validauthorization = array(
            integration_service::AUTHZ_OK,
            integration_service::AUTHZ_FORBIDDEN
        );

        $pendings = $this->manager->get_file_metadata($this->course->id);
        foreach ($validauthorization as $authz) {
            $filemetadata = array_pop($pendings);
            $filemetadata->set_state(local_lpi\orm\file_metadata::STATE_GT10);
            $this->manager->add_file_metadata($filemetadata);

            // This doesn' throw an exception.
            try {
                integration_service::set_file_authorization(
                        $filemetadata->get_id(),
                        $authz,
                        \local_lpi\objects\document_content::ADMINISTRATIVE,
                        time() + 3600,
                        "Deny"
                );
            } catch (local_lpi\exception\invalid_authorization_exception $e) {
                $this->assertTrue(false);
            }
        }

        // This throws an exception.
        $filemetadata = array_pop($pendings);
        $filemetadata->set_state(local_lpi\orm\file_metadata::STATE_GT10);
        $this->manager->add_file_metadata($filemetadata);

        integration_service::set_file_authorization(
                $filemetadata->get_id(),
                "laujirocks",
                \local_lpi\objects\document_content::ADMINISTRATIVE
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization_timeendlicense_needed() {
        $this->resetAfterTest();
        $this->expectException(local_lpi\exception\timeendlicense_needed_exception::class);

        $pending = $this->manager->get_file_metadata($this->course->id);
        $fm = array_pop($pending);
        $fm->set_state(local_lpi\orm\file_metadata::STATE_GT10);
        $this->manager->add_file_metadata($fm);

        integration_service::set_file_authorization(
                $fm->get_id(),
                integration_service::AUTHZ_OK,
                \local_lpi\objects\document_content::OTHER,
                null
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization() {
        $this->resetAfterTest();

        // First test forbidden.

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $this->manager->add_file_metadata($filemetadata);

        $ret = integration_service::set_file_authorization(
                $filemetadata->get_id(),
                integration_service::AUTHZ_FORBIDDEN,
                local_lpi\objects\document_content::OTHER,
                10,
                'Deny Reason 1'
        );
        $ret = external_api::clean_returnvalue(
                integration_service::set_file_authorization_returns(),
                $ret
        );

        $this->assertEquals($ret['id'], $filemetadata->get_id());
        $this->assertEquals($ret['authz'], integration_service::AUTHZ_FORBIDDEN);
        $this->assertEquals($ret['denyreason'], 'Deny Reason 1');

        $newfilemetadata = $this->manager->get_file_metadata_by_id($filemetadata->get_id());
        $this->assertEquals($newfilemetadata->get_state(), \local_lpi\orm\file_metadata::STATE_FORBIDDEN);
        $this->assertEquals($newfilemetadata->get_denyreason(), 'Deny Reason 1');

        // Test authz ok.
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $this->manager->add_file_metadata($filemetadata);

        $ret = integration_service::set_file_authorization(
                $filemetadata->get_id(),
                integration_service::AUTHZ_OK,
                local_lpi\objects\document_content::ADMINISTRATIVE,
                0,
                ''
        );
        $ret = external_api::clean_returnvalue(
                integration_service::set_file_authorization_returns(),
                $ret
        );

        $this->assertEquals($ret['id'], $filemetadata->get_id());
        $this->assertEquals($ret['authz'], integration_service::AUTHZ_OK);
        $this->assertEquals($ret['documentcontent'], local_lpi\objects\document_content::ADMINISTRATIVE);

        $newfilemetadata = $this->manager->get_file_metadata_by_id($filemetadata->get_id());
        $newdocumentcontent = local_lpi\objects\document_content::get_document_content_by_file_metadata($newfilemetadata);

        $this->assertEquals(local_lpi\objects\document_content::ADMINISTRATIVE, $newdocumentcontent);

        // Test auth ok and license.
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_GT10);
        $this->manager->add_file_metadata($filemetadata);

        $timeendlicense = time() + 3600;
        $ret = integration_service::set_file_authorization(
                $filemetadata->get_id(),
                integration_service::AUTHZ_OK,
                local_lpi\objects\document_content::OTHER,
                $timeendlicense,
                ''
        );
        $ret = external_api::clean_returnvalue(
                integration_service::set_file_authorization_returns(),
                $ret
        );

        $this->assertEquals($ret['id'], $filemetadata->get_id());
        $this->assertEquals($ret['authz'], integration_service::AUTHZ_OK);
        $this->assertEquals($ret['documentcontent'], local_lpi\objects\document_content::HAVELICENSE);
        $this->assertEquals($ret['timeendlicense'], $timeendlicense);

        $newfilemetadata = $this->manager->get_file_metadata_by_id($filemetadata->get_id());
        $newdocumentcontent = local_lpi\objects\document_content::get_document_content_by_file_metadata($newfilemetadata);

        $this->assertEquals(local_lpi\objects\document_content::HAVELICENSE, $newdocumentcontent);
        $this->assertEquals($timeendlicense, $newfilemetadata->get_timeendlicense());

    }

    /**
     * @runInSeparateProcess
     */
    public function test_set_file_authorization_not_printable() {
        $this->resetAfterTest();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        $filemetadata = array_pop($filemetadatas);
        $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_GT10);

        $this->manager->add_file_metadata($filemetadata);

        $ret = integration_service::set_file_authorization(
                $filemetadata->get_id(),
                integration_service::AUTHZ_OK,
                local_lpi\objects\document_content::NOT_PRINTED_OR_PRINTABLE,
                0,
                ''
        );
        $ret = external_api::clean_returnvalue(
                integration_service::set_file_authorization_returns(),
                $ret
        );

        $this->assertEquals($ret['id'], $filemetadata->get_id());
        $this->assertEquals($ret['authz'], integration_service::AUTHZ_OK);
        $this->assertEquals($ret['documentcontent'], local_lpi\objects\document_content::NOT_PRINTED_OR_PRINTABLE);

        $newfilemetadata = $this->manager->get_file_metadata_by_id($filemetadata->get_id());
        $newdocumentcontent = local_lpi\objects\document_content::get_document_content_by_file_metadata($newfilemetadata);

        $this->assertEquals(local_lpi\objects\document_content::NOT_PRINTED_OR_PRINTABLE, $newdocumentcontent);
    }
}
