@core @core_local @uji @lpi
Feature: derechos_lpi
    Creación de tests de aceptación para local/lpi

    Background:
      Given the following "users" exist:
        | username | firstname | lastname  | email |
        | u0 	   | User0     | Lastname0 | u0@example.com |
        | u1 	   | User1     | Lastname1 | u1@example.com |
        | u2 	   | User2     | Lastanme2 | u2@example.com |
      And the following "courses" exist:
        | fullname | shortname | format |
        | Course 0 | c0        | topics |
      And the following "course enrolments" exist:
        | user | course | role |
	| u0   | c0     | editingteacher |
        | u1   | c0     | teacher |
        | u2   | c0     | student |


    @javascript @_file_upload
    Scenario: Como profesor  con permisos de edición veo el aviso de gestión de derechos
        Given I log in as "u0"
        And I am on "Course 0" course homepage with editing mode on

        When I add a resource activity to course "Course 0" section "1" and I fill the form with:
          | Name                      | MyFileDemo |
          | Description               | FileDescription |
        And I upload "local/lpi/tests/fixtures/testfile.txt" file to "Select files" filemanager
        And I press "Save and return to course"

        Then I am on "Course 0" course homepage 
        And I should see "MyFileDemo"
        And I should see "Aviso sobre gestión de derechos de propiedad intelectual"


    @javascript @_file_upload
    Scenario: Como estudiante no debo ver el aviso de gestión de derechos de autor
        Given I log in as "u0"
        And I am on "Course 0" course homepage with editing mode on

        When I add a resource activity to course "Course 0" section "1" and I fill the form with:
          | Name                      | MyFileDemo |
          | Description               | FileDescription |
        And I upload "local/lpi/tests/fixtures/testfile.txt" file to "Select files" filemanager
        And I press "Save and return to course"

        Then I log out 
        And I log in as "u2"
        And I am on "Course 0" course homepage 
        And I should not see "Aviso sobre gestión de derechos de propiedad intelectual"


    Scenario: Como profesor con permisos de edición debo ver el enlace del burguer button
        Given I log in as "u0"
        And I am on "Course 0" course homepage
        When I click on "More" "link" in the ".secondary-navigation" "css_element"
        Then I should see "Propiedad Intelectual"

  
    Scenario: Como estudiante no debo ver el enlace del menú del burguer button
        Given I log in as "u2"
        And I am on "Course 0" course homepage
        When I click on "More" "link" in the ".secondary-navigation" "css_element"
        Then I should not see "Propiedad Intelectual"


    @javascript @_file_upload
    Scenario: Como profesor con permisos de edición no veo el aviso si actividades ocultas
        Given I log in as "u0"
        And I am on "Course 0" course homepage with editing mode on
        And I add a resource activity to course "Course 0" section "1" and I fill the form with:
          | Name                      | MyFileDemo |
          | Description               | FileDescription |
        And I upload "local/lpi/tests/fixtures/testfile.txt" file to "Select files" filemanager
        And I press "Save and return to course"
        And I open "MyFileDemo" actions menu
        And I click on "Hide" "link" in the "MyFileDemo" activity
        When I am on "Course 0" course homepage
        Then I should not see "Aviso sobre gestión de derechos de propiedad intelectual"
