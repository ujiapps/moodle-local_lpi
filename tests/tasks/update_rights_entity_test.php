<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Test for the hide_expired_modules activity.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/..//commons.php');

/**
 * @group local_lpi
 */
class update_rights_entity_test extends advanced_testcase {

    use files_methods;

    /**
     * @var \stdClass
     */
    private $course;
    /**
     * @var \local_lpi\manager
     */
    private $manager;

    public function setUp(): void {
        $this->course = $this->getDataGenerator()->create_course();

        $this->setAdminUser();
        $this->addResource($this->course, "Esto es un recurso");
        $this->addResource($this->course, "Esto es otro recurso");
        $this->setUser();

        $this->manager = \local_lpi\manager::get_instance();
    }

    public function test_not_prontable_or_printable_documents_are_excluded_from_updating_its_rights_entity_management() {
        $this->resetAfterTest();

        // Set the state for the files. No rights entity should be added.
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        foreach ($filemetadatas as $filemetadata) {
            $filemetadata->set_type(\local_lpi\orm\file_metadata::TYPE_NOT_PRINTED_OR_PRINTABLE);
            $this->manager->add_file_metadata($filemetadata);
        }

        $task = new \local_lpi\task\update_rights_entity();
        $task->execute();

        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        foreach ($filemetadatas as $filemetadata) {
            $this->assertNull($filemetadata->get_rightsentity());
        }
    }

    public function test_that_rights_management_entity_is_updated_for_printed_or_printable_documents() {

        $this->resetAfterTest();

        // Set the state for the files. No rights entity should be added.
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        foreach ($filemetadatas as $filemetadata) {
            $filemetadata->set_state(\local_lpi\orm\file_metadata::STATE_LOE10);
            $this->manager->add_file_metadata($filemetadata);
        }

        // The rights entity should not be set.
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        foreach ($filemetadatas as $filemetadata) {
            $this->assertNull($filemetadata->get_rightsentity());
        }

        // Execute the task.
        $task = new \local_lpi\task\update_rights_entity();
        $task->execute();

        // Now the rights entity should be set.
        $filemetadatas = $this->manager->get_file_metadata($this->course->id);
        foreach ($filemetadatas as $filemetadata) {
            $this->assertNotNull($filemetadata->get_rightsentity());
        }
    }
}
