<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* Test for the hide_expired_modules activity.
*
* @package    local
* @subpackage lpi
* @copyright  2018 Universitat Jaume I (http://www.uji.es/)
* @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
*/

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__) . '/../commons.php');

class send_doubts_email_test extends advanced_testcase {

    use files_methods;

    private $course;
    private $resource;
    private $teacher;

    public function setUp(): void {
        $this->course = $this->getDataGenerator()->create_course();
        $this->teacher = $this->getDataGenerator()->create_user();
        $this->getDataGenerator()->enrol_user($this->teacher->id, $this->course->id, 'editingteacher');

        $this->setUser($this->teacher);
        $this->resource = $this->addResource($this->course, 'Un recurso');
        $this->setUser();

        set_config('doubtsemail', 'noreply@uji.es', 'local_lpi');
    }

    public function test_a_doubt_is_sent_if_created() {
        $this->resetAfterTest();

        $this->setUser($this->teacher);

        $manager = \local_lpi\manager::get_instance();
        $file_metadata = $manager->get_file_metadata($this->course->id);
        $file_metadata = array_shift($file_metadata);

        $doubt = new \local_lpi\orm\doubt();
        $doubt->set_courseid($this->course->id);
        $doubt->set_message('A doubt');
        $doubt->set_messageformat(FORMAT_PLAIN);
        $doubt->set_timecreated(time());
        $doubt->set_userid($this->teacher->id);
        $doubt->set_contenthash($file_metadata->get_contenthash());

        $doubt_queue = \local_lpi\doubt_queue_manager::get_instance();
        $doubt_queue->enqueue($doubt);

        $this->setUser();

        $config = \local_lpi\config::get_instance();

        // Execute the task and no exception should be thrown.
        $emailSink = $this->redirectEmails();
        $eventSink = $this->redirectEvents();

        //ob_start();
        $task = new \local_lpi\task\send_doubts_email();
        $task->execute();
        //ob_end_clean();

        $this->assertEquals(1, $emailSink->count());
        $this->assertEquals($config->get_doubtsemail(), $emailSink->get_messages()[0]->to);
        $this->assertEquals($this->teacher->email, $emailSink->get_messages()[0]->from);
        $this->assertNotEmpty($emailSink->get_messages()[0]->subject);
        $this->assertNotEmpty($emailSink->get_messages()[0]->body);

        $events = $eventSink->get_events();
        $this->assertCount(1, $events);
        $this->assertInstanceOf(\local_lpi\event\doubt_sent::class, current($events));
    }

    public function test_doubts_are_not_sent_if_the_module_is_deleted() {
        $this->resetAfterTest();

        $this->setUser($this->teacher);

        $manager = \local_lpi\manager::get_instance();
        $file_metadata = $manager->get_file_metadata($this->course->id);
        $file_metadata = array_shift($file_metadata);

        $doubt = new \local_lpi\orm\doubt();
        $doubt->set_courseid($this->course->id);
        $doubt->set_message('A doubt');
        $doubt->set_messageformat(FORMAT_PLAIN);
        $doubt->set_timecreated(time());
        $doubt->set_userid($this->teacher->id);
        $doubt->set_contenthash($file_metadata->get_contenthash());

        $doubt_queue = \local_lpi\doubt_queue_manager::get_instance();
        $doubt_queue->enqueue($doubt);

        // Now delete de file.
        course_delete_module($this->resource->cmid, false);

        $this->setUser();

        // Execute the task and no exception should be thrown.
        $sink = $this->redirectEmails();

        ob_start();
        $task = new \local_lpi\task\send_doubts_email();
        $task->execute();
        ob_end_clean();

        $this->assertEquals(0, $sink->count());
    }


}