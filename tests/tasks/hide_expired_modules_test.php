<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Test for the hide_expired_modules activity.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

// I've to put this test in this namespace to mock the time() function.
namespace local_lpi\task;

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../commons.php');

/**
 * @group local_lpi
 */
class hide_expired_modules_test extends \advanced_testcase {

    use \files_methods;

    /**
     * The config singleton.
     *
     * @var local_lpi\config
     */
    private $config;
    /**
     * Courses created.
     *
     * @var \stdClass[]
     */
    private $courses;
    /**
     * Teachers in the course.
     * @var \stdClass[]
     */
    private $teachers;

    private $resources;

    /**
     * The current time to return for the mocked time() function.
     *
     * @var int
     */
    private static $mockedtime = null;

    public function setUp(): void {

        self::set_current_time_to_actual_time();

        $this->config = \local_lpi\config::get_instance();
        $this->config->set_enableactivityhiding(1);

        $this->courses = [];
        $this->courses[] = $this->getDataGenerator()->create_course(['visible' => 1]);
        $this->courses[] = $this->getDataGenerator()->create_course(['visible' => 1]);
        $this->courses[] = $this->getDataGenerator()->create_course(['visible' => 1]);

        $this->teachers = [];
        $this->teachers[] = $this->getDataGenerator()->create_user();
        $this->teachers[] = $this->getDataGenerator()->create_user();
        $this->teachers[] = $this->getDataGenerator()->create_user();

        $this->resources = [];

        for ($i = 0, $to = count($this->teachers); $i < $to; $i++) {
            $this->getDataGenerator()->enrol_user($this->teachers[$i]->id, $this->courses[$i]->id, 'editingteacher');

            $this->setUser($this->teachers[$i]);

            $this->resources[$this->courses[$i]->id] = [];
            $this->resources[$this->courses[$i]->id][] = $this->addResource($this->courses[$i], 'FICHERO 1/' . $i);
            $this->resources[$this->courses[$i]->id][] = $this->addResource($this->courses[$i], 'FICHERO 2/' . $i);

            $this->setUser();
        }
    }

    public function test_module_hidden_event_is_triggered_after_hidding_an_expired_module() {

        global $DB;

        $this->resetAfterTest();
        $manager = \local_lpi\manager::get_instance();

        self::set_current_time_out_of_weekend();

        $course = $this->courses[0];
        $filemetadatas = $manager->get_file_metadata($course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - $this->config->get_deadlineduration() - 3600;
        $DB->update_record('local_lpi_file_metadata', $r);

        $eventsink = $this->redirectEvents();

        ob_start();
        $task = new \local_lpi\task\hide_expired_modules();
        $task->execute();
        ob_end_clean();

        $eventsink->close();

        self::set_current_time_to_actual_time();

        $events = $eventsink->get_events();
        $modulehidden = $events[0];
        $this->assertInstanceOf(\local_lpi\event\module_hidden::class, $modulehidden);
    }

    public function test_task_hides_expired_modules_only() {

        global $DB;
        $this->resetAfterTest();
        self::set_current_time_out_of_weekend();

        $manager = \local_lpi\manager::get_instance();

        $course = $this->courses[0];
        $filemetadatas = $manager->get_file_metadata($course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - 3600 * 24 * 400;
        $DB->update_record('local_lpi_file_metadata', $r);

        ob_start();
        $task = new \local_lpi\task\hide_expired_modules();
        $task->execute();
        ob_end_clean();

        $mods = \get_course_mods($course->id);
        $modsinfilemetadata = $filemetadata->get_cmids();

        foreach ($modsinfilemetadata as $cmid) {
            $this->assertArrayHasKey($cmid, $mods);
            $this->assertEquals(0, $mods[$cmid]->visible);
        }

        $mods = array_filter($mods, function ($cmid) use ($modsinfilemetadata) {
            return !in_array($cmid, $modsinfilemetadata);
        });

        foreach (array_keys($mods) as $cmid) {
            if (in_array($cmid, $modsinfilemetadata)) {
                continue;
            }
            $this->assertEquals(1, $mods[$cmid]->visible);
        }

        self::set_current_time_to_actual_time();
    }

    public function test_disable_activity_hiding_disables_activity_hiding() {
        $this->resetAfterTest(true);

        self::set_current_time_out_of_weekend();

        $this->config->set_enableactivityhiding(0);

        $task = new \local_lpi\task\hide_expired_modules();

        ob_start();
        $task->execute();
        $content = ob_get_contents();
        ob_end_clean();

        $this->assertMatchesRegularExpression('/Activity hidding disabled.*/', $content);

        $this->config->set_enableactivityhiding(1);

        ob_start();
        $task->execute();
        $content = ob_get_contents();
        ob_end_clean();

        $this->assertDoesNotMatchRegularExpression('/Activity hidding disabled.*/', $content);

        self::set_current_time_to_actual_time();
    }

    public function test_activity_hiding_is_disabled_on_weekends() {
        global $DB;

        $this->resetAfterTest();

        self::set_current_time_to_a_weekend();
        $manager = \local_lpi\manager::get_instance();

        $course = $this->courses[0];
        $filemetadatas = $manager->get_file_metadata($course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - 3600 * 24 * 400;
        $DB->update_record('local_lpi_file_metadata', $r);

        $task = new hide_expired_modules();
        ob_start();
        $task->execute();
        $content = ob_get_contents();
        ob_end_clean();

        $mods = get_course_mods($course->id);
        foreach ($mods as $mod) {
            $this->assertEquals(1, $mod->visible);
        }

        self::set_current_time_to_actual_time();
    }

    public function test_hidden_courses_are_excluded_from_activity_hidden() {
        global $DB;

        $this->resetAfterTest();

        self::set_current_time_out_of_weekend();
        $manager = \local_lpi\manager::get_instance();

        $course = $this->courses[0];
        $course->visible = 0;
        update_course($course);

        $filemetadatas = $manager->get_file_metadata($course->id);

        // Put one of the files in the past. Even more than one day.
        $filemetadata = array_pop($filemetadatas);
        $manager->add_file_metadata($filemetadata);

        // The method add_file_metadata doesn't set timecreated field to the one provided.
        $r = $filemetadata->to_db();
        $r->timecreated = time() - 3600 * 24 * 400;
        $DB->update_record('local_lpi_file_metadata', $r);

        $task = new hide_expired_modules();
        ob_start();
        $task->execute();
        $content = ob_get_contents();
        ob_end_clean();

        $mods = get_course_mods($course->id);
        foreach ($mods as $mod) {
            $this->assertEquals(1, $mod->visible);
        }

        self::set_current_time_to_actual_time();
    }

    /**
     * We mock the time function for testing purposes.
     */
    public static function time() {
        return self::$mockedtime ?? \time();
    }

    /**
     * Sets the current time to the given timestamp.
     *
     * @param int $timestamp
     */
    private static function set_current_time_to (int $timestamp) {
        self::$mockedtime = $timestamp;
    }

    /**
     * Sets the current time to the actual timestamp.
     */
    private static function set_current_time_to_actual_time () {
        self::$mockedtime = null;
    }

    /**
     * Mocks time to a time that it's not weekend.
     */
    private static function set_current_time_out_of_weekend() {
        $servertz = new \DateTimeZone(\core_date::get_server_timezone());
        $now = \DateTime::createFromFormat(\DateTime::RFC3339, '2020-07-02T12:00:00P', $servertz);
        self::set_current_time_to($now->getTimestamp());
    }

    /**
     * Mocks time to a time that it on weekend.
     */
    private static function set_current_time_to_a_weekend() {
        $servertz = new \DateTimeZone(\core_date::get_server_timezone());
        $now = \DateTime::createFromFormat(\DateTime::RFC3339, '2020-07-05T12:00:00P', $servertz);
        self::set_current_time_to($now->getTimestamp());
    }
}

/**
 * Mocks time function within the namespace.
 *
 */
function time() {
    return hide_expired_modules_test::time();
}
