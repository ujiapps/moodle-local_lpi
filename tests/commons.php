<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Common traits, classes and functions to be used everywhere.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

trait files_methods {

    /**
     * Creates a file with the given mimetype at the user draft area. We use
     * the content to generate files with the same contenthash.
     *
     * @param string $mimetype
     * @param string $content
     * @returns int the draftitemid
     */
    private function create_file($filename, $mimetype = 'text/plain', $content = 'TEST FILE') {

        global $USER;

        $usercontext = \context_user::instance($USER->id);
        $itemid = file_get_unused_draft_itemid();

        $filerecord = array(
            'component' => 'user',
            'filearea' => 'draft',
            'contextid' => $usercontext->id,
            'itemid' => $itemid,
            'filename' => $filename,
            'filepath' => '/',
            'userid' => $USER->id,
            'mimetype' => $mimetype
        );
        $fs = get_file_storage();
        $file = $fs->create_file_from_string($filerecord, $content);
        return $itemid;
    }

    /**
     * Add a file in the draft user area to the given section in the course.
     *
     * @param int $draftitemid
     * @param int $courseid
     * @param int $sectionnum
     */
    private function add_file_to_section($draftitemid, $course, $sectionnum) {

        $modinfo = get_fast_modinfo($course->id);
        $section = $modinfo->get_section_info($sectionnum, MUST_EXIST);

        $contextcourse = \context_course::instance($course->id);

        file_save_draft_area_files($draftitemid, $contextcourse->id, 'course', 'section', $section->id);
    }

    /**
     * Creates a resource and adds the file sitting in the draft area.
     *
     * @param int $draftitemid
     * @param int $courseid
     */
    private function add_file_to_resource($draftitemid, $course) {
        $resourcegenerator = $this->getDataGenerator()->get_plugin_generator('mod_resource');
        $resource = new \stdClass();
        $resource->course = $course;
        $resource->files = $draftitemid;
        return $resourcegenerator->create_instance($resource);
    }

    /**
     * Creates a text file.
     *
     * @global type $USER
     * @param type $courseid
     * @param type $content
     * @return type
     */
    private function addFiles($courseid, $content = 'TEST RESOURCE') {
        global $USER;

        $usercontext = context_user::instance($USER->id);
        $itemid = file_get_unused_draft_itemid();

        $filerecord = array(
            'component' => 'user',
            'filearea' => 'draft',
            'contextid' => $usercontext->id,
            'itemid' => $itemid,
            'filename' => 'FILE_' . $courseid . '.txt',
            'filepath' => '/',
            'userid' => $USER->id
        );
        $fs = get_file_storage();
        $file = $fs->create_file_from_string($filerecord, $content);
        return $itemid;
    }

    /**
     * Adds a txt resource to the course and with the content specified.
     * s
     * @param \stdClass $course course object.
     * @param string $content
     * @return \stdClass info about the resource added.
     */
    private function addResource($course, $content = 'TEST RESOURCE') {
        $resourcegenerator = $this->getDataGenerator()->get_plugin_generator('mod_resource');
        $resource = new \stdClass();
        $resource->course = $course;
        $resource->files = $this->addFiles($course->id, $content);
        return $resourcegenerator->create_instance($resource);
    }
}


/**
 * This trait implements methods for running the plugin tasks.
 */
trait task_methods {
    private function run_task_insert_orphaned_files() {
        ob_start();
        $task = new local_lpi\task\insert_orphaned_files();
        $task->execute();
        ob_end_clean();
    }
}