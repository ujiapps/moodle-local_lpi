// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Pending changes modal.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['jquery', 'core/modal_factory', 'core/modal_events', 'local_lpi/strings'],
function($, ModalFactory, ModalEvents, str) {

    var REQUIRED_STRINGS = [
        {key: 'pending_changes_dlg_title', component: 'local_lpi'},
        {key: 'pending_changes_dlg_body', component: 'local_lpi'},
        {key: 'accept', 'component': 'core'}
    ];

    return {
        /**
         * Creates a new instance of the pending changes modal.
         *
         * @param {function} yesCb a callback to call when Yes is pressed.
         * @param {function} noCb a callback to call when No is pressed.
         * @returns {Promise}
         */
        create: function(yesCb, noCb) {

            var modal_type;

            if (typeof(ModalFactory.types.CONFIRM) == 'undefined') {
                modal_type = ModalFactory.types.SAVE_CANCEL;
            } else {
                modal_type = ModalFactory.types.CONFIRM;
            }

            str.get_strings(REQUIRED_STRINGS).then(function(strings) {

                return ModalFactory.create({
                    type: modal_type,
                    large: false,
                    title: strings.pending_changes_dlg_title,
                    body: strings.pending_changes_dlg_body,
                }).then(function(dlg) {

                    if (modal_type == ModalFactory.types.SAVE_CANCEL) {
                        dlg.setSaveButtonText(strings.accept);
                    }

                    dlg.show();
                    dlg.getRoot().on(ModalEvents.shown, function() {
                        dlg.getRoot().find('[data-action="no"]').focus();
                    });
                    dlg.getRoot().find('[data-action="no"]').focus();
                    if (yesCb) {
                        if (modal_type == ModalFactory.types.CONFIRM) {
                            dlg.getRoot().on(ModalEvents.yes, yesCb);
                        } else {
                            dlg.getRoot().on(ModalEvents.save, yesCb);
                        }
                    }
                    if (noCb) {
                        if (modal_type == ModalFactory.types.CONFIRM) {
                            dlg.getRoot().on(ModalEvents.no, noCb);
                        } else {
                            dlg.getRoot().on(ModalEvents.cancel, noCb);
                        }
                    }
                });
            });
        }
    };
});

