// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File Preview at detailed review page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(
[
    'jquery',
    'core/log'
],
function ($, log) {

    /**
     * A class to deal with file preview navigation.
     *
     * @param {int} pageNumber
     * @param {int} totalPages
     * @returns {detailed_review_pageL#16.FilePreviewNavigation}
     */
    var FilePreviewNavigation = function(pageNumber, totalPages) {
        this._el = $('.file-metadata-detailed-review-preview-navigation')[0];
        this._el = $(this._el);

        this._previousBtn = this._el.find('button.navigate-previous-button');
        this._nextBtn = this._el.find('button.navigate-next-button');
        this._navigateSelect = this._el.find('select.navigate-page-select');

        this._callbacks = [];

        this._currentPage = pageNumber;
        this._totalPages = totalPages;

        this._fillSelectPages();

        this._nextBtn.click(this._onNextBtnClick.bind(this));
        this._previousBtn.click(this._onPreviousBtnClick.bind(this));
        this._navigateSelect.change(this._onSelectChange.bind(this));
    };
    FilePreviewNavigation.prototype._fillSelectPages = function() {
        for (var i = 1; i <= this._totalPages; i++) {
            $('<option value="' + i + '">Page ' + i + ' of ' + this._totalPages + '</option>').appendTo(this._navigateSelect);
        }
    };
    FilePreviewNavigation.prototype._notifyListeners = function() {
        for (var i = 0, to = this._callbacks.length; i < to; i++) {
            this._callbacks[i](this._currentPage);
        }
    };
    FilePreviewNavigation.prototype._onNextBtnClick = function() {
        if (this._currentPage >= this._totalPages) {
            return;
        }
        this.changeCurrentPage(this._currentPage + 1);
        this._notifyListeners();
    };
    FilePreviewNavigation.prototype._onPreviousBtnClick = function() {
        if (this._currentPage == 1) {
            return;
        }
        this.changeCurrentPage(this._currentPage - 1);
        this._notifyListeners();
    };
    FilePreviewNavigation.prototype._onSelectChange = function() {
        log.debug(this._navigateSelect.val());
        this.changeCurrentPage(parseInt(this._navigateSelect.val()));
        this._notifyListeners();
    };
    FilePreviewNavigation.prototype.registerForPageChange = function(callback) {
        this._callbacks.push(callback);
    };
    FilePreviewNavigation.prototype.changeCurrentPage = function(pageNumber) {
        this._currentPage = pageNumber;
        this._navigateSelect.val(this._currentPage);
    };

    /**
     * Class to encapsulate the canvas for the file preview.
     *
     * @returns {detailed_review_page.L#16.FilePreviewCanvas}
     */
    var FilePreviewCanvas = function() {
        // The canvas element.
        this._el = $('canvas.file-preview-canvas')[0];

        this._fileNavigation = null;

        // The current page rendered.
        this._currentPage = 0;

        // The total number of pages in the document.
        this._totalPages = 0;

        // The task used to load the pdf.
        this._loadingTask = null;

        // The PDF document.
        this._pdf = null;
    };

    /**
     * Previews a new file. The file url is at the data-fileurl attribute of
     * the canvas element.
     *
     * @returns {Promise}
     */
    FilePreviewCanvas.prototype.previewFile = function() {
        var fileUrl = this._getFileUrl();
        if (fileUrl === undefined) {
            // TODO: moodle doesn't like this Promise to be here.
            return new Promise(function(resolve, reject) {
                reject();
            });
        }
        PDFJS.workerSrc = 'js/pdfjs/build/pdf.worker.js';
        this._loadingTask = PDFJS.getDocument(this._getFileUrl());
        return this._loadingTask.promise.then(this._onPDFLoaded.bind(this));
    };

    /**
     * Shows the element.
     */
    FilePreviewCanvas.prototype.show = function() {
        $(this._el).removeClass('hidden');
    };

    /**
     * Generates a preview for the specified file.
     *
     * @returns {Promise}
     */
    FilePreviewCanvas.prototype.renderPage = function(pageNumber) {

        var me = this;

        this._pdf.getPage(pageNumber).then(function(page) {
            var scale = 1;
            var viewport = page.getViewport(scale);

            var canvas = me._getElement();
            var context = canvas.getContext('2d');

            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
              canvasContext: context,
              viewport: viewport
            };
            return page.render(renderContext);
        }).catch(function(error) {
            log.error("Cannot load PDF: " + error);
        });
    };

    /**
     * Return the HTML element for the canvas Element.
     * @returns {unresolved}
     */
    FilePreviewCanvas.prototype._getElement = function() {
        return this._el;
    };
    /**
     * Returns de URL of the file to be previewed.
     *
     * @returns {string}
     */
    FilePreviewCanvas.prototype._getFileUrl = function() {
        return $(this._el).data('fileurl');
    };

    /**
     * Callback called when a PDF has been loaded (previewFile)
     *
     * @param {type} pdf
     * @returns {promise}
     */
    FilePreviewCanvas.prototype._onPDFLoaded = function(pdf) {
        this._pdf = pdf;

        this._fileNavigation = new FilePreviewNavigation(1, this._pdf.numPages);
        this._fileNavigation.registerForPageChange(this._onPageNavigationChange.bind(this));

        return this.renderPage(1);
    };
    FilePreviewCanvas.prototype._onPageNavigationChange = function(pageNum) {
        this.renderPage(pageNum);
    };

    return {
        create: function () {
            return new FilePreviewCanvas();
        }
    };

});


