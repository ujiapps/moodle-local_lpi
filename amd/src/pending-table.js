// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A module to control pending table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['jquery'], function($) {

    var PendingTable = function(id) {
        this._el = $('#' + id);
        this._el.find('[name="checkall"]').click(this._onCheckAllClick.bind(this));
        this._el.find('[name="check"]').change(this._onCheckChange);
        this._el.find('[data-action="review"]').click(this._onReviewClick.bind(this));
        this._el.find('[data-action="senddoubt"]').click(this._onSendDoubtClick.bind(this));

        this._doubtCallback = null;
        this._reviewCallback = null;
    };
    PendingTable.prototype._onCheckAllClick = function() {
        var checked = this._el.find('[name="checkall"]').prop('checked');
        this._el.find('[name="check"]').prop("checked", checked);
        if (checked) {
            this._el.find('.lpi-pending-table-row').addClass('lpi-pending-table-row-selected');
        } else {
            this._el.find('.lpi-pending-table-row').removeClass('lpi-pending-table-row-selected');
        }
    };
    PendingTable.prototype._onCheckChange = function() {
        if ($(this).prop('checked')) {
            $(this).closest('.lpi-pending-table-row').addClass('lpi-pending-table-row-selected');
        } else {
            $(this).closest('.lpi-pending-table-row').removeClass('lpi-pending-table-row-selected');
        }
    };
    PendingTable.prototype.getChosenFiles = function() {
        var ret = this._el.find('[name="check"]:checked').map(function() {
            return $(this).val();
        }).get();
        return ret;
    };
    PendingTable.prototype.doubtClick = function(doubtClickCb) {
        this._doubtCallback = doubtClickCb;
    };
    PendingTable.prototype._onSendDoubtClick = function(e) {
        e.preventDefault();
        this._doubtCallback(
            $(e.target).data('courseid'),
            $(e.target).data('contenthash'),
            $(e.target).data('filename'),
            $(e.target).data('useremail')
        );
    };
    PendingTable.prototype.reviewClick = function(reviewClickCb) {
        this._reviewCallback = reviewClickCb;
    };
    PendingTable.prototype._onReviewClick = function(e) {
        e.preventDefault();
        this._reviewCallback(
            $(e.target).data('courseid'),
            $(e.target).data('contenthash')
        );
    };

    return PendingTable;
});

