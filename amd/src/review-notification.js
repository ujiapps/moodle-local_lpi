// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A module to control pending table.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2018 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['jquery', 'core/notification', 'core/ajax', 'core/templates'], function($, Notification, ajax, Templates) {

    var TEMPLATES = {
        PENDING_FILES_NOTIFICATION: 'local_lpi/pending_files_notification'
    };

    return {

        checkPendingFiles: function(courseid, coursefullname, reviewurl, moreinfourl, notificationtext) {

            var promises = ajax.call([{
                methodname: 'local_lpi_should_notify',
                args: {courseid: courseid}
            }]);

            promises[0].then(function(should_notify) {

                if (!should_notify) {
                    return false;
                }

                var ret;

                if (notificationtext) {
                    ret = new Promise(function (accept) {
                        accept(notificationtext);
                    });
                } else {
                    ret = Templates.render(TEMPLATES.PENDING_FILES_NOTIFICATION, {
                        coursefullname: coursefullname,
                        reviewurl: reviewurl,
                        moreinfourl: moreinfourl,
                    });
                }

                return ret;

            }).then(function(text) {

                Notification.addNotification({
                    message: text,
                    type: 'warning'
                });

            });
        }
    };

});
