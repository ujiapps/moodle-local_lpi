// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An interface to the AJAX API.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['core/ajax'], function(Ajax) {
    return {

        FORMAT_PLAIN: 1,

        /**
         * Gets the pending fileids for a course.
         *
         * @param {int} courseid the courseid to check.
         * @returns {Promise}
         */
        get_pending_fileids: function(courseid) {
            return Ajax.call([{
                    methodname: 'local_lpi_get_pending_fileids',
                    args: {
                        courseid: parseInt(courseid)
                    }
            }], true, true)[0];
        },

        /**
         * Enqueues a new doubt message to send.
         *
         * @param {int} courseid the courseid.
         * @param {string} contenthash the id of the file.
         * @param {string} message the message.
         * @param {int} messageformat the message format.
         * @returns {Promise}
         */
        add_doubt_message: function(courseid, contenthash, message, messageformat) {
            return Ajax.call([{
                    methodname: 'local_lpi_add_doubt_message',
                    args: {
                        courseid: parseInt(courseid),
                        contenthash: contenthash,
                        message: message,
                        messageformat: parseInt(messageformat)
                    }
            }], true, true)[0];
        }
    };

});
