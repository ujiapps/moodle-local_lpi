// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Review page functionality.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['jquery', 'local_lpi/strings', 'local_lpi/modal-doubt', 'local_lpi/api',
        'core/notification', 'local_lpi/form-massive-actions', 'core/modal_factory',
        'core/modal_events', 'local_lpi/pending-table', 'core/config'],

function($, str, ModalDoubt, api, Notification, FormMassive, ModalFactory, ModalEvents, PendingTable, cfg) {

    // From FORMAT_PLAIN defined in code.
    var FORMAT_PLAIN = 2;

    var REQUIRED_STRINGS = [
        { key: 'doubt_enqueued_successfully', component: 'local_lpi' },
        { key: 'doubt_enqueued_error', component: 'local_lpi' },
        { key: 'massive_confirm_dlg_title', component: 'local_lpi' }
    ];

    var strings = null;

    var massiveActionsForm;
    var pendingTable;

    /**
     * Click handler for doubt button.
     */
    var onBtnDoubtClick = function(courseid, contenthash, filename, useremail) {
        ModalDoubt.create(courseid, contenthash, filename, useremail, onDoubtModalBtnSaveClick);
    };

    /**
     * Send the message
     * @returns {undefined}
     */
    var onDoubtModalBtnSaveClick = function(courseid, contenthash) {

        var textarea = $(this).find('textarea');

        str.get_strings(REQUIRED_STRINGS).then(function(data) {

            if (strings === null) {
                strings = data;
            }
            return api.add_doubt_message(courseid, contenthash, textarea.val(), FORMAT_PLAIN);

        }).then(function() {

            Notification.addNotification({
                message: strings.doubt_enqueued_successfully,
                type: 'success'
            });

        }).catch(function() {

            Notification.addNotification({
                message: strings.doubt_enqueued_error,
                type: 'error'
            });

        });
    };

    /**
     * Do a submit.
     *
     * @returns {undefined}
     */
    var onMassiveActionSubmit = function(chosen) {

        var files = pendingTable.getChosenFiles();

        str.get_strings([
            {key: 'massive_confirm_dlg_body', 'component': 'local_lpi', param: { numfiles: files.length, documentcontent: chosen }},
            {key: 'massive_confirm_dlg_title', 'component': 'local_lpi'},
            {key: 'accept', 'component': 'core'}
        ]).then(function(strings) {
            var modal_type;

            if (typeof(ModalFactory.types.CONFIRM) == 'undefined') {
                modal_type = ModalFactory.types.SAVE_CANCEL;
            } else {
                modal_type = ModalFactory.types.CONFIRM;
            }

            ModalFactory.create({
                type: modal_type,
                title: strings.massive_confirm_dlg_title,
                body: strings.massive_confirm_dlg_body
            }).then(function(dlg) {
                var accept_button;

                if (modal_type == ModalFactory.types.CONFIRM) {
                    accept_button = ModalEvents.yes;
                } else {
                    dlg.setSaveButtonText(strings.accept);
                    accept_button = ModalEvents.save;
                }

                var dlgRoot = dlg.getRoot();
                dlg.show();

                dlgRoot.on(accept_button, function() {
                    massiveActionsForm.submit();
                });

                dlgRoot.on(ModalEvents.shown, function() {
                    dlgRoot.find('[data-action="no"]').focus();
                });
                dlgRoot.find('[data-action="no"]').focus();
            });

        });

    };

    var onBtnReviewClick = function(courseid, contenthash) {
        window.location.href = cfg.wwwroot + '/local/lpi/detailed_review.php?courseid=' + courseid + "&contenthash=" + contenthash;
    };

    return {

        init: function(pendingTableId) {
            pendingTable = new PendingTable(pendingTableId);
            pendingTable.doubtClick(onBtnDoubtClick);
            pendingTable.reviewClick(onBtnReviewClick);

            massiveActionsForm = new FormMassive('massive-actions-form');
            massiveActionsForm.registerSubmit(onMassiveActionSubmit);
            massiveActionsForm.setCbFiles(function() {
                // To give the chosn files to the actionsForm.
                return pendingTable.getChosenFiles();
            });
        }

    };
});
