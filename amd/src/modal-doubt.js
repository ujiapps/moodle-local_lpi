// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A modal to ask for doubts.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(
[
    'jquery',
    'core/modal_factory',
    'core/modal_events',
    'local_lpi/strings',
    'core/templates',
    'core/notification'
],
function ($, ModalFactory, ModalEvents, str, Templates, Notification) {

    var TEMPLATES = {
        DOUBT_MODAL: 'local_lpi/doubt_modal'
    };

    var REQUIRED_STRINGS = [
        { key: 'doubt_modal_title', component: 'local_lpi' },
        { key: 'doubt_modal_send', component: 'local_lpi' },
        { key: 'doubt_modal_cancel', component: 'local_lpi' },
        { key: 'error_doubt', component: 'local_lpi' },
        { key: 'error_cannot_send_empty_doubt', component: 'local_lpi' },
        { key: 'error_cannot_send_empty_doubt_title', component: 'local_lpi' },
        { key: 'close', component: 'local_lpi' }
    ];

    var strings = null;

    return {
        /**
         * Creates a new modal for doubts and register saveCb and cancelCb as
         * callabacks for the save and cancel buttons.
         *
         * @paran {int} fileid
         * @param {string} filename
         * @param {string} useremail
         * @param {function} saveCb
         * @param {function} cancelCb
         * @returns {Promise}
         */
        create: function(courseid, contenthash, filename, useremail, saveCb, cancelCb) {

            var context = {
                fileinfo: [{
                        fileid: contenthash,
                        filename: filename
                    }
                ],
                useremail: useremail
            };

            str.get_strings(REQUIRED_STRINGS).then(function(data) {

                if (strings === null) {
                    strings = data;
                }

                return Templates.render(
                    TEMPLATES.DOUBT_MODAL,
                    context
                );

            }).then(function(bodyHtml) {

                var footer = '<input type="button" class="btn btn-primary" '
                             + 'data-action="send" value="'
                             + strings.doubt_modal_send
                             + '" />&nbsp;'

                             + '<input type="button" class="btn btn-secondary" '
                             + 'data-action="cancel" value="'
                             + strings.doubt_modal_cancel
                             + '" />';

                return ModalFactory.create({
                    type: ModalFactory.types.DEFAULT,
                    title: strings['doubt_modal_title'],
                    body: bodyHtml,
                    footer: footer
                });

            }).then(function(dlg) {

                dlg.show();
                dlg.getRoot().find('select').attr('disabled', true);
                dlg.getRoot().find('textarea').focus();

                dlg.getRoot().on(ModalEvents.shown, function() {
                    dlg.getRoot().find('textarea').focus();
                });

                dlg.getRoot().find('input[data-action="send"]').click(function() {

                    var textarea = dlg.getRoot().find('textarea');
                    if (textarea.val().trim().length == 0) {
                        Notification.alert(
                            strings.error_cannot_send_empty_doubt_title,
                            strings.error_cannot_send_empty_doubt,
                            strings.close
                        );
                    } else {
                        if (saveCb) {
                            saveCb.bind(dlg.getRoot())(courseid, contenthash);
                        }
                        dlg.hide();
                    }
                });
                dlg.getRoot().find('input[data-action="cancel"]').click(function() {
                    if (cancelCb) {
                        cancelCb.bind(dlg.getRoot())();
                    }
                    dlg.hide();
                });
            }).catch(function(e) {
                if (strings) {
                    var message = strings.error_doubt + '<br /> ' + e.debuginfo;
                } else {
                    var message = "Se produjo un error y la consulta no pudo ser efectudada. "
                                + "Detalles: <br /><br />" + e.debuginfo + "<br />" + e.backtrace;
                }
                Notification.addNotification({
                    message: message,
                    type: 'error',
                    closebutton: true
                });
            });
        }

    };

});
