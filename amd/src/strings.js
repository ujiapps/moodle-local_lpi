// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A small utility module to ease the use of strings.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['core/str'], function (str) {

    var StringHelper = function() {};

    /**
     * Loads the strings indicated. It return a Promise that, when solved,
     * passes as arguments an objets whose keys are the string keys and
     * the value the text.
     *
     * This works if and only if there is no collision with key names.
     *
     * @param {array} strings Array of required strings like core/string.get_strings
     * @returns {Promise}
     */
    StringHelper.prototype.get_strings = function(strings) {
        return str.get_strings(strings).then(function(data) {
            var ret = {};
            for (var i = 0; i < data.length; i++) {
                ret[strings[i].key] = data[i];
            }
            return ret;
        });
    };

    var helper = new StringHelper();

    return {
        get_strings: helper.get_strings
    };

});
