// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A controller for the metadata form.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(
    ['jquery', 'local_lpi/strings', 'local_lpi/modal-doubt', 'local_lpi/api', 'core/notification'],
    function ($, str, ModalDoubt, Api, Notification) {

    var REQUIRED_STRINGS = [
        { key: 'comments_placeholder', component: 'local_lpi' },
        { key: 'comments_placeholder_other', component: 'local_lpi' },
        { key: 'doubt_enqueued_successfully', component: 'local_lpi' },
        { key: 'doubt_enqueued_error', component: 'local_lpi' }
    ];

    var strings_promise;

    /**
     * The form element.
     */
    var el;

    /**
     * Determines if the form has been modified.
     *
     * @type Boolean
     */
    var changed = false;

    /**
     * The id of the form.
     *
     * @type string
     */
    var id;

    /**
     * Hides disabled elements in the form.
     */
    var hideDisabledElements = function() {

        M.form.updateFormState(id);

        el.find(':disabled').parents('.lpi_metadata_field').addClass('hidden');
        el.find('input').not(':disabled').parents('.lpi_metadata_field.hidden').removeClass('hidden');
        el.find('select').not(':disabled').parents('.lpi_metadata_field.hidden').removeClass('hidden');
        el.find('textarea').not(':disabled').parents('.lpi_metadata_field.hidden').removeClass('hidden');
    };


    var changeCommentsPlaceHolder = function() {
        var selected = el.find('[name^="document_content_group"]:checked').val();
        var comments = el.find('[name="comments"]');
        strings_promise.then(function(strings) {
            if (selected == 'other') {
                comments.attr('placeholder', strings.comments_placeholder_other);
            } else if (selected == 'lt10fragment' || selected == 'havelicense') {
                comments.attr('placeholder', strings.comments_placeholder);
            }
        });
    };

    var changeHelpButtonToHover = function() {

        el.find('a.btn.btn-link[data-toggle="popover"]').each(function() {
            $(this).attr('data-trigger', 'hover');
        });

        el.find('a[data-toggle="popover"] i.icon').hover(function() {
            $(this).attr('title', '');
        });
    };

    return {

        init: function (formid) {

            id = formid;

            el = $('#' + formid);
            el.find('input').change(function() {
                if (!changed) {
                    changed = true;
                }
            });

            // document_content_group can hide / show elements.
            el.find('[name^="document_content_group"]').change(function() {
                el.find('.form_metadata_hidden').removeClass('form_metadata_hidden');
                hideDisabledElements();
                changeCommentsPlaceHolder();
            });

            // OK. this is something tricky but I can't rely on the form being
            // initialized now.
            setTimeout(function() {
                if (M.form.updateFormState) {
                    if (el.find('[name^="document_content_group"]:checked').val()) {
                        el.find('.form_metadata_hidden').removeClass('form_metadata_hidden');
                    }

                    M.form.updateFormState(formid);
                    hideDisabledElements();
                } else {
                    setTimeout(this, 100);
                }
            }, 100);

            el.find('button.btn-secondary').click(function() {
                var fileid = $(this).data('fileid');
                var useremail = $(this).data('useremail');
                var courseid = $(this).data('courseid');
                var filename = $(this).data('filename');

                strings_promise.then(function(strings) {

                    ModalDoubt.create(courseid, fileid, filename, useremail, function() {
                        var message  = $(this).find('textarea').val();
                        Api.add_doubt_message(courseid, fileid, message, Api.FORMAT_PLAIN).then(function() {
                            Notification.addNotification({
                                message: strings.doubt_enqueued_successfully,
                                type: 'success'
                            });
                        }).catch(function() {
                            Notification.addNotification({
                                message: strings.doubt_enqueued_error,
                                type: 'error'
                            });
                        });

                    });
                });
            });

            // We want the help button to be displayed on hover not on click.
            changeHelpButtonToHover();

            strings_promise = str.get_strings(REQUIRED_STRINGS);

        },

        /**
         * Returns true if something changed in the form.
         * @returns {Boolean}
         */
        isFormChanged: function() {
            return changed;
        },

        /**
         * Sets the value for the next fileid to load when sending file
         * metadata.
         *
         * @param {type} fileid
         * @returns {undefined}
         */
        setNextFileId: function(fileid) {
            el.find('[name="nextfileid"]').val(fileid);
        },

        /**
         * Register a callback to call when submit is called.
         *
         * @param {type} callback
         * @returns {undefined}
         */
        submit: function(callback) {
            el.submit(callback);
        }
    };

});

