// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A controller for the detailed review page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(
[
    'jquery',
    'local_lpi/api',
    'local_lpi/form-metadata-controller',
    'local_lpi/page-detailed-review-navigation',
    'local_lpi/page-detailed-review-filepreview',
    'local_lpi/modal-pending-changes',
    'core/config',
    'jqueryui'
],
function($, api, FormMetadata, FileNavigation, FilePreview, ModalPendingChanges, config, jqueryui) {

    /**
     * The file navigation controller.
     *
     * @type FileNavigation
     */
    var navigation = null;

    var filePreview;

    var _courseid = null;

    /**
     * Returns the url of the detailed_review page for a course / fileid pair.
     *
     * @param {int} contenthash
     * @returns {String}
     */
    var getDetailedReviewUrl = function(contenthash) {
        return config.wwwroot + '/local/lpi/detailed_review.php?courseid=' + _courseid + '&contenthash=' + contenthash;
    };

    /**
     * Renders the detailed navigation page for the given contenthash.
     *
     * @param {int} contenthash
     */
    var renderFile = function(contenthash) {
        if (contenthash === null) {
            return;
        }
        if (FormMetadata.isFormChanged()) {
            ModalPendingChanges.create(function() {
                window.location.href = getDetailedReviewUrl(contenthash);
            });
        } else {
            window.location.href = getDetailedReviewUrl(contenthash);
        }
    };

    /**
     * Handler for previous file button click.
     */
    var onFilePreviousButtonClick = function() {
        renderFile(navigation.getPrevFile());
    };
    /**
     * Handler for next file button click.
     */
    var onFileNextButtonClick = function() {
        renderFile(navigation.getNextFile());
    };

    return {
        /**
         * Initialize the module.
         *
         * @param {int} courseid the id of the course.
         * @param {string} contenthash the id of the file to show now.
         * @returns {undefined}
         */
        init: function(contenthash, courseid) {
            _courseid = parseInt(courseid);

            $('.file-metadata-review-panel-resizable').resizable({
                handles: 'w',
                ghost: false,
                maxWidth: $(window).width(),
                start: function() {
                    $(this).resizable("option", "maxWidth", $(window).width());
                },
                resize: function(event, ui) {
                    var totalWidth = $(window).width();
                    var xWidth = ui.size.width * 100 / totalWidth;
                    var previewWidth = 100 - xWidth;
                    $(this).css({width: "" + xWidth + "%", left: "" + previewWidth + "%"});
                    $('.file-metadata-detailed-review-preview-container').css({width: "" + previewWidth + "%"});
                }
            });

            // The file preview canvas.
            filePreview = FilePreview.create();
            filePreview.previewFile().then(function() {
                filePreview.show();
            }).catch(function() {
                // TODO: err handling.
            });

            // The file navigation controller.
            navigation = FileNavigation.create(contenthash, courseid);
            navigation.next(onFileNextButtonClick);
            navigation.previous(onFilePreviousButtonClick);

            // Initialize the form controller.
            FormMetadata.init('detailed-review-form');
            FormMetadata.submit(function() {
                var nextfile = navigation.getNextFile();
                nextfile = ( nextfile === null ) ? '' : nextfile;
                FormMetadata.setNextFileId(nextfile);
            });
        }
    };
});
