// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File navigation for detailed review page.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later2
 */

define(['jquery', 'local_lpi/api'], function ($, api) {

    /**
     * A class to deal with File Navigation in the detailed review page.
     */
    var FileNavigation = function(contenthash, courseid) {

        var me = this;

        this._el = $('.file-metadata-detailed-review-navigation');

        // Previous button element.
        this._previous = this._el.find('.file-previous');

        // Next button element.
        this._next = this._el.find('.file-next');

        // The contenthash of the file we're currently showing.
        this._file = contenthash;

        // An array of contenthash that we're going to navigate.
        this._files = null;

        // The current file index.
        this._currentFileIndex = null;

        // The courseid.
        this._courseid = courseid;

        // If it's initialized.
        this._initialized = false;

        // Callbacks registered for next button click.
        this._nextCbs = [];

        // Callbacks registers for previous button click.
        this._prevCbs = [];

        // Get the fileids we're going to navigate, once.
        api.get_pending_fileids(courseid).then(function(files) {

            if (files.count === 0) {
                return;
            }
            me._files = files.data;
            me._currentFileIndex = files.data.indexOf(me._file);
            // This should not happen... but... who knows.
            if (me._currentFileIndex === -1) {
                me._file = me._files[0];
                me._currentFileIndex = 0;
            }
            me._initialized = true;

            if (me._files.length > 1) {
                if (me._currentFileIndex == 0) {
                    me._previous.addClass('hidden');
                } else if (me._currentFileIndex == me._files.length-1) {
                    me._next.addClass('hidden');
                }
            } else {
                me._next.addClass('hidden');
                me._previous.addClass('hidden');
            }

        }).catch(function() {
            // TODO: ERROR HANDLING :-/
        });

        this._previous.click(this._onPreviousClick.bind(this));
        this._next.click(this._onNextClick.bind(this));
    };
    /**
     * Registers a new callback for next button click.
     *
     * @param {function} callback
     */
    FileNavigation.prototype.next = function(callback) {
        this._nextCbs.push(callback);
    };
    /**
     * Registers a new callback for previous button click.
     *
     * @param {function} callback
     * @returns {undefined}
     */
    FileNavigation.prototype.previous = function(callback) {
        this._prevCbs.push(callback);
    };
    /**
     * Returns the next fileid or null if no more fileids are available.
     *
     * @returns {int}
     */
    FileNavigation.prototype.getNextFile = function() {
        if (this._currentFileIndex === this._files.length - 1) {
            return null;
        }
        return this._files[this._currentFileIndex + 1];
    };
    /**
     * Returns the previous fileid or null if no more fileids are available.
     *
     * @returns {int}
     */
    FileNavigation.prototype.getPrevFile = function() {
        if (this._currentFileIndex === 0) {
            return null;
        }
        return this._files[this._currentFileIndex - 1];
    };
    /**
     * Returns the current file id.
     *
     * @returns {int}
     */
    FileNavigation.prototype.getCurrentFile = function() {
        return this._files[this._currentFileIndex];
    };

    FileNavigation.prototype._onPreviousClick = function() {
        if (!this._initialized) {
            return;
        }
        this._prevCbs.forEach(function(callback) {
            callback();
        });
    };
    FileNavigation.prototype._onNextClick = function() {
        if (!this._initialized) {
            return;
        }
        this._nextCbs.forEach(function(callback) {
            callback();
        });
    };

    return {
        /**
         * Returns a new instance of FileNavigation.
         *
         * @param {int} fileid
         * @param {courseid} courseid
         * @returns {page-detailed-review-navigationL#24.FileNavigation}
         */
        create: function(fileid, courseid) {
            return new FileNavigation(fileid, courseid);
        }
    };

});
