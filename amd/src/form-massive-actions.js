// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Descripción
 *
 * @package    local/lpi
 * @copyright  2017 Universitat Jaume I (https://www.uji.es/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/notification', 'local_lpi/strings'],
function ($, Notification, str) {

    var MassiveActionsForm = function(id) {
        this._el = $('#' + id);
        this._callbacks = [];
        this._cbgetfiles = null;

        this._el.submit(this._onSubmit.bind(this));
    };
    MassiveActionsForm.prototype.registerSubmit = function(cb) {
        this._callbacks.push(cb);
    };
    MassiveActionsForm.prototype._onSubmit = function(e) {

        if (this._el.find('[name="confirmed"]').val() == 1) {
            return;
        }

        e.preventDefault();

        var files = this._cbgetfiles();
        // If no files chosen then cancel.
        if (files.length === 0) {
            str.get_strings([{
                key: 'massive_actions_one_needed', component: 'local_lpi'
            }, {
                key: 'yes', component: 'moodle'
            }]).then(function(strings) {
                Notification.alert(
                        null,
                        strings.massive_actions_one_needed,
                        strings.yes);
            }).catch(Notification.exception);

            return;
        }

        this.setFiles(files);

        var chosen = this._el.find('[name="actions"] option:selected').text();
        for (var i = 0; i < this._callbacks.length; i++) {
            this._callbacks[i](chosen);
        }
    };
    MassiveActionsForm.prototype.disable = function() {
        this._el.find('[name="review"]').attr('disabled', 'disabled');
    };
    /**
     * Adds files to be submited.
     *
     * @param {Array} contenthashes an array of the selected contenthashes.
     * @returns {undefined}
     */
    MassiveActionsForm.prototype.setFiles = function(contenthashes) {
        var filesElement = this._el.find('[name="files"]');
        filesElement.val(contenthashes.join(","));
    };

    MassiveActionsForm.prototype.setCbFiles = function(cb) {
        this._cbgetfiles = cb;
    };

    /**
     * Submits the form. This method is called when the user has confirmed that
     * the form must be submitted.
     *
     * @returns {undefined}
     */
    MassiveActionsForm.prototype.submit = function() {
        this._el.find('[name="confirmed"]').val(1);
        this._el.submit();
    };

    return MassiveActionsForm;
});

