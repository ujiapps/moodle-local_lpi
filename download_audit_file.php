<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * An interface to download a file for the audit subsystem. It can download
 * just files that have been classified.
 *
 * We don't use pluginfile.php or webservice/pluginfile.php because there no
 * way to specify that the above conditions are met. Moreover,
 * we don't want to give the webservice user more privileges that the necessary.
 *
 * This script is mainly inspired by webservice/pluginfile.php.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

define('AJAX_SCRIPT', true);
define('NO_MOODLE_COOKIES', true);

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/webservice/lib.php');

use local_lpi\orm\file_metadata;

// The id in the {local_lpi_file_metadata} table.
$metadataid = required_param('id', PARAM_INT);
// Token based authentication.
$token = required_param('token', PARAM_RAW);

$webservicelib = new webservice();
$authenticationinfo = $webservicelib->authenticate_user($token);
$enabledfiledownload = (int) ($authenticationinfo['service']->downloadfiles);
if (empty($enabledfiledownload)) {
    throw new webservice_access_exception('Web service file downloading must be enabled in external service settings');
}

// Authorization.
$manager = \local_lpi\manager::get_instance();
$filemetadata = $manager->get_file_metadata_by_id($metadataid);
if (!$filemetadata) {
    send_file_not_found();
}

// Do not call require_login. We don't require to be enrolled into the course.
$ctx = \context_course::instance($filemetadata->get_courseid());
require_capability('local/lpi:audit', $ctx);

$dc = $filemetadata->get_document_content();
switch ($dc->get_value()) {
    case \local_lpi\objects\document_content::UNKNOWN:
    case \local_lpi\objects\document_content::ADMINISTRATIVE:
        send_file_not_found();
        break;
}

// Finally, send the file (one of them if multiple area set).
send_stored_file($filemetadata->get_files()[0], null, 0, true);
