<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Global settings.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {

    $settings = new admin_settingpage('local_lpi', get_string('lpisettings', 'local_lpi'));
    $ADMIN->add('localplugins', $settings);

    $settings->add(new admin_setting_heading(
        'header_hide_activities',
        get_string('header_hide_activity', 'local_lpi'),
        get_string('header_hide_activity_desc', 'local_lpi')
    ));

    $settings->add(new admin_setting_configcheckbox(
            'local_lpi/enableactivityhiding',
            get_string('enableactivityhiding', 'local_lpi'),
            get_string('enableactivityhiding_desc', 'local_lpi'),
            '0',
            '1',
            '0'
        )
    );

    // Maximum number of hours a teacher can have its files without settings.
    $settings->add(new admin_setting_configduration(
            'local_lpi/deadlineduration',
            get_string('deadlineduration', 'local_lpi'),
            get_string('deadlinedurationdesc', 'local_lpi'),
            24 * 3600
        )
    );

    $settings->add(new admin_setting_heading(
        'header_others',
        get_string('header_others', 'local_lpi'),
        get_string('header_others_desc', 'local_lpi')
    ));

    // Text to be shown in the user notification zone at course page.
    $settings->add(new admin_setting_confightmleditor(
            'local_lpi/notificationtext',
            get_string('notificationtext', 'local_lpi'),
            get_string('notificationtextdesc', 'local_lpi'),
            '',
            PARAM_RAW,
            60, // Cols.
            8   // Rows.
            )
    );

    // TODO: Develop an admin_setting to choose a date. It doesn't exists yet.

    // Email to send doubts to.
    $settings->add(new admin_setting_configtext(
            'local_lpi/doubtsemail',
            get_string('doubtsemail', 'local_lpi'),
            get_string('doubtsemaildesc', 'local_lpi'),
            '',
            PARAM_EMAIL
            )
    );

    // Email to send pending to get licenses email notifications.
    $settings->add(new admin_setting_configtext(
            'local_lpi/pendingreviewemail',
            get_string('pending_review_email', 'local_lpi'),
            get_string('pending_review_email_desc', 'local_lpi'),
            '',
            PARAM_EMAIL
            )
    );

    // The url of the external tool used to review externally files.
    $settings->add(new admin_setting_configtext(
            'local_lpi/reviewweburl',
            get_string('review_web_url', 'local_lpi'),
            get_string('review_web_url_desc', 'local_lpi'),
            '',
            PARAM_URL
            )
    );

    // URL for the help guide shown at the review page.
    $settings->add(new admin_setting_configtext(
            'local_lpi/guideurl',
            get_string('guide_url', 'local_lpi'),
            get_string('guide_url_desc', 'local_lpi'),
            '',
            PARAM_URL
            )
    );
}
