<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Navigation links.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Adds a link to the upside flat navigation menu.
 *
 * @global \moodle_page $PAGE
 * @param global_navigation $navigation
 */
function local_lpi_extend_navigation(global_navigation $navigation) {
    global $PAGE, $COURSE;

    if ($PAGE->context->contextlevel !== CONTEXT_COURSE) {
        return;
    }
    $manager = local_lpi\manager::get_instance();
    if ($manager->is_ignored_course($PAGE->context->instanceid)) {
        return;
    }
    if (has_capability('local/lpi:addfilemetadata', $PAGE->context)) {
        $coursenode = $navigation->find($PAGE->context->instanceid, navigation_node::TYPE_COURSE);
        if ($coursenode === false) {
            return;
        }

        $node = navigation_node::create(
            \html_writer::div(get_string('navigation_title', 'local_lpi'), 'gdlpi-link'),
            new moodle_url('/local/lpi/review.php', array('courseid' => $PAGE->context->instanceid)),
            global_navigation::TYPE_CUSTOM,
            null,
            "lpimanagement",
            new pix_icon('copyrightask', 'cosa', 'local_lpi')

        );
        $node->mainnavonly = true;
        $coursenode->add_node($node, 'participants');
    }
}

/**
 * Adds a category and a link to the course navigation.
 *
 * @param navigation_node $navigation
 * @param type $course
 * @param type $context
 */
function local_lpi_extend_navigation_course(navigation_node $navigation, $course, $context) {

    $manager = local_lpi\manager::get_instance();
    if ($manager->is_ignored_course($course->id)) {
        return;
    }
    if (has_capability('local/lpi:addfilemetadata', $context)) {
        $cat = $navigation->create(
                get_string('navigation_category', 'local_lpi'),
                null,
                navigation_node::TYPE_CATEGORY
        );
        $navigation->add_node($cat);

        $node = $cat->create(
            get_string('navigation_title', 'local_lpi'),
            new moodle_url('/local/lpi/review.php', array('courseid' => $course->id)),
            global_navigation::TYPE_CUSTOM,
            null,
            "lpimanagement",
            new pix_icon('e/question', get_string('navigation_title', 'local_lpi'))
        );
        $cat->add_node($node);
    }
}

