<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Backup code.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

class backup_local_lpi_plugin extends backup_local_plugin {

    private static $filemetadatafields = [
        'userid',
        'property',
        'type',
        'contenthash',
        'author',
        'license',
        'title',
        'identification',
        'state',
        'timecreated',
        'publisher',
        'pages',
        'totalpages',
        'rightsentity',
        'timeendlicense',
        'comments',
        'denyreason',
        'nenrolments',
        'lastdoubttime',
        'timemodified'
    ];

    private function define_mygeneral_structure() {
        $metadatas = new backup_nested_element('file_metadata', array('id'), self::$filemetadatafields);

        $sql = "SELECT DISTINCT fm.* "
                . "FROM {local_lpi_file_metadata} fm "
                . "     JOIN {files} f ON f.contenthash = fm.contenthash "
                . "WHERE fm.courseid = :courseid "
                . "      AND f.contextid = :contextid ";

        $metadatas->set_source_sql(
            $sql,
            ['courseid' => backup::VAR_COURSEID, 'contextid' => backup::VAR_CONTEXTID]
        );
        $metadatas->annotate_ids('user', 'userid');

        // The plugin wrapper.
        $locallpiinfo = core_plugin_manager::instance()->get_plugin_info('local_lpi');

        $pluginwrapper = new backup_nested_element($this->get_recommended_name());
        $pluginwrapper->add_child($metadatas);
        $pluginwrapper->add_attributes("version");

        $pluginwrapper->set_source_array(array(
            array('version' => $locallpiinfo->versiondisk)
        ));

        $plugin = $this->get_plugin_element();
        $plugin->add_child($pluginwrapper);

        return $plugin;
    }

    public function define_module_plugin_structure() {
        return $this->define_mygeneral_structure();
    }

    public function define_section_plugin_structure() {
        return $this->define_mygeneral_structure();
    }
}
