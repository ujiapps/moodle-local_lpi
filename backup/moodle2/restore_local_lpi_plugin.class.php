<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Restore code for the local/lpi plugin.
 *
 * @package    local
 * @subpackage lpi
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 */

defined('MOODLE_INTERNAL') || die();

use local_lpi\orm\file_metadata;

class restore_local_lpi_plugin extends restore_local_plugin {

    /**
     * Ids of file_metadatas already restored. This is to prevent nasty errors.
     *
     * @var string[]
     */
    private $fmsalreadyrestored;

    public function __construct($plugintype, $pluginname, $step) {
        parent::__construct($plugintype, $pluginname, $step);
        $this->fmsalreadyrestored = [];
    }

    public function define_course_plugin_structure() {

        $elements = array();
        $elements[] = new restore_path_element('file_metadata', $this->get_pathfor("/file_metadata"));

        return $elements;
    }

    public function define_module_plugin_structure() {
        return [
            new restore_path_element('file_metadata', $this->get_pathfor('/file_metadata'))
        ];
    }
    public function define_section_plugin_structure() {
        return [
            new restore_path_element('file_metadata', $this->get_pathfor('/file_metadata'))
        ];
    }

    public function process_file_metadata($data) {
        global $USER, $DB;

        $data = (object) $data;
        if (array_key_exists($data->contenthash, $this->fmsalreadyrestored)) {
            return;
        }

        // If it's the property of the teachers at this course we put it in unknown state by not restoring it.
        // It must be reviewed at the time.
        if ($data->property == file_metadata::PROPERTY_TEACHERS) {
            return;
        }

        // If data is in unknown state, then no need to insert anything.
        if ($data->state == file_metadata::STATE_UNKNOWN || $data->state == null) {
            return;
        }

        // If the backup was not generated on the same site, task the mappingid.
        if (!$this->task->is_samesite()) {
            $data->userid = $this->get_mappingid('user', $data->userid);
            // If the mapping is not set, then return.
            if ($data->userid == 0) {
                return;
            }
        }

        $manager = \local_lpi\manager::get_instance();

        // Do not process file metadata that has been already reviewed by the user in the destination course.
        $filemetadata = $DB->get_record('local_lpi_file_metadata', ['courseid' => $this->task->get_courseid(), 'contenthash' => $data->contenthash], 'id,state');
        if ($filemetadata && $filemetadata->state !== null && $filemetadata->state !== file_metadata::STATE_UNKNOWN) {
            return;
        }

        $newfilemetadata = new file_metadata($data);
        if ($filemetadata) {
            // If it exists, set the already existing id.
            $newfilemetadata->set_id($filemetadata->id);
        } else {
            // If not exists, set null as the id.
            $newfilemetadata->set_id(null);
        }

        // Change the courseid to the destination courseid.
        $newfilemetadata->set_courseid($this->task->get_courseid());
        // Change the userid to the currently loggeding user.
        $newfilemetadata->set_userid($data->userid);

        $manager->add_file_metadata($newfilemetadata);

        $this->fmsalreadyrestored[$data->contenthash] = null;
    }

}
