# Módulo de Gestión de Derechos de Propiedad Intelectual para Moodle 

El módulo de Gestión de Derechos de Propiedad Intelecual (GDPI) es un plugin [local de Moodle](https://docs.moodle.org/dev/Local_plugins) 
que permite al profesorado de un curso declarar el contenido de los ficheros que ponen a disposición del estudiantado en base a la Ley de 
Propiedad Intelectual Española. Dicha catalogación permite determinar si el contenido del documento genera obligaciones legales y/o 
económicas para la institución donde se pone en marcha.

El público objetivo del plugin es el profesorado de la educación reglada impartida en centros integrados en el sistema educativo español 
(ver [artículo 32 de la LPI](https://www.boe.es/buscar/act.php?id=BOE-A-1996-8930&tn=1&p=20180414#a32)) y permite únicamente la catalogación 
de obras impresas o susceptibles de serlo (obras textuales).

## Prerequisitos

El plugin tiene los siguientes requisito mínimos:

* Moodle 4.1 o superior.
* Extensión openssl de PHP.

## Instalación

El código del plugin se gestiona con Git y está disponible en [Bitbucket](https://bitbucket.org/ujiapps/moodle-local_lpi/). En la rama 
`master` dispondremos siempre de una versión estable etiquetada con un `tag` que correspoderá al número de versión.

Puede descargar el código utilizando Git:

```
$ git clone https://bitbucket.org/ujiapps/moodle-local_lpi.git
```

Para la instalación siga el [procedimiento estándar](https://docs.moodle.org/33/en/Installing_plugins) de cualquier plugin:

* Copie el directorio raíz en el directorio *local/lpi* de su instalación de Moodle.
* Como administrador visite la página *Notificaciones*.

## Configuración

Puede acceder a la configuración del plugin en el apartado:

    *Administración del sitio > Extensiones locales > Ajustes LPI*

Los parámetros de configuración son los siguientes:

* **Plazo de Ocultación**. Indica el periodo de tiempo en que un fichero puede permanecer visible al estudiantado de un curso sin haber sido tramitado y sin ser ocultado automáticamente por el plugin.
* **Texto a notificar**. Permite definir un texto de aviso al profesorado de ficheros pendientes de revisión.
* **Correo electrónico para las dudas**. Permite definir una dirección de correo electrónico de destino para formular las dudas que el profesorado puede plantear desde la interfaz de usuario del módulo.
* **Correo electrónico de tramitación pendiente**. Permite definir una dirección de correo electrónico a la que enviar un mensaje de documento con tramitación pendiente.
* **URL de la herramienta de tramitación***. Indica la URL en la que se encuentra instalada la herramienta externa para la tramitación de licencias.
* **URL a la guía de ayuda**. Indica la URL en la que se encuentra alojado el documento de ayuda al profesorado.

## Funcionamiento

La puesta en marcha del plugin es un tema delicado y debe venir precedido por un trabajo de información y formación amplio al profesorado. 
Tenga en cuenta que el plugin, tras un período de tiempo configurable, oculta aquellas actividades que contengan ficheros no catalogados y, 
por lo tanto, tiene un fuerte impacto en el normal transcurso del curso.

Para más detalles sobre el uso y la parametrización del plugin, consulte la [Guía de Administración del módulo GDPI](https://jira.uji.es/confluence/pages/viewpage.action?pageId=49807528).

## Licencia

Este proyecto se distribuye bajo licencia dual GPLv3 y EUPLv1.2. Puede encontrar información adicional [aquí](https://www.uji.es/ujiapps/llicencia).
